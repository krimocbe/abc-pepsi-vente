<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class body_daily_vente extends Model
{
    //
     protected $table='body_daily_vente';

       protected $fillable = [
        	'date_ajout',
        	'body_id',
  			'produit_id',
  			'quantite',
  			'RB30',
            'RB100',
            'PET0_33',
            'PET_1',
            'PET1_5',
            'PET_2',
            'VISITES_PROGRAMMEES',
            'VISITES_AVEC_VENTE',
            'can',
            'VISITES_realise','objectif_id','type'
		]; 


    public function headers()
    {
        return $this->belongsTo('App\head_daily_vente','body_id');
    }


		
}
