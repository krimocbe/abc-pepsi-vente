<?php

namespace App\Exports;

use App\body_daily_vente;
use Maatwebsite\Excel\Concerns\FromCollection;

class bodyExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return body_daily_vente::all();
    }
}
