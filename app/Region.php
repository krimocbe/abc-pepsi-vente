<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    //
    protected $fillable = [
        'name_region', 'responsable_region'
    ];
    public function zones()
    {
    	return $this->hasMany('app\Zone');
    }
}
