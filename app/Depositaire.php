<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Superviseur;

class Depositaire extends Model
{
    //

    protected $table = 'dispositaires';

    protected $fillable = [
        'name_dipositaire',
    ];

    public function superviseurs()
    {
    	return $this->belongsToMany('App\Superviseur')
      ;
    }

    public function depositaire_superviseurs()
    {
        return $this->hasMany('App\depositaire_superviseur','depositaire_id');
    }

}
