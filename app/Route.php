<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Client;
use App\prevendeur;
use App\depositaire_provendeur;

class Route extends Model
{
    //
    protected $fillable = [
        'name_route', 'type_route' , 'depositaire_superviseur_id'
    ];

    

    public function prevendeur()
    {
        return $this->hasOne('App\prevendeur');
    }

     public function depo()
    {
        return $this->belongsTo('App\depositaire_provendeur');
    }
    
    public function head_daily_ventes()
    {
        return $this->hasMany('App\head_daily_vente','route_id');
    }
    
}
