<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client_secondaire extends Model
{
    //
        protected $fillable = [
        'name_client_secondaire'
    ];
}
