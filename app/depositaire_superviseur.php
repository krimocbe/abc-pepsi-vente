<?php

namespace App;
use App\prevendeur;
use App\Route;

use Illuminate\Database\Eloquent\Model;

class depositaire_superviseur extends Model
{
    //
    protected $table='depositaire_superviseur';
    
    protected $fillable = [
        'superviseur_id','depositaire_id','taux'
    ];

    public function prevendeurs()
    {
    	return $this->hasMany('App\prevendeur','depositaire_superviseur_id');
    }

     public function routes()
    {
    	return $this->hasMany('App\Route','depositaire_superviseur_id');
    } 

    public function superviseurs()
    {
        return $this->belongsTo('App\Superviseur','superviseur_id');
    }

    public function dipositaires()
    {
        return $this->belongsTo('App\Depositaire','depositaire_id');
    }

    public function head_daily_ventes()
    {
        return $this->hasMany('App\head_daily_vente','depositaire_superviseur_id');
    }
}
