<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class head_daily_vente extends Model
{
    //
    protected $fillable = [
        'route_id','prevendeur_id','depositaire_superviseur_id'
    ];



    protected $table='head_daily_vente';

    /*public function route()
    {
        return $this->hasOne('App\Route','head_id');
    }*/

    public function prevendeur()
    {
        return $this->belongsTo('App\prevendeur');
    }
    
    
    public function depositaire_superviseur()
    {
        return $this->belongsTo('App\depositaire_superviseur');
    }

    public function route()
    {
        return $this->belongsTo('App\Route');
    }

    public function body_daily_ventes()
    {
    	return $this->hasMany('App\body_daily_vente','body_id');
    }
}
