<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Route;

class prevendeur extends Model
{
    //
        protected $fillable = [
        'name_prevendeur'	,
        'type_prevendeur'	,
        'code_prevendeur'
    ];

    public function route()
    {
        return $this->hasOne('App\Route');
    }

    public function head_daily_vente()
    {
        return $this->belongsTo('App\head_daily_vente');
    }

}
