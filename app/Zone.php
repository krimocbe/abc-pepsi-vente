<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\superviseur;

class Zone extends Model
{
    //

    protected $fillable = [
        'name_zone', 'responsable_zone','region_id','email','password'
    ];
    

    public function superviseurs()
    {
    	return $this->hasMany('app\Superviseur');
    }
}
