<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class entete_obj extends Model
{
    //
    protected $table = 'entete_obj';
    public $timestamps = false;
    
        protected $fillable = [
        'date','nbr_jour','objectif','jour','type_route'
    ];
    
    protected $casts = [
        'jour' => 'array',
    ];

    public function setOptionsAttribute($options)
	{
    	$this->attributes['jour'] = json_encode($options);
	}



}
