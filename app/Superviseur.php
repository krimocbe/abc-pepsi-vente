<?php

namespace App;
use App\Client;
use App\Superviseur;
use App\Depositaire;
use Illuminate\Database\Eloquent\Model;

class Superviseur extends Model
{
    //
    protected $fillable = [
        'name_superviseur', 'email', 'password','wilaya','matricule','zone_id'
    ];

    

    public function depositaires()
    {
   		return $this->belongsToMany('App\Depositaire','depositaire_superviseur');
   	}

   	public function depositaire_superviseurs()
   	{
   		return $this->hasMany('App\depositaire_superviseur','superviseur_id');
   	}
}
