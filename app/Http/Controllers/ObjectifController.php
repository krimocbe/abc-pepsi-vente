<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\entete_obj;


class ObjectifController extends Controller
{
    //
    public function index()
    {
		//find all superviseurs who has email:auth()
        $test=entete_obj::all();

        return view('dashboard.Objectif.affichage',compact('test'));
    }

    public function show_objectif($id)
    {
     	
     $test=entete_obj::find($id);
     return response()->json(['status' => true,'depot'=>$test]);
 	}

    public function edit($id)
    {
        $test=entete_obj::find($id);
    	return view('dashboard.Objectif.edit',compact('test'));
    
    } 
    
    public function index_prime()
    {
		//find all superviseurs who has email:auth()
		
        return view('dashboard.Objectif.index');
    }

    public function destroy($id)
    {
        
        entete_obj::destroy($id);
		return redirect()->route('Objectif.index');
    }


    public function store(Request $request)
    {
    	
    	//$month=date("F", strtotime($request->Date));
    	$stack = array();
    	$i=0;
    	foreach ($request->selectedDays as $day) {
    		$day2=explode("T", $day);
    		$stack[$i]=$day2[0];
    		$i++;
    	}
    	
    	
    	$test = entete_obj::create([
    		'date' => $request->Date,
    		'nbr_jour'=> $i,
    		'objectif'=>$request->Obj ,
    		'jour'=> $stack ,
            'type_route'=>$request->type_route
    	]);
        return response()->json(['status' => true,'depot'=>$test]);
    }
    
    public function upd_objectif(Request $request)
    {
    	$stack = array();
    	$i=0;
    	foreach ($request->jour as $day) {
    		$day2=explode("T", $day);
    		$stack[$i]=$day2[0];
    		$i++;
    	}
    	$test = entete_obj::find($request->id);
    	$test->update([
    		'date' => $request->date,
    		'nbr_jour'=> $i,
    		'objectif'=> $request->objectif,
    		'jour'=> $stack
    	]);
        return response()->json(['status' => true,'depot'=>$test]);
    }


    public function update()
    {
        
	    return \View::make('dashboard.Objectif.edit');

    }


}
