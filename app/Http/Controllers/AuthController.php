<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use JWTAuth;
use App\User;
use App\Client;
use App\Route;
use App\Superviseur;
use App\body_daily_vente;
use App\depositaire_superviseur;
use App\Depositaire;
use App\head_daily_vente;
use App\prevendeur;
use App\Produit;
use App\entete_obj;

class AuthController extends Controller
{
    //

    public function fetch_clients(Request $request)
    {
       $superviseurs = Superviseur::whereEmail( $request->email )->first();
       $depositives= $superviseurs->depositaires;
       $superviseurs->push($depositives);
	   $collection = collect([]);
       foreach ($depositives as $ro) 
        {
            $depo=$ro->depositaire_superviseurs;      
            foreach ($depo as $o) 
            {
                $route=$o->routes;
                foreach($route as $day) 
        	    {
                    $provendeur=$day->prevendeur;
                    $day->push($provendeur);
		    	}
                $o->push($day);    
            }
        }
 
        return response()->json(['status' => true,'depot'=>$depositives]);
        
	}


	public function add_daily_vente(Request $request)
	{
        return response()->json(['status' => true,'depot'=>"great_thnx"]);
	}

    public function login(Request $request)
    {
       
        header ("Access-Control-Allow-Origin: *");
		header ("Access-Control-Expose-Headers: Content-Length, X-JSON");
		header ("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");
		header ("Access-Control-Allow-Headers: *");
        $credentials = $request->only('email', 'password');
        $token = JWTAuth::attempt($credentials);
        if($token==true)
        return response()->json([
        	'status' => true, 
        	'message' => 'Login successfully',
        	'token'=>$token
        ]);
        else 
        {
        	return "unautorized";
        }

    } 

    public function register(Request $request)
    {

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password'))
        ]);
        return response()->json([
            'status' => true, 
        	'message' => 'User created successfully', 	
        	'data' => $user
        ]);
    }

    public function add_facture( Request $request)
    {
		$heads=head_daily_vente::create([
            'depositaire_superviseur_id'=>$request->depositaire_superviseur_id,
            'route_id'=>$request->route_id,
            'prevendeur_id'=>1
        ]);

        $month=date("Y-m", strtotime($request->date_ajout));
        
        $objectif_id=entete_obj::Where('date','=',$month)->first();
        
        $body=body_daily_vente::create([
  			'date_ajout' => $request->date_ajout ,
  			'body_id' => $heads->id ,
  			'quantite'=>$request->RB30,
            'RB30'=>$request->RB30,
            'RB100'=>$request->RB100,
            'PET0_33'=>$request->PET0_33,
            'PET_1'=>$request->PET_1,
            'PET1_5'=>$request->PET1_5,
            'PET_2'=>$request->PET_2,
            'VISITES_PROGRAMMEES'=>$request->visite_avec_vente,
            'VISITES_AVEC_VENTE'=>$request->visite,
            'can'=>$request->can,
            'VISITES_realise'=>$request->VISITES_realise,
            'type'=>0,
            'objectif_id'=>$objectif_id->id
    	 ]);
        return response()->json([
            'status' => true, 
        	'message'=> 'facture bien ajouté', 	
        	'data' 	 => $body->headers
        ]);
	}
}
