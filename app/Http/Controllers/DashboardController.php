<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Zone;
use App\Superviseur;
use App\body_daily_vente;
use App\entete_obj;
class DashboardController extends Controller
{
    public function index()
    {
    $role=Auth::user()->roles->first();
        if($role->name=="Admin")  
           { $superviseur=Superviseur::all();
        $chef_zone="";}
    else if($role->name=="Superviseur"){ 
        $superviseur=Superviseur::whereEmail(Auth::user()->email)->get();
        $chef_zone="";
    }
    if($role->name=="CDF")
    		{
                $zone=Zone::whereEmail(Auth::user()->email)->first();
                $superviseur=Superviseur::whereZone_id($zone->id)->get();
                $chef_zone=Auth::user()->name;
    		}    
                

                $userData1[] = [
                                    'CDZ'=> $chef_zone,
                                    'nom_superviseur' => "",
                                    'Nom route' => "",
                                    'RB30' =>0, 
                                    'PET0.33'=>0 ,
                                    'RB100'=>  0, 
                                    'PET1'=>0 ,
                                    'PET1.5'=> 0, 
                                    'PET2'=>0 ,
                                    'can'=>0 ,
                                    'VISITES_AVEC_VENTE'=>0,
                                    'VISITES_PROGRAMMEES'=>0,
                                    'VISITES_realise'=>0,
                                    'Total phy'=>0,
                                    'Familial'=>"",
                                    'Single serve'=>"",
                                    'Taux de succés'=>"" ,
                                    'Taux de visite'=>"" ,
                                    'Total 80_Z'=> "",
                                    'Drop size (80Z)'=> "",
                                    'Drop size (PHY)'=>"",
                                    'Taux de succés'=>"", 
                                    'Objectif'=>0,
                                    'TRO%'=>  ""
                                ];
            
            $facture = collect([]);
            $i=0;           
            foreach ($superviseur as $super) {
                    $SUM_RB30=0; 
                    $SUM_PET33=0;
                    $SUM_RB100=0;  
                    $SUM_PET1=0;
                    $SUM_PET15=0; 
                    $SUM_PET2=0;
                    $SUM_can=0;
                    $SUM_VISITES_AVEC_VENTE=0; 
                    $SUM_VISITES_PROGRAMMEES=0;
                    $SUM_VISITES_realise=0; 
                    $RB30=0; 
                    $PET33=0;
                    $RB100=0;  
                    $PET1=0;
                    $PET15=0; 
                    $PET2=0;
                    $can=0;
                    $VISITES_AVEC_VENTE=0;  
                    $VISITES_PROGRAMMEES=0;
                    $VISITES_realise=0;    
                    
                $depot_super=$super->depositaire_superviseurs;
                //$routep=Route::where('name_route','LIKE','%'.$request->search2.'%')->get();
                $ii=0;
                foreach ($depot_super as $du) 
                {
                    $routep=$du->routes;
                    foreach ($routep as $r) 
                    {
                        $RB30=0; 
                        $PET33=0;
                        $RB100=0;  
                        $PET1=0;
                        $PET15=0; 
                        $PET2=0;
                        $can=0;
                        $VISITES_AVEC_VENTE=0;  
                        $VISITES_PROGRAMMEES=0;
                        $VISITES_realise=0;    
                       
                /*->whereDate('date_ajout','LIKE','%'.$request->search1.'%')->get()*/
                        $head_body=$du->head_daily_ventes->where('route_id','=',$r->id);
                        
                        foreach ($head_body as $h_b) {
                            $month=date("m", strtotime(NOW()));
                			$facture1=body_daily_vente::whereBody_id($h_b->id)->whereMonth('date_ajout','=',$month)->get();
                            $counter=$facture1->count();
                            if($counter==0) continue;
                            foreach ($facture1 as $fact) 
                            {      
                                    $RB30=$fact->RB30 + $RB30;
                                    $month=date("Y-m", strtotime($fact->date_ajout));
                                    $objectif=entete_obj::where('date','=',$month)->whereType_route($h_b->route->type_route)->first();
                                    $PET33=$fact->PET0_33 + $PET33;
                                    $RB100=$fact->RB100 + $RB100;  
                                    $PET1=$fact->PET_1 + $PET1;
                                    $PET15=$fact->PET1_5+ $PET15; 
                                    $PET2=$fact->PET_2+ $PET2;
                                    $can=$fact->can+ $can;
                                    $name_superviseur= $super->name_superviseur;
                                    $SUM_RB30=$SUM_RB30 +   $fact->RB30; 
                                    $SUM_PET33=$SUM_PET33   +   $fact->PET0_33;
                                    $SUM_RB100=$SUM_RB100   +  $fact->RB100;  
                                    $SUM_PET1=$SUM_PET1+    $fact->PET_1;
                                    $SUM_PET15=$SUM_PET15   +   $fact->PET1_5; 
                                    $SUM_PET2=$SUM_PET2 +    $fact->PET_2;
                                    $SUM_can=$SUM_can+  $fact->can;
                                    $VISITES_AVEC_VENTE=$fact->VISITES_AVEC_VENTE + $VISITES_AVEC_VENTE; 

                                    $VISITES_PROGRAMMEES=$fact->VISITES_PROGRAMMEES      +   $VISITES_PROGRAMMEES;
                                    $SUM_VISITES_AVEC_VENTE=$fact->VISITES_AVEC_VENTE+ $SUM_VISITES_AVEC_VENTE;  
                                    
                                    $SUM_VISITES_PROGRAMMEES=$fact->VISITES_PROGRAMMEES+$SUM_VISITES_PROGRAMMEES;
                                    $VISITES_realise=$fact->VISITES_realise+$VISITES_realise;

                                    $SUM_VISITES_realise=$fact->VISITES_realise+$SUM_VISITES_realise;
                                    if ($VISITES_PROGRAMMEES==0) {
                        $xl=0;
                    }else $xl=round( $VISITES_AVEC_VENTE / $VISITES_PROGRAMMEES *100,0);
                    if ($VISITES_AVEC_VENTE==0) {
                        $x2=0;
                    }else $x2=round(  
                                                $can *0.33* 24 /5.678 + 
                                                $PET2 *2* 6 /5.678 + 
                                                $PET15*6*1.5/5.678+
                                                $PET1 *6 / 5.678 + 
                                                $PET33 * 0.33 *  12 / 5.678 + 
                                                $RB100 * 12 / 5.678 + 
                                                $RB30 *0.30* 24 / 5.678 
                                                 , 0)/
                                                 $VISITES_AVEC_VENTE;

                    if ($VISITES_AVEC_VENTE==0) {
                        $x3=0;
                    }else                             
                        $x3=round(($can + 
                                                $PET2 + 
                                                $PET15 +  
                                                $PET1 + 
                                                $PET33 + 
                                                $RB100 + 
                                                $RB30) 
                                    /$VISITES_AVEC_VENTE,0);

                    if ($VISITES_AVEC_VENTE==0) {
                        $x4=0;
                    }else                             
                        $x4=$VISITES_PROGRAMMEES/$VISITES_realise*100;     
                    
                                    
                                    
                                    
                                $headers=$fact->headers;
                                $routes=$headers->route->get();
                                foreach ($routes as $route) 
                                {
                                    $prevendeur=$route->prevendeur;
                                    $routes->push($prevendeur);
                                }
                                $headers->push($routes);
                                $sup=$headers->depositaire_superviseur;
                                $superviseurs=$sup->superviseurs;
                                $sup->push($superviseurs);
                                $dipositaires=$sup->dipositaires;
                                $sup->push($dipositaires);
                                $headers->push($sup);
                                $fact->push($headers);
                
                            }
                $userData1[] = [
                                        'CDZ'=> "",
                                        'nom_superviseur' => $name_superviseur,
                                        'Nom route' => $r->name_route,
                                        'RB30' =>$RB30, 
                                        'PET0.33'=>$PET33 ,
                                        'RB100'=>  $RB100, 
                                        'PET1'=>$PET1 ,
                                        'PET1.5'=> $PET15, 
                                        'PET2'=>$PET2 ,
                                        'can'=>$can,
                                        'VISITES_AVEC_VENTE'=>$VISITES_AVEC_VENTE,
                                        'VISITES_PROGRAMMEES'=>$VISITES_PROGRAMMEES,
                                        'VISITES_realise'=>$VISITES_realise,
                                        'Total phy'=>$can +$PET2 +$PET15 + $PET1 +$PET33 +$RB100 +$RB30 ,
                    'Familial'=>round( ($can*0.33*24/5.678) +($PET2 *2* 6 /5.678) +($PET15*6*1.5/5.678)+$PET1 *6 / 5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB100 * 12 / 5.678 +$RB30 *0.30* 24 / 5.678 , 0)-round ( $can *0.33* 24 /5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB30 *0.30* 24 / 5.678 , 0),
                    'Single serve'=>round ( $can *0.33* 24 /5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB30 *0.30* 24 / 5.678, 0),
                    'Taux de succés'=>$xl ,
                    'Taux de visite'=>$x4 ,
                    'Total 80_Z'=> round (  
                                                $can *0.33* 24 /5.678 + 
                                                $PET2 *2* 6 /5.678 + 
                                                $PET15*6*1.5/5.678+
                                                $PET1 *6 / 5.678 + 
                                                $PET33 * 0.33 *  12 / 5.678 + 
                                                $RB100 * 12 / 5.678 + 
                                                $RB30 *0.30* 24 / 5.678 
                                                 , 3),
                                              
                    'Drop size (80Z)'=> $x2,
                                             
                    'Drop size (PHY)'=>$x3,
                                                       
                     
                    
                    'Objectif'=>$objectif->objectif/$objectif->nbr_jour,
                    'TRO%'=>  round (  
                                    $can *0.33* 24 /5.678 + 
                                    $PET2 *2* 6 /5.678 + 
                                    $PET15*6*1.5/5.678+
                                    $PET1 *6 / 5.678 + 
                                    $PET33 * 0.33 *  12 / 5.678 + 
                                    $RB100 * 12 / 5.678 + 
                                    $RB30 *0.30* 24 / 5.678 
                                     , 0)/
                                     $objectif->objectif *100

                                

                                    ];


                    $facture->push($facture1);
                    $i=$i+$facture1->count();
                } 
            }
        
        }

    if($counter!=0){
        if ($SUM_VISITES_PROGRAMMEES==0) {
                        $xl=0;
                    }else $xl=round( $SUM_VISITES_AVEC_VENTE / $SUM_VISITES_PROGRAMMEES *100,0);
                    if ($SUM_VISITES_AVEC_VENTE==0) {
                        $x2=0;
                    }else $x2=round (  
                                                $SUM_can *0.33* 24 /5.678 + 
                                                $SUM_PET2 *2* 6 /5.678 + 
                                                $SUM_PET15*6*1.5/5.678+
                                                $SUM_PET1 *6 / 5.678 + 
                                                $SUM_PET33 * 0.33 *  12 / 5.678 + 
                                                $SUM_RB100 * 12 / 5.678 + 
                                                $SUM_RB30 *0.30* 24 / 5.678 
                                                 , 0)/
                                                 $SUM_VISITES_AVEC_VENTE;

                    if ($SUM_VISITES_AVEC_VENTE==0) {
                        $x3=0;
                    }else                             
                        $x3=round(($SUM_can + 
                                                $SUM_PET2 + 
                                                $SUM_PET15 +  
                                                $SUM_PET1 + 
                                                $SUM_PET33 + 
                                                $SUM_RB100 + 
                                                $SUM_RB30) 
                                    /$SUM_VISITES_AVEC_VENTE,0);

                    if ($SUM_VISITES_AVEC_VENTE==0) {
                        $x4=0;
                    }else                             
                        $x4=$SUM_VISITES_PROGRAMMEES/$SUM_VISITES_realise*100;     
                    
                    $userData1[] = [
                                        'CDZ'=> "",
                                        'nom_superviseur' => "total",
                                        'Nom route' => "",
                                        'RB30' =>$SUM_RB30, 
                                        'PET0.33'=>$SUM_PET33 ,
                                        'RB100'=>  $SUM_RB100, 
                                        'PET1'=>$SUM_PET1 ,
                                        'PET1.5'=> $SUM_PET15, 
                                        'PET2'=>$SUM_PET2 ,
                                        'can'=>$SUM_can , 
                                        'VISITES_AVEC_VENTE'=>$SUM_VISITES_AVEC_VENTE,
                                        'VISITES_PROGRAMMEES'=>$SUM_VISITES_PROGRAMMEES,
                                        'VISITES_realise'=>$SUM_VISITES_realise,
                                        'Total phy'=>$SUM_can +$SUM_PET2 +$SUM_PET15 + $SUM_PET1 +$SUM_PET33 +$SUM_RB100 +$SUM_RB30 ,
                                        'Familial'=>round( ($SUM_can*0.33*24/5.678) +($SUM_PET2 *2* 6 /5.678) +($SUM_PET15*6*1.5/5.678)+$SUM_PET1 *6 / 5.678 +$SUM_PET33 * 0.33 *  12 / 5.678 +$SUM_RB100 * 12 / 5.678 +$SUM_RB30 *0.30* 24 / 5.678 , 0)-round ( $SUM_can *0.33* 24 /5.678 +$SUM_PET33 * 0.33 *  12 / 5.678 +$SUM_RB30 *0.30* 24 / 5.678 , 0),
                                        'Single serve'=>round ( $SUM_can *0.33* 24 /5.678 +$SUM_PET33 * 0.33 *  12 / 5.678 +$SUM_RB30 *0.30* 24 / 5.678, 0),
                                        'Taux de succés'=> $xl ,
                                        'Taux de visite'=>$x4,
                                        'Total 80_Z'=> round (  
                                                $SUM_can *0.33* 24 /5.678 + 
                                                $SUM_PET2 *2* 6 /5.678 + 
                                                $SUM_PET15*6*1.5/5.678+
                                                $SUM_PET1 *6 / 5.678 + 
                                                $SUM_PET33 * 0.33 *  12 / 5.678 + 
                                                $SUM_RB100 * 12 / 5.678 + 
                                                $SUM_RB30 *0.30* 24 / 5.678 
                                                 , 3),
                                              
                                        'Drop size (80Z)'=>$x2,
                                             
                                        'Drop size (PHY)'=>$x3,
                                                       
                    
                                        'Objectif'=>$objectif->objectif/$objectif->nbr_jour,
                                        'TRO%'=>  round (  
                                    $SUM_can *0.33* 24 /5.678 + 
                                    $SUM_PET2 *2* 6 /5.678 + 
                                    $SUM_PET15*6*1.5/5.678+
                                    $SUM_PET1 *6 / 5.678 + 
                                    $SUM_PET33 * 0.33 *  12 / 5.678 + 
                                    $SUM_RB100 * 12 / 5.678 + 
                                    $SUM_RB30 *0.30* 24 / 5.678 
                                     , 0)/
                                     $objectif->objectif *100

                                    ];
                                
                     
    }            
 

       }

        return view('dashboard.home',compact('userData1'));
		
 
}

}