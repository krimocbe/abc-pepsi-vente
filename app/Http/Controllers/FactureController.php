<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\body_daily_vente;
use App\Superviseur;
use Auth;
use App\depositaire_superviseur;
use App\head_daily_vente;
use App\Depositaire;
use App\Route;
use App\entete_obj;
use App\Zone;
use Carbon\Carbon;

class FactureController extends Controller
{
    //
    public function index()
    {
		//find all superviseurs who has email:auth()
		
    $role=Auth::user()->roles->first();
    if($role->name=="Admin"){  
            $superviseur=Superviseur::all();
    }else if($role->name=="Superviseur"){ 
                $superviseur=Superviseur::whereEmail(Auth::user()->email)->get();}
    else if($role->name=="CDF")
    {
                $zone=Zone::whereEmail(Auth::user()->email)->first();
                $superviseur=Superviseur::whereZone_id($zone->id)->get();
    }            
                $userData1[] = [
                                    'CDZ'=> Auth::user()->name,
                                    'nom_superviseur' => "",
                                    'Nom route' => "",
                                    'RB30' =>0, 
                                    'PET0.33'=>0 ,
                                    'RB100'=>  0, 
                                    'PET1'=>0 ,
                                    'PET1.5'=> 0, 
                                    'PET2'=>0 ,
                                    'can'=>0 ,
                                    'VISITES_AVEC_VENTE'=>0,
                                    'VISITES_PROGRAMMEES'=>0,
                                    'VISITES_realise'=>0,
                                    'Total phy'=>0,
                                    'Familial'=>"",
                                    'Single serve'=>"",
                                    'Taux de succés'=>"" ,
                                    'Taux de visite'=>"" ,
                                    'Total 80_Z'=> "",
                                    'Drop size (80Z)'=> "",
                                    'Drop size (PHY)'=>"",
                                    'Taux de succés'=>"", 
                                    'Objectif'=>0,
                                    'TRO%'=>  ""
                                ];
            
            $facture = collect([]);
            $i=0;           
            foreach ($superviseur as $super) {
                    $SUM_RB30=0; 
                    $SUM_PET33=0;
                    $SUM_RB100=0;  
                    $SUM_PET1=0;
                    $SUM_PET15=0; 
                    $SUM_PET2=0;
                    $SUM_can=0;
                    $SUM_VISITES_AVEC_VENTE=0; 
                    $SUM_VISITES_PROGRAMMEES=0;
                    $SUM_VISITES_realise=0; 
                    $RB30=0; 
                    $PET33=0;
                    $RB100=0;  
                    $PET1=0;
                    $PET15=0; 
                    $PET2=0;
                    $can=0;
                    $VISITES_AVEC_VENTE=0;  
                    $VISITES_PROGRAMMEES=0;
                    $VISITES_realise=0;    
                    
                $depot_super=$super->depositaire_superviseurs;
                //$routep=Route::where('name_route','LIKE','%'.$request->search2.'%')->get();
                $ii=0;
                foreach ($depot_super as $du) 
                {
                    $routep=$du->routes;
                    foreach ($routep as $r) 
                    {
                        $RB30=0; 
                        $PET33=0;
                        $RB100=0;  
                        $PET1=0;
                        $PET15=0; 
                        $PET2=0;
                        $can=0;
                        $VISITES_AVEC_VENTE=0;  
                        $VISITES_PROGRAMMEES=0;
                        $VISITES_realise=0;    
                       
                /*->whereDate('date_ajout','LIKE','%'.$request->search1.'%')->get()*/
                        $head_body=$du->head_daily_ventes->where('route_id','=',$r->id);
                        
                        foreach ($head_body as $h_b) {
                            $facture1=body_daily_vente::orderBy('created_at', 'desc')->whereBody_id($h_b->id)->get();
                            $counter=$facture1->count();
                            if($counter==0) continue;
                            foreach ($facture1 as $fact) 
                            {      
                                    $RB30=$fact->RB30 + $RB30;
                                    $month=date("Y-m", strtotime($fact->date_ajout));
                                    $objectif=entete_obj::where('date','=',$month)->whereType_route($h_b->route->type_route)->first();
                                    $PET33=$fact->PET0_33 + $PET33;
                                    $RB100=$fact->RB100 + $RB100;  
                                    $PET1=$fact->PET_1 + $PET1;
                                    $PET15=$fact->PET1_5+ $PET15; 
                                    $PET2=$fact->PET_2+ $PET2;
                                    $can=$fact->can+ $can;
                                    $name_superviseur= $super->name_superviseur;
                                    $SUM_RB30=$SUM_RB30 +   $fact->RB30; 
                                    $SUM_PET33=$SUM_PET33   +   $fact->PET0_33;
                                    $SUM_RB100=$SUM_RB100   +  $fact->RB100;  
                                    $SUM_PET1=$SUM_PET1+    $fact->PET_1;
                                    $SUM_PET15=$SUM_PET15   +   $fact->PET1_5; 
                                    $SUM_PET2=$SUM_PET2 +    $fact->PET_2;
                                    $SUM_can=$SUM_can+  $fact->can;
                                    $VISITES_AVEC_VENTE=$fact->VISITES_AVEC_VENTE + $VISITES_AVEC_VENTE; 

                                    $VISITES_PROGRAMMEES=$fact->VISITES_PROGRAMMEES      +   $VISITES_PROGRAMMEES;
                                    $SUM_VISITES_AVEC_VENTE=$fact->VISITES_AVEC_VENTE+ $SUM_VISITES_AVEC_VENTE;  
                                    
                                    $SUM_VISITES_PROGRAMMEES=$fact->VISITES_PROGRAMMEES+$SUM_VISITES_PROGRAMMEES;
                                    $VISITES_realise=$fact->VISITES_realise+$VISITES_realise;

                                    $SUM_VISITES_realise=$fact->VISITES_realise+$SUM_VISITES_realise;
                                    if ($VISITES_PROGRAMMEES==0) {
                        $xl=0;
                    }else $xl=round( $VISITES_AVEC_VENTE / $VISITES_PROGRAMMEES *100,0);
                    if ($VISITES_AVEC_VENTE==0) {
                        $x2=0;
                    }else $x2=round (  
                                                $can *0.33* 24 /5.678 + 
                                                $PET2 *2* 6 /5.678 + 
                                                $PET15*6*1.5/5.678+
                                                $PET1 *6 / 5.678 + 
                                                $PET33 * 0.33 *  12 / 5.678 + 
                                                $RB100 * 12 / 5.678 + 
                                                $RB30 *0.30* 24 / 5.678 
                                                 , 0)/
                                                 $VISITES_AVEC_VENTE;

                    if ($VISITES_AVEC_VENTE==0) {
                        $x3=0;
                    }else                             
                        $x3=round(($can + 
                                                $PET2 + 
                                                $PET15 +  
                                                $PET1 + 
                                                $PET33 + 
                                                $RB100 + 
                                                $RB30) 
                                    /$VISITES_AVEC_VENTE,0);

                    if ($VISITES_AVEC_VENTE==0) {
                        $x4=0;
                    }else                             
                        $x4=$VISITES_PROGRAMMEES/$VISITES_realise*100;     
                    
                                    
                                    
                                    
                                $headers=$fact->headers;
                                $routes=$headers->route->get();
                                foreach ($routes as $route) 
                                {
                                    $prevendeur=$route->prevendeur;
                                    $routes->push($prevendeur);
                                }
                                $headers->push($routes);
                                $sup=$headers->depositaire_superviseur;
                                $superviseurs=$sup->superviseurs;
                                $sup->push($superviseurs);
                                $dipositaires=$sup->dipositaires;
                                $sup->push($dipositaires);
                                $headers->push($sup);
                                $fact->push($headers);
                
                            }
                $userData1[] = [
                                        'CDZ'=> "",
                                        'nom_superviseur' => $name_superviseur,
                                        'Nom route' => $r->name_route,
                                        'RB30' =>$RB30, 
                                        'PET0.33'=>$PET33 ,
                                        'RB100'=>  $RB100, 
                                        'PET1'=>$PET1 ,
                                        'PET1.5'=> $PET15, 
                                        'PET2'=>$PET2 ,
                                        'can'=>$can,
                                        'VISITES_AVEC_VENTE'=>$VISITES_AVEC_VENTE,
                                        'VISITES_PROGRAMMEES'=>$VISITES_PROGRAMMEES,
                                        'VISITES_realise'=>$VISITES_realise,
                                        'Total phy'=>$can +$PET2 +$PET15 + $PET1 +$PET33 +$RB100 +$RB30 ,
                    'Familial'=>round( ($can*0.33*24/5.678) +($PET2 *2* 6 /5.678) +($PET15*6*1.5/5.678)+$PET1 *6 / 5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB100 * 12 / 5.678 +$RB30 *0.30* 24 / 5.678 , 0)-round ( $can *0.33* 24 /5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB30 *0.30* 24 / 5.678 , 0),
                    'Single serve'=>round ( $can *0.33* 24 /5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB30 *0.30* 24 / 5.678, 0),
                    'Taux de succés'=>$xl ,
                    'Taux de visite'=>$x4 ,
                    'Total 80_Z'=> round (  
                                                $can *0.33* 24 /5.678 + 
                                                $PET2 *2* 6 /5.678 + 
                                                $PET15*6*1.5/5.678+
                                                $PET1 *6 / 5.678 + 
                                                $PET33 * 0.33 *  12 / 5.678 + 
                                                $RB100 * 12 / 5.678 + 
                                                $RB30 *0.30* 24 / 5.678 
                                                 , 3),
                                              
                    'Drop size (80Z)'=> $x2,
                                             
                    'Drop size (PHY)'=>$x3,
                                                       
                     
                    
                    'Objectif'=>$objectif->objectif/$objectif->nbr_jour,
                    'TRO%'=>  round (  
                                    $can *0.33* 24 /5.678 + 
                                    $PET2 *2* 6 /5.678 + 
                                    $PET15*6*1.5/5.678+
                                    $PET1 *6 / 5.678 + 
                                    $PET33 * 0.33 *  12 / 5.678 + 
                                    $RB100 * 12 / 5.678 + 
                                    $RB30 *0.30* 24 / 5.678 
                                     , 0)/
                                     $objectif->objectif *100

                                

                                    ];


                    $facture->push($facture1);
                    $i=$i+$facture1->count();
                } 
            }
        

    if($counter!=0){
        if ($SUM_VISITES_PROGRAMMEES==0) {
                        $xl=0;
                    }else $xl=round( $SUM_VISITES_AVEC_VENTE / $SUM_VISITES_PROGRAMMEES *100,0);
                    if ($SUM_VISITES_AVEC_VENTE==0) {
                        $x2=0;
                    }else $x2=round (  
                                                $SUM_can *0.33* 24 /5.678 + 
                                                $SUM_PET2 *2* 6 /5.678 + 
                                                $SUM_PET15*6*1.5/5.678+
                                                $SUM_PET1 *6 / 5.678 + 
                                                $SUM_PET33 * 0.33 *  12 / 5.678 + 
                                                $SUM_RB100 * 12 / 5.678 + 
                                                $SUM_RB30 *0.30* 24 / 5.678 
                                                 , 0)/
                                                 $SUM_VISITES_AVEC_VENTE;

                    if ($SUM_VISITES_AVEC_VENTE==0) {
                        $x3=0;
                    }else                             
                        $x3=round(($SUM_can + 
                                                $SUM_PET2 + 
                                                $SUM_PET15 +  
                                                $SUM_PET1 + 
                                                $SUM_PET33 + 
                                                $SUM_RB100 + 
                                                $SUM_RB30) 
                                    /$SUM_VISITES_AVEC_VENTE,0);

                    if ($SUM_VISITES_AVEC_VENTE==0) {
                        $x4=0;
                    }else                             
                        $x4=$SUM_VISITES_PROGRAMMEES/$SUM_VISITES_realise*100;     
                    
                    $userData1[] = [
                                        'CDZ'=> "",
                                        'nom_superviseur' => "total",
                                        'Nom route' => "",
                                        'RB30' =>$SUM_RB30, 
                                        'PET0.33'=>$SUM_PET33 ,
                                        'RB100'=>  $SUM_RB100, 
                                        'PET1'=>$SUM_PET1 ,
                                        'PET1.5'=> $SUM_PET15, 
                                        'PET2'=>$SUM_PET2 ,
                                        'can'=>$SUM_can , 
                                        'VISITES_AVEC_VENTE'=>$SUM_VISITES_AVEC_VENTE,
                                        'VISITES_PROGRAMMEES'=>$SUM_VISITES_PROGRAMMEES,
                                        'VISITES_realise'=>$SUM_VISITES_realise,
                                        'Total phy'=>$SUM_can +$SUM_PET2 +$SUM_PET15 + $SUM_PET1 +$SUM_PET33 +$SUM_RB100 +$SUM_RB30 ,
                                        'Familial'=>round( ($SUM_can*0.33*24/5.678) +($SUM_PET2 *2* 6 /5.678) +($SUM_PET15*6*1.5/5.678)+$SUM_PET1 *6 / 5.678 +$SUM_PET33 * 0.33 *  12 / 5.678 +$SUM_RB100 * 12 / 5.678 +$SUM_RB30 *0.30* 24 / 5.678 , 0)-round ( $SUM_can *0.33* 24 /5.678 +$SUM_PET33 * 0.33 *  12 / 5.678 +$SUM_RB30 *0.30* 24 / 5.678 , 0),
                                        'Single serve'=>round ( $SUM_can *0.33* 24 /5.678 +$SUM_PET33 * 0.33 *  12 / 5.678 +$SUM_RB30 *0.30* 24 / 5.678, 0),
                                        'Taux de succés'=> $xl ,
                                        'Taux de visite'=>$x4,
                                        'Total 80_Z'=> round (  
                                                $SUM_can *0.33* 24 /5.678 + 
                                                $SUM_PET2 *2* 6 /5.678 + 
                                                $SUM_PET15*6*1.5/5.678+
                                                $SUM_PET1 *6 / 5.678 + 
                                                $SUM_PET33 * 0.33 *  12 / 5.678 + 
                                                $SUM_RB100 * 12 / 5.678 + 
                                                $SUM_RB30 *0.30* 24 / 5.678 
                                                 , 3),
                                              
                                        'Drop size (80Z)'=>$x2,
                                             
                                        'Drop size (PHY)'=>$x3,
                                                       
                    
                                        'Objectif'=>$objectif->objectif/$objectif->nbr_jour,
                                        'TRO%'=>  round (  
                                    $SUM_can *0.33* 24 /5.678 + 
                                    $SUM_PET2 *2* 6 /5.678 + 
                                    $SUM_PET15*6*1.5/5.678+
                                    $SUM_PET1 *6 / 5.678 + 
                                    $SUM_PET33 * 0.33 *  12 / 5.678 + 
                                    $SUM_RB100 * 12 / 5.678 + 
                                    $SUM_RB30 *0.30* 24 / 5.678 
                                     , 0)/
                                     $objectif->objectif *100

                                    ];
                                
                     
    }}            
}            
    $i=0;
    $facture = collect([]);
    $factu=collect([]);
    foreach ($superviseur as $super) {
        
        $depot_super=$super->depositaire_superviseurs;
		foreach ($depot_super as $du) {
			$head_body=$du->head_daily_ventes;
            foreach ($head_body as $h_b) {
            	$facture1=body_daily_vente::orderBy('created_at', 'desc')->whereBody_id($h_b->id)->get();
                $month=date("m", strtotime(NOW()));
                
                $facture2=body_daily_vente::whereBody_id($h_b->id)->whereMonth('date_ajout','=',$month)->get();
                $factu->push($facture2);
                
                foreach ($facture1 as $fact) {
    				$headers=$fact->headers;
    				$routes=$headers->route;
    				$prevendeur=$routes->prevendeur;
                    $routes->push($prevendeur);
            		$headers->push($routes);
            		$sup=$headers->depositaire_superviseur;
					$superviseurs=$sup->superviseurs;
            		$sup->push($superviseurs);
            		$dipositaires=$sup->dipositaires;
            		$sup->push($dipositaires);
            		$headers->push($sup);
            		$fact->push($headers);
				}
                if($facture1->count()==0){	
                    continue;
				}
                
				$facture->push($facture1);
				$i=$i+$facture1->count();
        	} 

        }

    } 
        $depot=Depositaire::all()->count();
        $ro=Route::all()->count();
        $sum_RB30 = $factu->sum(function ($region) {
            return $region->sum('RB30');
        });

        $sum_RB100 = $factu->sum(function ($region) {
            return $region->sum('RB100');
        });
        $sum_PET0_33 = $factu->sum(function ($region) {
            return $region->sum('PET0_33');
        });
        $sum_PET_1 = $factu->sum(function ($region) {
            return $region->sum('PET_1');
        });
        $sum_PET1_5 = $factu->sum(function ($region) {
            return $region->sum('PET1_5');
        });
        $sum_PET_2 = $factu->sum(function ($region) {
            return $region->sum('PET_2');
        });
        $sum_can = $factu->sum(function ($region) {
            return $region->sum('can');
        });
        $sum_VISITES_AVEC_VENTE=$factu->sum(function ($region)
        {
            return $region->sum('VISITES_AVEC_VENTE');
        });  
        $sum_VISITES_PROGRAMMEES=$factu->sum(function ($region)
        {
            return $region->sum('VISITES_PROGRAMMEES');
        });
        return view('dashboard.facture.index',compact('facture','i','depot','ro' ,'sum_RB30','sum_RB100','sum_PET0_33','sum_PET_1','sum_PET1_5','sum_PET_2','sum_can','sum_VISITES_AVEC_VENTE','sum_VISITES_PROGRAMMEES','userData1'));

}


    public function fetch()
    {
       $superviseurs = Superviseur::whereEmail( Auth::user()->email )->first();
       $depositives= $superviseurs->depositaires;
       $superviseurs->push($depositives);
       $collection = collect([]);
       foreach ($depositives as $ro) 
        {
            $depo=$ro->depositaire_superviseurs;      
            foreach ($depo as $o) 
            {
                $route=$o->routes;
                foreach($route as $day) 
                {
                    $provendeur=$day->prevendeur;
                    $day->push($provendeur);
                }
                $o->push($day);    
            }
        }

        $facture = collect([]);
        $depot_super=$superviseurs->depositaire_superviseurs;
        foreach ($depot_super as $du) {
            $head_body=$du->head_daily_ventes;
            foreach ($head_body as $h_b) {
                $facture1=body_daily_vente::whereBody_id($h_b->id);
                $facture->push($facture1);
            }
        }
        $sum_RB30 = $facture->sum(function ($region) {
            return $region->sum('RB30');
        });
        $sum_RB100 = $facture->sum(function ($region) {
            return $region->sum('RB100');
        });
        $sum_PET0_33 = $facture->sum(function ($region) {
            return $region->sum('PET0_33');
        });
        $sum_PET_1 = $facture->sum(function ($region) {
            return $region->sum('PET_1');
        });
        $sum_PET1_5 = $facture->sum(function ($region) {
            return $region->sum('PET1_5');
        });
        $sum_PET_2 = $facture->sum(function ($region) {
            return $region->sum('PET_2');
        });
        $sum_can = $facture->sum(function ($region) {
            return $region->sum('can');
        });
        $userData[] = [
                    'sum_RB30' =>$sum_RB30 ,
                    'sum_RB100' => $sum_RB100,
                    'sum_PET0_33' => $sum_PET0_33,
                    'sum_PET_1' =>$sum_PET_1,
                    'sum_PET1_5' => $sum_PET1_5,
                    'sum_PET_2' => $sum_PET_2,
                    'sum_can'=> $sum_can
                ];    
        return response()->json(['status' => true,'depot'=>$depositives,'facture'=>$userData]);
	}

    public function show($facture)
    {
        dd($facture);
    }
    

    public function route()
    {
 
        return view('dashboard.facture.create');  
    }
    
    public function add_many_facture(Request $request)
    {
        foreach ($request->all() as $key) {

            $heads=head_daily_vente::whereDepositaire_superviseur_id($key['id_depot_super'])->whereRoute_id($key['id_route'])->wherePrevendeur_id(1)->first();

        if($heads==null)        
        $heads=head_daily_vente::create([
                'depositaire_superviseur_id'=>$key['id_depot_super'],
                'route_id'=>$key['id_route'],
                'prevendeur_id'=>1
        ]);
        $month=date("Y-m", strtotime($key['Date']));
        $route=Route::find($key['id_route']);
        $objectif_id=entete_obj::whereType_route($route->type_route)->where('date','=',
            $month)->first();
        $body=body_daily_vente::create([
                'date_ajout' => $key['Date'] ,
                'body_id' => $heads->id ,
                'quantite'=>$key['RB30'],
                'RB30'=>$key['RB30'],
                'RB100'=>$key['RB100'],
                'PET0_33'=>$key['PET033'],
                'PET_1'=>$key['PET1'],
                'PET1_5'=>$key['PET15'],
                'PET_2'=>$key['PET2'],
                'VISITES_PROGRAMMEES'=>$key['visite_programmé'],
                'VISITES_AVEC_VENTE'=>$key['visite_vente'],
                'can'=>$key['can'],
                'VISITES_realise'=>$key['visite'],
                'objectif_id'=>$objectif_id->id,
                'type'=>0
            ]);
            # code...
        }
       
        $superviseurs = Superviseur::whereEmail( Auth::user()->email )->first();
        $facture = collect([]);
        $depot_super=$superviseurs->depositaire_superviseurs;
        foreach ($depot_super as $du) {
            $head_body=$du->head_daily_ventes;
            foreach ($head_body as $h_b) {
                $facture1=body_daily_vente::whereBody_id($h_b->id);
                $date=NOW()->format('m');
                $facto=body_daily_vente::whereBody_id($h_b->id)->whereMonth('date_ajout','=',$date);
                $facture->push($facto);
            }
        }
        $sum_RB30 = $facture->sum(function ($region) {
            return $region->sum('RB30');
        });
        $sum_RB100 = $facture->sum(function ($region) {
            return $region->sum('RB100');
        });
        $sum_PET0_33 = $facture->sum(function ($region) {
            return $region->sum('PET0_33');
        });
        $sum_PET_1 = $facture->sum(function ($region) {
            return $region->sum('PET_1');
        });
        $sum_PET1_5 = $facture->sum(function ($region) {
            return $region->sum('PET1_5');
        });
        $sum_PET_2 = $facture->sum(function ($region) {
            return $region->sum('PET_2');
        });
        $sum_can = $facture->sum(function ($region) {
            return $region->sum('can');
        });
        $userData[] = [
                    'sum_RB30' =>$sum_RB30 ,
                    'sum_RB100' => $sum_RB100,
                    'sum_PET0_33' => $sum_PET0_33,
                    'sum_PET_1' =>$sum_PET_1,
                    'sum_PET1_5' => $sum_PET1_5,
                    'sum_PET_2' => $sum_PET_2,
                    'sum_can'=> $sum_can
                ];
        return response()->json([
            'status' => true, 
            'message'=> 'facture bien ajouté',  
            'data'   => $body->headers,
            'facture'=> $userData
        ]);
    }

     public function add_facture( Request $request)
    {
        
        $heads=head_daily_vente::whereDepositaire_superviseur_id($request->id_depot_super)->whereRoute_id($request->id_route)->wherePrevendeur_id(1)->first();
        if($heads==null)        
        $heads=head_daily_vente::create([
                'depositaire_superviseur_id'=>$request->id_depot_super,
                'route_id'=>$request->id_route,
                'prevendeur_id'=>1
        ]);
        $month=date("Y-m", strtotime($request->Date));
        $route=Route::find($request->id_route);
        $objectif_id=entete_obj::whereType_route($route->type_route)->where('date','=',
            $month)->first();
        $body=body_daily_vente::create([
            'date_ajout' => $request->Date ,
            'body_id' => $heads->id ,
            'quantite'=>$request->RB30,
            'RB30'=>$request->RB30,
            'RB100'=>$request->RB100,
            'PET0_33'=>$request->PET033,
            'PET_1'=>$request->PET1,
            'PET1_5'=>$request->PET15,
            'PET_2'=>$request->PET2,
            'VISITES_PROGRAMMEES'=>$request->visite_programmé,
            'VISITES_AVEC_VENTE'=>$request->visite_vente,
            'can'=>$request->can,
            'VISITES_realise'=>$request->visite,
            'objectif_id'=>$objectif_id->id,
            'type'=>0
         ]);
        return response()->json([
            'status' => true, 
            'message'=> 'facture bien ajouté',  
            'data'   => $body->headers
        ]);
    
    }
    


    public function store(Request $request)
    {
    	
        $id=head_daily_vente::whereDepositaire_superviseur_id($request->id_depot_super)->whereRoute_id($request->id_route)->wherePrevendeur_id(1)->first();
        if($id==null)        
        $id=head_daily_vente::create([
                'depositaire_superviseur_id'=>$request->id_depot_super,
                'route_id'=>$request->id_route,
                'prevendeur_id'=>1
        ]);
        
         
        $month=date("Y-m", strtotime($request->date));
        $route=Route::find($request->route);
        $objectif_id=entete_obj::whereType_route($route->type_route)->where('date','=',
            $month)->first();
        $body=body_daily_vente::create([
  			'date_ajout' => $request->date ,
  			'body_id' => $id->id ,
  			'quantite'=>$request->RB30,
            'RB30'=>$request->RB30,
            'RB100'=>$request->RB100,
            'PET0_33'=>$request->PET0_33,
            'PET_1'=>$request->PET1,
            'PET1_5'=>$request->PET1_5,
            'PET_2'=>$request->PET2,
            'VISITES_PROGRAMMEES'=>$request->nbr_client_visite,
            'VISITES_AVEC_VENTE'=>$request->nbr_client_visite_vente,
            'VISITES_realise'=>$request->VISITES_realise,
            'can'=>$request->can,
            'VISITES_realise'=>$request->VISITES_realise,
            'objectif_id'=>$objectif_id->id,
            'type'=>0
        
		]);
		return redirect()->route('Facture.index');
   
    }

    public function edit($id)
    {
    	$body=body_daily_vente::whereId($id)->first();
    	$head=$body->headers;
    	$name_route=$head->route->name_route;
    	$body->push($name_route);
		$depot_name=$head->depositaire_superviseur->dipositaires->name_dipositaire;
        $body->push($depot_name);
        $role=Auth::user()->roles->first();
        if($role->name=="Admin"){  
            $superviseur=$head->depositaire_superviseur->superviseurs;
        }else{ 
            $superviseur=Superviseur::whereEmail(Auth::user()->email)->first();
        }    
        $depositaire=$superviseur->depositaires;
        $items = array();
        $items_route= array();
        foreach ($depositaire as $depot) {
			
			$depot_super=depositaire_superviseur::whereDepositaire_id($depot->id)->whereSuperviseur_id($superviseur->id)->first();
			$items[$depot_super->id] = $depot->name_dipositaire;
        	$route=$depot_super->routes;
        	foreach ($route as $item_route) {
				$items_route[$item_route->id] = $item_route->name_route;
            }
            
        }
	    
	    return \View::make('dashboard.facture.edit', compact('items',$items,'items_route',$items_route,'body','name_route','depot_name','id')); 
	}

    public function upd_facture(Request $request,$id)
    {
    	$fac=body_daily_vente::whereId($id)->first();
    	$head=head_daily_vente::whereId($fac->body_id)->first();
    	$head->update([
    		'depositaire_superviseur_id'=>$request->text,
            'route_id'=>$request->route,
            'prevendeur_id'=>1
    	]);
    	$route=Route::find($request->route);
        $objectif_id=entete_obj::whereType_route($route->type_route)->where('date','=',
            $month)->first();
    	$fac->update([
    		'date_ajout' => $request->date ,
  			'body_id' => $head->id ,
  			'quantite'=>$request->RB30,
            'RB30'=>$request->RB30,
            'RB100'=>$request->RB100,
            'PET0_33'=>$request->PET0_33,
            'PET_1'=>$request->PET1,
            'PET1_5'=>$request->PET1_5,
            'PET_2'=>$request->PET2,
            'VISITES_PROGRAMMEES'=>$request->nbr_client_visite,
            'VISITES_realise'=>$request->VISITES_realise,
            'VISITES_AVEC_VENTE'=>$request->nbr_client_visite_vente,
            'can'=>$request->can,
            'objectif_id'=>$objectif_id->id
            
    	]);
    	return redirect()->route('Facture.index');
   
    	
	}

	public function get_products($id)
	{
		$date = date("Y-m-d");//  ici ta date
		$date = strtotime(date("Y-m-d", strtotime($date)) . " +5 day");  
		$fac=body_daily_vente::whereId($id)->first();
        $head=$fac->headers;
        $objectif=entete_obj::whereId($fac->objectif_id)->whereType_route($head->route->type_route)->first();
        
		return view('dashboard.facture.show_products',compact('fac','objectif'));
    

    }
    
    public function update()
    {
    	$superviseur=Superviseur::whereEmail(Auth::user()->email)->first(); 
        $depositaire=$superviseur->depositaires;
        $items = array();
        $items_route= array();
        foreach ($depositaire as $depot) {
			
			$depot_super=depositaire_superviseur::whereDepositaire_id($depot->id)->whereSuperviseur_id($superviseur->id)->first();
			$items[$depot_super->id] = $depot->name_dipositaire;
        	$route=$depot_super->routes;
        	foreach ($route as $item_route) {
				$items_route[$item_route->id] = $item_route->name_route;
            }
            
        }
	    
	    return \View::make('dashboard.facture.create', compact('items',$items,'items_route',$items_route));

    }

    public function destroy($id)
    {
        
        body_daily_vente::destroy($id);
		return redirect()->route('Facture.index');
    }

    public function supprimer($id)
    {
    	# code...
    	body_daily_vente::destroy($id);
		return redirect()->route('Facture.index');
        
    }

}
