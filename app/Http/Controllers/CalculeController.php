<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\body_daily_vente;
use App\Superviseur;
use Auth;
use App\depositaire_superviseur;
use App\head_daily_vente;
use App\Route;
use Maatwebsite\Excel\Facades\Excel;
use App\User;
use App\Zone;
use App\entete_obj;
use Carbon\Carbon;

class CalculeController extends Controller
{
    //
    public function search(Request $request)
	{
 
		if($request->ajax())
 		{

 		$output="";
		$superviseur=Superviseur::whereEmail(Auth::user()->email)->first(); 
		$depot_super=$superviseur->depositaire_superviseurs;
		$facture = collect([]);
		$i=0;
		foreach ($depot_super as $du)
		{
			$head_body=$du->head_daily_ventes;
            foreach ($head_body as $h_b) {
            	$facture1=body_daily_vente::orderBy('created_at', 'desc')->whereBody_id($h_b->id)->get();
				foreach ($facture1 as $fact) 
				{
    				$headers=$fact->headers;
    				$routes=$headers->route->where('name_route','LIKE','%'.mb_strtolower($request->search)."%")->get();

    				
    				foreach ($routes as $route) 
    				{
    					$prevendeur=$route->prevendeur;
           	 			$routes->push($prevendeur);
            			$headers->push($routes);
            		}
            		$sup=$headers->depositaire_superviseur;
					$superviseurs=$sup->superviseurs;
            		$sup->push($superviseurs);
            		$dipositaires=$sup->dipositaires;
            		$sup->push($dipositaires);
            		$headers->push($sup);
            		$fact->push($headers);
				}
	            
	            if($facture1->count()==0)
	            {	
					continue;
				}
				$facture->push($facture1);
				$i=$i+$facture1->count();
        	} 
        }
        foreach ($facture as $facture1) {
            foreach ($facture1 as $fact) {
               
					$output.=
					'<tr>'.
                             '<td>'.'<input type="checkbox" id="scales" name="feature"
               value="{{ .$fact->id. }}"  />'.'</td>'. 
						'<td>'.$fact.'</td>'.
 						'<td>'.$fact->headers['depositaire_superviseur']['dipositaires']['name_dipositaire'].'</td>'.
 						'<td>'.$fac->headers['depositaire_superviseur']['dipositaires']['name_dipositaire'].'</td>'.
 						'<td>'.$fact->headers['route']['name_route'].'</td>'.
						'<td>'.$fact->headers['prevendeur']['name_prevendeur'].'</td>'.
						'<td>'.$fact->VISITES_PROGRAMMEES.'</td>'.
						'<td>'.$fact->VISITES_AVEC_VENTE.'</td>'.
						'<td>'.'						
                    			<a href="/dashboard/Facture/'.$fact->id.'/edit"'.'class="btn btn-primary btn-xs"><i
                                class="fa fa-pencil"></i></a>
                   	 			<a href="/dashboard/show_products/'.$fact->id.'" class="btn btn-primary btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a>

                    			<a href="/dashboard/supprimer/'.$fact->id.'" class="btn btn-primary btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a>
                     
                    			<form method="POST" action="/dashboard/Facture/'.$fact->id.'  style="display:inline"><input name="_method" type="hidden" value="DELETE">
                     				<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<button type="submit" class="btn btn-danger btn-xs cur-p"><i class="fa 		fa-trash"></i></button>
								</form>
                     
                    	 </td>'.
 
 
						'</tr>';
 				}
			}
        	return Response($output);
        }
    }

         public function search_between_date(Request $request)
    {
 
        if($request->ajax())
 
        {
            $output="";
            $timestamp = strtotime($request->search1);
            $role=Auth::user()->roles->first();
            if($role->name=="Admin"){
                $superviseur=Superviseur::where('name_superviseur','LIKE','%'.$request->search2.'%')->get();
            }else{ 
                $superviseur=Superviseur::whereEmail(Auth::user()->email)->get();
            }
            $facture = collect([]);
            $i=0;           
            foreach ($superviseur as $super) {
                $depot_super=$super->depositaire_superviseurs;
                $routep=Route::where('name_route','LIKE','%'.$request->search.'%')->get();
                foreach ($depot_super as $du) 
                {
                    foreach ($routep as $r) 
                    {
                /*->whereDate('date_ajout','LIKE','%'.$request->search1.'%')->get()*/
                        $head_body=$du->head_daily_ventes->where('route_id','=',$r->id);
                        foreach ($head_body as $h_b) {
                            $facture1=body_daily_vente::orderBy('created_at', 'desc')->whereBody_id($h_b->id)->WhereBetweenRaw("CAST(date_ajout AS TEXT) ILIKE '%$request->search1%'")->get();
                            foreach ($facture1 as $fact) 
                            {
                                $headers=$fact->headers;
                                $routes=$headers->route->where('name_route','LIKE','%'.$request->search.'%')->get();
                                foreach ($routes as $route) 
                                {
                                    $prevendeur=$route->prevendeur;
                                    $routes->push($prevendeur);
                                }
                                $headers->push($routes);
                                $sup=$headers->depositaire_superviseur;
                                $superviseurs=$sup->superviseurs;
                                $sup->push($superviseurs);
                                $dipositaires=$sup->dipositaires;
                                $sup->push($dipositaires);
                                $headers->push($sup);
                                $fact->push($headers);
                
                            }
                
                    if($facture1->count()==0)
                    {   

                        continue;
                    }

                    $facture->push($facture1);
                    $i=$i+$facture1->count();
                } 
            }
        }
    }

            foreach ($facture as $facture1) {
               foreach ($facture1 as $fact) {

                $timestamp = strtotime($fact->date_ajout);
                $userData[] = [
                    'Date' => date('d-m-Y', $timestamp),
                    'Name_depot' => $fact->headers['depositaire_superviseur']['dipositaires']['name_dipositaire'],
                    'name_superviseur' => $fact->headers['depositaire_superviseur']['superviseurs']['name_superviseur'],
                    'name_route' => $fact->headers['route']['name_route'],
                    'name_prevendeur' => $fact->headers['route']['prevendeur']['name_prevendeur'],
                ];  
               
                    $output.='<tr>'.
 
                        
                    
                        '<td>'.'<input type="checkbox" id="scales" name="feature"
                            value="{{ .$fact->id. }}"  />'.'</td>'.

                        '<td>'.date('d-m-Y', $timestamp).'</td>'.
 
                        '<td>'.$fact->headers['depositaire_superviseur']['dipositaires']['name_dipositaire'].'</td>'.
 
                        '<td>'.$fact->headers['depositaire_superviseur']['superviseurs']['name_superviseur'].'</td>'.
 
                        '<td>'.$fact->headers['route']['name_route'].'</td>'.

                        '<td>'.$fact->headers['route']['prevendeur']['name_prevendeur'].'</td>'.

                        '<td>'.$fact->VISITES_PROGRAMMEES.'</td>'.

                        '<td>'.$fact->VISITES_AVEC_VENTE.'</td>'.

                        '<td>'.'                        
                                <a href="/dashboard/Facture/'.$fact->id.'/edit"'.'class="btn btn-primary btn-xs"><i
                                            class="fa fa-pencil"></i></a>
                                <a href="/dashboard/show_products/'.$fact->id.'" class="btn btn-primary btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a>

                                <a href="/dashboard/supprimer/'.$fact->id.'" class="btn btn-danger btn-xs cur-p"><i class="fa fa-trash" aria-hidden="true"></i></a>
                     
                                </td>'.
 
 
                        '</tr>';
 
                }
            }

            return Response($output);
   }
    
}


     public function search_date(Request $request)
	{
 
		if($request->ajax())
 
		{
            $output="";
        	
            $timestamp = strtotime($request->search1);
            $role=Auth::user()->roles->first();
    		if($role->name=="Admin"){
    		    $superviseur=Superviseur::where('name_superviseur','LIKE','%'.$request->search2.'%')->get();
    		
            }else if($role->name=="Superviseur"){ 
                $superviseur=Superviseur::whereEmail(Auth::user()->email)->get();
            }else if($role->name=="CDF"){
                $zone=Zone::whereEmail(Auth::user()->email)->first();
                $superviseur=Superviseur::where('name_superviseur','LIKE','%'.$request->search2.'%')->whereZone_id($zone->id)->get();
            }
    		$facture = collect([]);
    		$i=0;		 	
        	foreach ($superviseur as $super) {
        		$depot_super=$super->depositaire_superviseurs;
				$routep=Route::where('name_route','LIKE','%'.$request->search.'%')->get();
				foreach ($depot_super as $du) 
				{
					foreach ($routep as $r) 
					{
				/*->whereDate('date_ajout','LIKE','%'.$request->search1.'%')->get()*/
						$head_body=$du->head_daily_ventes->where('route_id','=',$r->id);
						foreach ($head_body as $h_b) {
                            if($request->search3 == null)
            				    $facture1=body_daily_vente::orderBy('created_at', 'desc')->whereBody_id($h_b->id)->WhereRaw("CAST(date_ajout AS TEXT) ILIKE '%$request->search1%'")->get();
            		        else {
                                /*$fromDate = new Carbon($request->search1); 
                                $toDate = new Carbon($request->search3); 
                                */
                                $fromDate = date($request->search1); 
                                $toDate = date($request->search3);
                                    
                                $facture1=body_daily_vente::whereBody_id($h_b->id)->WhereBetween("date_ajout",array($fromDate, $toDate))->get();
                            } 
            				foreach ($facture1 as $fact) 
            				{
    							$headers=$fact->headers;
    							$routes=$headers->route->where('name_route','LIKE','%'.$request->search.'%')->get();
    							foreach ($routes as $route) 
    							{
    								$prevendeur=$route->prevendeur;
           	 						$routes->push($prevendeur);
           	 					}
           	 					$headers->push($routes);
            					$sup=$headers->depositaire_superviseur;
								$superviseurs=$sup->superviseurs;
            					$sup->push($superviseurs);
            					$dipositaires=$sup->dipositaires;
            					$sup->push($dipositaires);
            					$headers->push($sup);
            					$fact->push($headers);
				
                			}
	            
	            	if($facture1->count()==0)
	            	{	

	            		continue;
					}

					$facture->push($facture1);
					$i=$i+$facture1->count();
        		} 
         	}
    	}
	}
                                    


           	foreach ($facture as $facture1) {
               foreach ($facture1 as $fact) {

               	$timestamp = strtotime($fact->date_ajout);
				$userData[] = [
                    'Date' => date('d-m-Y', $timestamp),
                    'Name_depot' => $fact->headers['depositaire_superviseur']['dipositaires']['name_dipositaire'],
                    'name_superviseur' => $fact->headers['depositaire_superviseur']['superviseurs']['name_superviseur'],
                    'name_route' => $fact->headers['route']['name_route'],
                    'name_prevendeur' => $fact->headers['route']['prevendeur']['name_prevendeur'],
                ];  

                if($fact->type==1)

                 $str='<a href="/dashboard/show_products/'.$fact->id.'" class="btn btn-primary btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a>

                                <a href="/dashboard/supprimer/'.$fact->id.'" class="btn btn-danger btn-xs cur-p"><i class="fa fa-trash" aria-hidden="true"></i></a>
                     
                                </td>';
                elseif ($fact->type==0) {
                        $str='<a href="/dashboard/Facture/'.$fact->id.'/edit"'.'class="btn btn-primary btn-xs"><i
                                            class="fa fa-pencil"></i></a>
                                <a href="/dashboard/show_products/'.$fact->id.'" class="btn btn-primary btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a>

                                <a href="/dashboard/supprimer/'.$fact->id.'" class="btn btn-danger btn-xs cur-p"><i class="fa fa-trash" aria-hidden="true"></i></a>
                     
                                </td>';            # code...
                                }                
               
					$output.='<tr>'.
 
						
                    
                        '<td>'.'<input type="checkbox" id="scales" name="feature"
                            value="{{ .$fact->id. }}"  />'.'</td>'.

                        '<td>'.date('d-m-Y', $timestamp).'</td>'.
 
						'<td>'.$fact->headers['depositaire_superviseur']['dipositaires']['name_dipositaire'].'</td>'.
 
						'<td>'.$fact->headers['depositaire_superviseur']['superviseurs']['name_superviseur'].'</td>'.
 
						'<td>'.$fact->headers['route']['name_route'].'</td>'.


                        '<td>'.$fact->headers['route']['type_route'].'</td>'.

						'<td>'.$fact->headers['route']['prevendeur']['name_prevendeur'].'</td>'.

						'<td>'.$fact->VISITES_PROGRAMMEES.'</td>'.

						'<td>'.$fact->VISITES_AVEC_VENTE.'</td>'.

                        '<td>'.$str.'</td>'.
 
 
						'</tr>';
 
				}
			}

            return Response($output);
   }
    
}

public function imprimer(Request $request)
    {
 
        
            $output="";
            $counter=0;
            $timestamp = strtotime($request->search1);

            $role=Auth::user()->roles->first();
            if($role->name=="Admin"){
                $superviseur=Superviseur::where('name_superviseur','LIKE','%'.$request->search1.'%')->get();

            $facture = collect([]);
            $i=0;           
            foreach ($superviseur as $super) {
                    
                $depot_super=$super->depositaire_superviseurs;
                $routep=Route::where('name_route','LIKE','%'.$request->search2.'%')->get();
                
                foreach ($depot_super as $du) 
                {

                    foreach ($routep as $r) 
                    {
                        $head_body=$du->head_daily_ventes->where('route_id','=',$r->id);
                        
                        foreach ($head_body as $h_b) {
                            if($request->search3 == null)
                            {
                                $facture1=body_daily_vente::orderBy('created_at', 'desc')->whereBody_id($h_b->id)->WhereRaw("CAST(date_ajout AS TEXT) ILIKE '%$request->search3%'")->Where('type','=',0)->get();
                                
                               body_daily_vente::orderBy('created_at', 'desc')->whereBody_id($h_b->id)->WhereRaw("CAST(date_ajout AS TEXT) ILIKE '%$request->search3%'")->Where('type','=',0)->update([
                                               'type'=>1         
                               ]);
                               } 
                            else {
                                $fromDate = date($request->search3); 
                                $toDate = date($request->search4);
                                $facture1=body_daily_vente::whereBody_id($h_b->id)->WhereBetween("date_ajout",array($fromDate, $toDate))->WhereType(0)->update([
                                    'type'=>1
                                    ])->get();
                                
                            }

                            $counter=$facture1->count();
                            if($counter==0) continue;
                            foreach ($facture1 as $fact) 
                            {      
                                    $RB30=$fact->RB30 ;
                                    $month=date("Y-m", strtotime($fact->date_ajout));
                                    $objectif=entete_obj::where('date','=',$month)->whereType_route($h_b->route->type_route)->first();
                                    $PET33=$fact->PET0_33  ;
                                    $RB100=$fact->RB100  ;  
                                    $PET1=$fact->PET_1 ;
                                    $PET15=$fact->PET1_5 ; 
                                    $PET2=$fact->PET_2;
                                    $can=$fact->can;
                                    $name_superviseur= $super->name_superviseur;
                                    $SUM_RB30=$fact->RB30; 
                                    $SUM_PET33= $fact->PET0_33;
                                    $SUM_RB100= $fact->RB100;  
                                    $SUM_PET1=  $fact->PET_1;
                                    $SUM_PET15=   $fact->PET1_5; 
                                    $SUM_PET2=    $fact->PET_2;
                                    $SUM_can=  $fact->can;
                                    $VISITES_AVEC_VENTE=$fact->VISITES_AVEC_VENTE; 

                                    $VISITES_PROGRAMMEES=$fact->VISITES_PROGRAMMEES;
                                    $SUM_VISITES_AVEC_VENTE=$fact->VISITES_AVEC_VENTE;  
                                    
                                    $SUM_VISITES_PROGRAMMEES=$fact->VISITES_PROGRAMMEES;
                                    $VISITES_realise=$fact->VISITES_realise;

                                    $SUM_VISITES_realise=$fact->VISITES_realise;
                                    if ($VISITES_PROGRAMMEES==0) {
                                        $xl=0;
                    }else $xl=round( $VISITES_AVEC_VENTE / $VISITES_PROGRAMMEES *100,0);
                    if ($VISITES_AVEC_VENTE==0) {
                        $x2=0;
                    }else $x2=round (  
                                                $can *0.33* 24 /5.678 + 
                                                $PET2 *2* 6 /5.678 + 
                                                $PET15*6*1.5/5.678+
                                                $PET1 *6 / 5.678 + 
                                                $PET33 * 0.33 *  12 / 5.678 + 
                                                $RB100 * 12 / 5.678 + 
                                                $RB30 *0.30* 24 / 5.678 
                                                 , 0)/
                                                 $VISITES_AVEC_VENTE;

                    if ($VISITES_AVEC_VENTE==0) {
                        $x3=0;
                    }else                             
                        $x3=round(($can + 
                                                $PET2 + 
                                                $PET15 +  
                                                $PET1 + 
                                                $PET33 + 
                                                $RB100 + 
                                                $RB30) 
                                    /$VISITES_AVEC_VENTE,0);

                    if ($VISITES_AVEC_VENTE==0) {
                        $x4=0;
                    }else                             
                        $x4=$VISITES_PROGRAMMEES/$VISITES_realise*100;     
                    
                                    
                                    
                                    
                                $headers=$fact->headers;
                                $routes=$headers->route->where('name_route','LIKE','%'.$request->search.'%')->get();
                                foreach ($routes as $route) 
                                {
                                    $prevendeur=$route->prevendeur;
                                    $routes->push($prevendeur);
                                }
                                $headers->push($routes);
                                $sup=$headers->depositaire_superviseur;
                                $superviseurs=$sup->superviseurs;
                                $sup->push($superviseurs);
                                $dipositaires=$sup->dipositaires;
                                $sup->push($dipositaires);
                                $headers->push($sup);
                                $fact->push($headers);
                
                            
                $userData1[] = [
                                        'CDZ'=> "",
                                        'nom_superviseur' => $name_superviseur,
                                        'Nom route' => $r->name_route,
                                        'RB30' =>$RB30, 
                                        'PET0.33'=>$PET33 ,
                                        'RB100'=>  $RB100, 
                                        'PET1'=>$PET1 ,
                                        'PET1.5'=> $PET15, 
                                        'PET2'=>$PET2 ,
                                        'can'=>$can,
                                        'VISITES_AVEC_VENTE'=>$VISITES_AVEC_VENTE,
                                        'VISITES_PROGRAMMEES'=>$VISITES_PROGRAMMEES,
                                        'VISITES_realise'=>$VISITES_realise,
                                        'Total phy'=>$can +$PET2 +$PET15 + $PET1 +$PET33 +$RB100 +$RB30 ,
                    'Familial'=>round( ($can*0.33*24/5.678) +($PET2 *2* 6 /5.678) +($PET15*6*1.5/5.678)+$PET1 *6 / 5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB100 * 12 / 5.678 +$RB30 *0.30* 24 / 5.678 , 0)-round ( $can *0.33* 24 /5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB30 *0.30* 24 / 5.678 , 0),
                    'Single serve'=>round ( $can *0.33* 24 /5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB30 *0.30* 24 / 5.678, 0),
                    'Taux de succés'=>$xl ,
                    'Taux de visite'=>$x4 ,
                    'Total 80_Z'=> round (  
                                                $can *0.33* 24 /5.678 + 
                                                $PET2 *2* 6 /5.678 + 
                                                $PET15*6*1.5/5.678+
                                                $PET1 *6 / 5.678 + 
                                                $PET33 * 0.33 *  12 / 5.678 + 
                                                $RB100 * 12 / 5.678 + 
                                                $RB30 *0.30* 24 / 5.678 
                                                 , 3),
                                              
                    'Drop size (80Z)'=> $x2,
                                             
                    'Drop size (PHY)'=>$x3,
                                                       
                     
                    
                    'Objectif_jour'=>$objectif->objectif/$objectif->nbr_jour,
                    'TRO%'=>  round (  
                                    $can *0.33* 24 /5.678 + 
                                    $PET2 *2* 6 /5.678 + 
                                    $PET15*6*1.5/5.678+
                                    $PET1 *6 / 5.678 + 
                                    $PET33 * 0.33 *  12 / 5.678 + 
                                    $RB100 * 12 / 5.678 + 
                                    $RB30 *0.30* 24 / 5.678 
                                     , 0)/
                                     $objectif->objectif *100

                                

                                    ];


                    $facture->push($facture1);
                    $i=$i+$facture1->count();
                } 
            }
        }
    }
}
}
if($role->name=="Superviseur"){ 
                $superviseur=Superviseur::whereEmail(Auth::user()->email)->get();

 $facture = collect([]);
            $i=0;           
            foreach ($superviseur as $super) {
                    
                $depot_super=$super->depositaire_superviseurs;
                $routep=Route::where('name_route','LIKE','%'.$request->search2.'%')->get();
                
                foreach ($depot_super as $du) 
                {

                    foreach ($routep as $r) 
                    {
                        $head_body=$du->head_daily_ventes->where('route_id','=',$r->id);
                        
                        foreach ($head_body as $h_b) {
                            if($request->search3 == null)
                                $facture1=body_daily_vente::orderBy('created_at', 'desc')->whereBody_id($h_b->id)->WhereRaw("CAST(date_ajout AS TEXT) ILIKE '%$request->search3%'")->get();
                                
                            else {
                                $fromDate = date($request->search3); 
                                $toDate = date($request->search4);
                                $facture1=body_daily_vente::whereBody_id($h_b->id)->WhereBetween("date_ajout",array($fromDate, $toDate))->get();
                                
                            }

                            $counter=$facture1->count();
                            if($counter==0) continue;
                            foreach ($facture1 as $fact) 
                            {      
                                    $RB30=$fact->RB30 ;
                                    $month=date("Y-m", strtotime($fact->date_ajout));
                                    $objectif=entete_obj::where('date','=',$month)->whereType_route($h_b->route->type_route)->first();
                                    $PET33=$fact->PET0_33  ;
                                    $RB100=$fact->RB100  ;  
                                    $PET1=$fact->PET_1 ;
                                    $PET15=$fact->PET1_5 ; 
                                    $PET2=$fact->PET_2;
                                    $can=$fact->can;
                                    $name_superviseur= $super->name_superviseur;
                                    $SUM_RB30=$fact->RB30; 
                                    $SUM_PET33= $fact->PET0_33;
                                    $SUM_RB100= $fact->RB100;  
                                    $SUM_PET1=  $fact->PET_1;
                                    $SUM_PET15=   $fact->PET1_5; 
                                    $SUM_PET2=    $fact->PET_2;
                                    $SUM_can=  $fact->can;
                                    $VISITES_AVEC_VENTE=$fact->VISITES_AVEC_VENTE; 

                                    $VISITES_PROGRAMMEES=$fact->VISITES_PROGRAMMEES;
                                    $SUM_VISITES_AVEC_VENTE=$fact->VISITES_AVEC_VENTE;  
                                    
                                    $SUM_VISITES_PROGRAMMEES=$fact->VISITES_PROGRAMMEES;
                                    $VISITES_realise=$fact->VISITES_realise;

                                    $SUM_VISITES_realise=$fact->VISITES_realise;
                                    if ($VISITES_PROGRAMMEES==0) {
                                        $xl=0;
                    }else $xl=round( $VISITES_AVEC_VENTE / $VISITES_PROGRAMMEES *100,0);
                    if ($VISITES_AVEC_VENTE==0) {
                        $x2=0;
                    }else $x2=round (  
                                                $can *0.33* 24 /5.678 + 
                                                $PET2 *2* 6 /5.678 + 
                                                $PET15*6*1.5/5.678+
                                                $PET1 *6 / 5.678 + 
                                                $PET33 * 0.33 *  12 / 5.678 + 
                                                $RB100 * 12 / 5.678 + 
                                                $RB30 *0.30* 24 / 5.678 
                                                 , 0)/
                                                 $VISITES_AVEC_VENTE;

                    if ($VISITES_AVEC_VENTE==0) {
                        $x3=0;
                    }else                             
                        $x3=round(($can + 
                                                $PET2 + 
                                                $PET15 +  
                                                $PET1 + 
                                                $PET33 + 
                                                $RB100 + 
                                                $RB30) 
                                    /$VISITES_AVEC_VENTE,0);

                    if ($VISITES_AVEC_VENTE==0) {
                        $x4=0;
                    }else                             
                        $x4=$VISITES_PROGRAMMEES/$VISITES_realise*100;     
                    
                                    
                                    
                                    
                                $headers=$fact->headers;
                                $routes=$headers->route->where('name_route','LIKE','%'.$request->search.'%')->get();
                                foreach ($routes as $route) 
                                {
                                    $prevendeur=$route->prevendeur;
                                    $routes->push($prevendeur);
                                }
                                $headers->push($routes);
                                $sup=$headers->depositaire_superviseur;
                                $superviseurs=$sup->superviseurs;
                                $sup->push($superviseurs);
                                $dipositaires=$sup->dipositaires;
                                $sup->push($dipositaires);
                                $headers->push($sup);
                                $fact->push($headers);
                
                            
                $userData1[] = [
                                        'CDZ'=> "",
                                        'nom_superviseur' => $name_superviseur,
                                        'Nom route' => $r->name_route,
                                        'RB30' =>$RB30, 
                                        'PET0.33'=>$PET33 ,
                                        'RB100'=>  $RB100, 
                                        'PET1'=>$PET1 ,
                                        'PET1.5'=> $PET15, 
                                        'PET2'=>$PET2 ,
                                        'can'=>$can,
                                        'VISITES_AVEC_VENTE'=>$VISITES_AVEC_VENTE,
                                        'VISITES_PROGRAMMEES'=>$VISITES_PROGRAMMEES,
                                        'VISITES_realise'=>$VISITES_realise,
                                        'Total phy'=>$can +$PET2 +$PET15 + $PET1 +$PET33 +$RB100 +$RB30 ,
                    'Familial'=>round( ($can*0.33*24/5.678) +($PET2 *2* 6 /5.678) +($PET15*6*1.5/5.678)+$PET1 *6 / 5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB100 * 12 / 5.678 +$RB30 *0.30* 24 / 5.678 , 0)-round ( $can *0.33* 24 /5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB30 *0.30* 24 / 5.678 , 0),
                    'Single serve'=>round ( $can *0.33* 24 /5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB30 *0.30* 24 / 5.678, 0),
                    'Taux de succés'=>$xl ,
                    'Taux de visite'=>$x4 ,
                    'Total 80_Z'=> round (  
                                                $can *0.33* 24 /5.678 + 
                                                $PET2 *2* 6 /5.678 + 
                                                $PET15*6*1.5/5.678+
                                                $PET1 *6 / 5.678 + 
                                                $PET33 * 0.33 *  12 / 5.678 + 
                                                $RB100 * 12 / 5.678 + 
                                                $RB30 *0.30* 24 / 5.678 
                                                 , 3),
                                              
                    'Drop size (80Z)'=> $x2,
                                             
                    'Drop size (PHY)'=>$x3,
                                                       
                     
                    
                    'Objectif_jour'=>$objectif->objectif/$objectif->nbr_jour,
                    'TRO%'=>  round (  
                                    $can *0.33* 24 /5.678 + 
                                    $PET2 *2* 6 /5.678 + 
                                    $PET15*6*1.5/5.678+
                                    $PET1 *6 / 5.678 + 
                                    $PET33 * 0.33 *  12 / 5.678 + 
                                    $RB100 * 12 / 5.678 + 
                                    $RB30 *0.30* 24 / 5.678 
                                     , 0)/
                                     $objectif->objectif *100

                                

                                    ];


                    $facture->push($facture1);
                    $i=$i+$facture1->count();
                } 
            }
        }
    }
}
}












 if($role->name=="CDF"){
                $zone=Zone::whereEmail(Auth::user()->email)->first();
                $superviseur=Superviseur::where('name_superviseur','LIKE','%'.$request->search1.'%')->whereZone_id($zone->id)->get();
                $userData1[] = [
                                    'CDZ'=> Auth::user()->name,
                                    'nom_superviseur' => "",
                                    'Nom route' => "",
                                    'RB30' =>0, 
                                    'PET0.33'=>0 ,
                                    'RB100'=>  0, 
                                    'PET1'=>0 ,
                                    'PET1.5'=> 0, 
                                    'PET2'=>0 ,
                                    'can'=>0 ,
                                    'VISITES_AVEC_VENTE'=>0,
                                    'VISITES_PROGRAMMEES'=>0,
                                    'VISITES_realise'=>0,
                                    'Total phy'=>0,
                                    'Familial'=>"",
                                    'Single serve'=>"",
                                    'Taux de succés'=>"" ,
                                    'Taux de visite'=>"" ,
                                    'Total 80_Z'=> "",
                                    'Drop size (80Z)'=> "",
                                    'Drop size (PHY)'=>"",
                                    'Taux de succés'=>"", 
                                    'Objectif'=>0,
                                    'TRO%'=>  ""
                                ];
            
            $facture = collect([]);
            $i=0;           
            foreach ($superviseur as $super) {
                    $SUM_RB30=0; 
                    $SUM_PET33=0;
                    $SUM_RB100=0;  
                    $SUM_PET1=0;
                    $SUM_PET15=0; 
                    $SUM_PET2=0;
                    $SUM_can=0;
                    $SUM_VISITES_AVEC_VENTE=0; 
                    $SUM_VISITES_PROGRAMMEES=0;
                    $SUM_VISITES_realise=0; 
                    $RB30=0; 
                    $PET33=0;
                    $RB100=0;  
                    $PET1=0;
                    $PET15=0; 
                    $PET2=0;
                    $can=0;
                    $VISITES_AVEC_VENTE=0;  
                    $VISITES_PROGRAMMEES=0;
                    $VISITES_realise=0;    
                    
                $depot_super=$super->depositaire_superviseurs;
                $routep=Route::where('name_route','LIKE','%'.$request->search2.'%')->get();
                
                foreach ($depot_super as $du) 
                {

                    foreach ($routep as $r) 
                    {
                        $RB30=0; 
                        $PET33=0;
                        $RB100=0;  
                        $PET1=0;
                        $PET15=0; 
                        $PET2=0;
                        $can=0;
                        $VISITES_AVEC_VENTE=0;  
                        $VISITES_PROGRAMMEES=0;
                        $VISITES_realise=0;    
                       
                /*->whereDate('date_ajout','LIKE','%'.$request->search1.'%')->get()*/
                        $head_body=$du->head_daily_ventes->where('route_id','=',$r->id);
                        
                        foreach ($head_body as $h_b) {
                            if($request->search3 == null)
                                $facture1=body_daily_vente::orderBy('created_at', 'desc')->whereBody_id($h_b->id)->WhereRaw("CAST(date_ajout AS TEXT) ILIKE '%$request->search3%'")->get();
                                
                            else {
                                $fromDate = date($request->search3); 
                                $toDate = date($request->search4);
                                $facture1=body_daily_vente::whereBody_id($h_b->id)->WhereBetween("date_ajout",array($fromDate, $toDate))->get();
                                
                            }

                            $counter=$facture1->count();
                            if($counter==0) continue;
                            foreach ($facture1 as $fact) 
                            {      
                                    $RB30=$fact->RB30 + $RB30;
                                    $month=date("Y-m", strtotime($fact->date_ajout));
                                    $objectif=entete_obj::where('date','=',$month)->whereType_route($h_b->route->type_route)->first();
                                    $PET33=$fact->PET0_33 + $PET33;
                                    $RB100=$fact->RB100 + $RB100;  
                                    $PET1=$fact->PET_1 + $PET1;
                                    $PET15=$fact->PET1_5+ $PET15; 
                                    $PET2=$fact->PET_2+ $PET2;
                                    $can=$fact->can+ $can;
                                    $name_superviseur= $super->name_superviseur;
                                    $SUM_RB30=$SUM_RB30 +   $fact->RB30; 
                                    $SUM_PET33=$SUM_PET33   +   $fact->PET0_33;
                                    $SUM_RB100=$SUM_RB100   +  $fact->RB100;  
                                    $SUM_PET1=$SUM_PET1+    $fact->PET_1;
                                    $SUM_PET15=$SUM_PET15   +   $fact->PET1_5; 
                                    $SUM_PET2=$SUM_PET2 +    $fact->PET_2;
                                    $SUM_can=$SUM_can+  $fact->can;
                                    $VISITES_AVEC_VENTE=$fact->VISITES_AVEC_VENTE + $VISITES_AVEC_VENTE; 

                                    $VISITES_PROGRAMMEES=$fact->VISITES_PROGRAMMEES      +   $VISITES_PROGRAMMEES;
                                    $SUM_VISITES_AVEC_VENTE=$fact->VISITES_AVEC_VENTE+ $SUM_VISITES_AVEC_VENTE;  
                                    
                                    $SUM_VISITES_PROGRAMMEES=$fact->VISITES_PROGRAMMEES+$SUM_VISITES_PROGRAMMEES;
                                    $VISITES_realise=$fact->VISITES_realise+$VISITES_realise;

                                    $SUM_VISITES_realise=$fact->VISITES_realise+$SUM_VISITES_realise;
                                    if ($VISITES_PROGRAMMEES==0) {
                        $xl=0;
                    }else $xl=round( $VISITES_AVEC_VENTE / $VISITES_PROGRAMMEES *100,0);
                    if ($VISITES_AVEC_VENTE==0) {
                        $x2=0;
                    }else $x2=round (  
                                                $can *0.33* 24 /5.678 + 
                                                $PET2 *2* 6 /5.678 + 
                                                $PET15*6*1.5/5.678+
                                                $PET1 *6 / 5.678 + 
                                                $PET33 * 0.33 *  12 / 5.678 + 
                                                $RB100 * 12 / 5.678 + 
                                                $RB30 *0.30* 24 / 5.678 
                                                 , 0)/
                                                 $VISITES_AVEC_VENTE;

                    if ($VISITES_AVEC_VENTE==0) {
                        $x3=0;
                    }else                             
                        $x3=round(($can + 
                                                $PET2 + 
                                                $PET15 +  
                                                $PET1 + 
                                                $PET33 + 
                                                $RB100 + 
                                                $RB30) 
                                    /$VISITES_AVEC_VENTE,0);

                    if ($VISITES_AVEC_VENTE==0) {
                        $x4=0;
                    }else                             
                        $x4=$VISITES_PROGRAMMEES/$VISITES_realise*100;     
                    
                                    
                                    
                                    
                                $headers=$fact->headers;
                                $routes=$headers->route->where('name_route','LIKE','%'.$request->search.'%')->get();
                                foreach ($routes as $route) 
                                {
                                    $prevendeur=$route->prevendeur;
                                    $routes->push($prevendeur);
                                }
                                $headers->push($routes);
                                $sup=$headers->depositaire_superviseur;
                                $superviseurs=$sup->superviseurs;
                                $sup->push($superviseurs);
                                $dipositaires=$sup->dipositaires;
                                $sup->push($dipositaires);
                                $headers->push($sup);
                                $fact->push($headers);
                
                            }
                $userData1[] = [
                                        'CDZ'=> "",
                                        'nom_superviseur' => $name_superviseur,
                                        'Nom route' => $r->name_route,
                                        'RB30' =>$RB30, 
                                        'PET0.33'=>$PET33 ,
                                        'RB100'=>  $RB100, 
                                        'PET1'=>$PET1 ,
                                        'PET1.5'=> $PET15, 
                                        'PET2'=>$PET2 ,
                                        'can'=>$can,
                                        'VISITES_AVEC_VENTE'=>$VISITES_AVEC_VENTE,
                                        'VISITES_PROGRAMMEES'=>$VISITES_PROGRAMMEES,
                                        'VISITES_realise'=>$VISITES_realise,
                                        'Total phy'=>$can +$PET2 +$PET15 + $PET1 +$PET33 +$RB100 +$RB30 ,
                    'Familial'=>round( ($can*0.33*24/5.678) +($PET2 *2* 6 /5.678) +($PET15*6*1.5/5.678)+$PET1 *6 / 5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB100 * 12 / 5.678 +$RB30 *0.30* 24 / 5.678 , 0)-round ( $can *0.33* 24 /5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB30 *0.30* 24 / 5.678 , 0),
                    'Single serve'=>round ( $can *0.33* 24 /5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB30 *0.30* 24 / 5.678, 0),
                    'Taux de succés'=>$xl ,
                    'Taux de visite'=>$x4 ,
                    'Total 80_Z'=> round (  
                                                $can *0.33* 24 /5.678 + 
                                                $PET2 *2* 6 /5.678 + 
                                                $PET15*6*1.5/5.678+
                                                $PET1 *6 / 5.678 + 
                                                $PET33 * 0.33 *  12 / 5.678 + 
                                                $RB100 * 12 / 5.678 + 
                                                $RB30 *0.30* 24 / 5.678 
                                                 , 3),
                                              
                    'Drop size (80Z)'=> $x2,
                                             
                    'Drop size (PHY)'=>$x3,
                                                       
                     
                    
                    'Objectif'=>$objectif->objectif/$objectif->nbr_jour,
                    'TRO%'=>  round (  
                                    $can *0.33* 24 /5.678 + 
                                    $PET2 *2* 6 /5.678 + 
                                    $PET15*6*1.5/5.678+
                                    $PET1 *6 / 5.678 + 
                                    $PET33 * 0.33 *  12 / 5.678 + 
                                    $RB100 * 12 / 5.678 + 
                                    $RB30 *0.30* 24 / 5.678 
                                     , 0)/
                                     $objectif->objectif *100

                                

                                    ];


                    $facture->push($facture1);
                    $i=$i+$facture1->count();
                } 
            }
        

    if($counter!=0){
        if ($SUM_VISITES_PROGRAMMEES==0) {
                        $xl=0;
                    }else $xl=round( $SUM_VISITES_AVEC_VENTE / $SUM_VISITES_PROGRAMMEES *100,0);
                    if ($SUM_VISITES_AVEC_VENTE==0) {
                        $x2=0;
                    }else $x2=round (  
                                                $SUM_can *0.33* 24 /5.678 + 
                                                $SUM_PET2 *2* 6 /5.678 + 
                                                $SUM_PET15*6*1.5/5.678+
                                                $SUM_PET1 *6 / 5.678 + 
                                                $SUM_PET33 * 0.33 *  12 / 5.678 + 
                                                $SUM_RB100 * 12 / 5.678 + 
                                                $SUM_RB30 *0.30* 24 / 5.678 
                                                 , 0)/
                                                 $SUM_VISITES_AVEC_VENTE;

                    if ($SUM_VISITES_AVEC_VENTE==0) {
                        $x3=0;
                    }else                             
                        $x3=round(($SUM_can + 
                                                $SUM_PET2 + 
                                                $SUM_PET15 +  
                                                $SUM_PET1 + 
                                                $SUM_PET33 + 
                                                $SUM_RB100 + 
                                                $SUM_RB30) 
                                    /$SUM_VISITES_AVEC_VENTE,0);

                    if ($SUM_VISITES_AVEC_VENTE==0) {
                        $x4=0;
                    }else                             
                        $x4=$SUM_VISITES_PROGRAMMEES/$SUM_VISITES_realise*100;     
                    
                    $userData1[] = [
                                        'CDZ'=> "",
                                        'nom_superviseur' => "total",
                                        'Nom route' => "",
                                        'RB30' =>$SUM_RB30, 
                                        'PET0.33'=>$SUM_PET33 ,
                                        'RB100'=>  $SUM_RB100, 
                                        'PET1'=>$SUM_PET1 ,
                                        'PET1.5'=> $SUM_PET15, 
                                        'PET2'=>$SUM_PET2 ,
                                        'can'=>$SUM_can , 
                                        'VISITES_AVEC_VENTE'=>$SUM_VISITES_AVEC_VENTE,
                                        'VISITES_PROGRAMMEES'=>$SUM_VISITES_PROGRAMMEES,
                                        'VISITES_realise'=>$SUM_VISITES_realise,
                                        'Total phy'=>$SUM_can +$SUM_PET2 +$SUM_PET15 + $SUM_PET1 +$SUM_PET33 +$SUM_RB100 +$SUM_RB30 ,
                                        'Familial'=>round( ($SUM_can*0.33*24/5.678) +($SUM_PET2 *2* 6 /5.678) +($SUM_PET15*6*1.5/5.678)+$SUM_PET1 *6 / 5.678 +$SUM_PET33 * 0.33 *  12 / 5.678 +$SUM_RB100 * 12 / 5.678 +$SUM_RB30 *0.30* 24 / 5.678 , 0)-round ( $SUM_can *0.33* 24 /5.678 +$SUM_PET33 * 0.33 *  12 / 5.678 +$SUM_RB30 *0.30* 24 / 5.678 , 0),
                                        'Single serve'=>round ( $SUM_can *0.33* 24 /5.678 +$SUM_PET33 * 0.33 *  12 / 5.678 +$SUM_RB30 *0.30* 24 / 5.678, 0),
                                        'Taux de succés'=> $xl ,
                                        'Taux de visite'=>$x4,
                                        'Total 80_Z'=> round (  
                                                $SUM_can *0.33* 24 /5.678 + 
                                                $SUM_PET2 *2* 6 /5.678 + 
                                                $SUM_PET15*6*1.5/5.678+
                                                $SUM_PET1 *6 / 5.678 + 
                                                $SUM_PET33 * 0.33 *  12 / 5.678 + 
                                                $SUM_RB100 * 12 / 5.678 + 
                                                $SUM_RB30 *0.30* 24 / 5.678 
                                                 , 3),
                                              
                                        'Drop size (80Z)'=>$x2,
                                             
                                        'Drop size (PHY)'=>$x3,
                                                       
                    
                                        'Objectif'=>$objectif->objectif/$objectif->nbr_jour,
                                        'TRO%'=>  round (  
                                    $SUM_can *0.33* 24 /5.678 + 
                                    $SUM_PET2 *2* 6 /5.678 + 
                                    $SUM_PET15*6*1.5/5.678+
                                    $SUM_PET1 *6 / 5.678 + 
                                    $SUM_PET33 * 0.33 *  12 / 5.678 + 
                                    $SUM_RB100 * 12 / 5.678 + 
                                    $SUM_RB30 *0.30* 24 / 5.678 
                                     , 0)/
                                     $objectif->objectif *100

                                    ];
                                
    }
    }
                                    
}

            foreach ($facture as $facture1) {
               foreach ($facture1 as $fact) {

                $timestamp = strtotime($fact->date_ajout);
                $userData[] = [
                    'Date' => date('d-m-Y', $timestamp),
                    'Name_depot' => $fact->headers['depositaire_superviseur']['dipositaires']['name_dipositaire'],
                    'name_superviseur' => $fact->headers['depositaire_superviseur']['superviseurs']['name_superviseur'],
                    'name_route' => $fact->headers['route']['name_route'],
                    'name_prevendeur' => $fact->headers['route']['prevendeur']['name_prevendeur'],
                ];  
               
                    $output.='<tr>'.
 
                        
                    
                        '<td>'.'<input type="checkbox" id="scales" name="feature"
                            value="{{ .$fact->id. }}"  />'.'</td>'.

                        '<td>'.date('d-m-Y', $timestamp).'</td>'.
 
                        '<td>'.$fact->headers['depositaire_superviseur']['dipositaires']['name_dipositaire'].'</td>'.
 
                        '<td>'.$fact->headers['depositaire_superviseur']['superviseurs']['name_superviseur'].'</td>'.
 
                        '<td>'.$fact->headers['route']['name_route'].'</td>'.

                        '<td>'.$fact->headers['route']['prevendeur']['name_prevendeur'].'</td>'.

                        '<td>'.$fact->VISITES_PROGRAMMEES.'</td>'.

                        '<td>'.$fact->VISITES_AVEC_VENTE.'</td>'.

                        '<td>'.'                        
                                <a href="/dashboard/Facture/'.$fact->id.'/edit"'.'class="btn btn-primary btn-xs"><i
                                            class="fa fa-pencil"></i></a>
                                <a href="/dashboard/show_products/'.$fact->id.'" class="btn btn-primary btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a>

                                <a href="/dashboard/supprimer/'.$fact->id.'" class="btn btn-danger btn-xs cur-p"><i class="fa fa-trash" aria-hidden="true"></i></a>
                     
                                </td>'.
 
 
                        '</tr>';
 
                }
            }

            $total_RB30=0;
                $total_PET33=0;
                $total_RB100=0;
                $total_PET1=0;
                $total_PET15=0;
                $total_PET2=0;
                $total_can=0;
                $total_VISITES_AVEC_VENTE=0;
                $total_VISITES_PROGRAMMEES=0;
                $total_VISITES_realise=0;
                $somme_objectif=0;
                   
                $nombre_somme=0;
                foreach ($userData1 as $user) {
               
                    if ($user['nom_superviseur']== "total") {
                         # code...
                        
                        $total_RB30=$user['RB30']+$total_RB30;
                        $total_PET33=$user['PET0.33']+$total_PET33;
                        $total_RB100=$user['RB100']+$total_RB100;
                        $total_PET1=$user['PET1']+$total_PET1;
                        $total_PET15=$user['PET1.5']+$total_PET15;
                        $total_PET2=$user['PET2']+$total_PET2;
                        $total_can=$user['can']+$total_can;
                        $total_VISITES_PROGRAMMEES=$user['VISITES_PROGRAMMEES']+$total_VISITES_PROGRAMMEES;
                        $total_VISITES_realise=$user['VISITES_realise']+$total_VISITES_realise;
                        $total_VISITES_AVEC_VENTE=$user['VISITES_AVEC_VENTE']+$total_VISITES_AVEC_VENTE;
                        $somme_objectif=$user['Objectif']+$somme_objectif;
                        $nombre_somme=$nombre_somme + 1;
                        

                     }
              }
               if ($total_VISITES_PROGRAMMEES==0) {
                        $xl=0;
                    }else $xl=round( $total_VISITES_AVEC_VENTE / $total_VISITES_PROGRAMMEES *100,0);
                    if ($total_VISITES_AVEC_VENTE==0) {
                        $x2=0;
                    }else $x2=round (  
                                                $total_can *0.33* 24 /5.678 + 
                                                $total_PET2 *2* 6 /5.678 + 
                                                $total_PET15*6*1.5/5.678+
                                                $total_PET1 *6 / 5.678 + 
                                                $total_PET33 * 0.33 *  12 / 5.678 + 
                                                $total_RB100 * 12 / 5.678 + 
                                                $total_RB30 *0.30* 24 / 5.678 
                                                 , 0)/
                                                 $total_VISITES_AVEC_VENTE;

                    if ($total_VISITES_AVEC_VENTE==0) {
                        $x3=0;
                    }else                             
                        $x3=round(($total_can + 
                                                $total_PET2 + 
                                                $total_PET15 +  
                                                $total_PET1 + 
                                                $total_PET33 + 
                                                $total_RB100 + 
                                                $total_RB30) 
                                    /$total_VISITES_AVEC_VENTE,0);

                    if ($total_VISITES_AVEC_VENTE==0) {
                        $x4=0;
                    }else                             
                        $x4=$total_VISITES_PROGRAMMEES/$total_VISITES_realise*100;     
                    
       
    $userData1[] = [
                                'CDZ'=> "Total",
                                'nom_superviseur' => "",
                                'Nom route' =>""  ,
                                'RB30' =>$total_RB30, 
                                'PET0.33'=>$total_PET33 ,
                                'RB100'=>$total_RB100, 
                                'PET1'=>$total_PET1 ,
                                'PET1.5'=>$total_PET15, 
                                'PET2'=>$total_PET2 ,
                                'can'=>$total_can,
                                'VISITES_AVEC_VENTE'=>$total_VISITES_AVEC_VENTE,
                                'VISITES_PROGRAMMEES'=>$total_VISITES_PROGRAMMEES,
                                'VISITES_realise'=>$total_VISITES_realise,
                                'Total phy'=>$total_can +$total_PET2 +$total_PET15 + $total_PET1 +$total_PET33 +$total_RB100 +$total_RB30 ,
                                        'Familial'=>round( ($total_can*0.33*24/5.678) +($total_PET2 *2* 6 /5.678) +($total_PET15*6*1.5/5.678)+$total_PET1 *6 / 5.678 +$total_PET33 * 0.33 *  12 / 5.678 +$total_RB100 * 12 / 5.678 +$total_RB30 *0.30* 24 / 5.678 , 0)-round ( $total_can *0.33* 24 /5.678 +$total_PET33 * 0.33 *  12 / 5.678 +$total_RB30 *0.30* 24 / 5.678 , 0),
                                        'Single serve'=>round ( $total_can *0.33* 24 /5.678 +$total_PET33 * 0.33 *  12 / 5.678 +$total_RB30 *0.30* 24 / 5.678, 0),
                                        'Taux de succés'=> $xl ,
                                        'Taux de visite'=>$x4,
                                        'Total 80_Z'=> round (  
                                                $total_can *0.33* 24 /5.678 + 
                                                $total_PET2 *2* 6 /5.678 + 
                                                $total_PET15*6*1.5/5.678+
                                                $total_PET1 *6 / 5.678 + 
                                                $total_PET33 * 0.33 *  12 / 5.678 + 
                                                $total_RB100 * 12 / 5.678 + 
                                                $total_RB30 *0.30* 24 / 5.678 
                                                 , 3),
                                              
                                        'Drop size (80Z)'=>$x2,
                                             
                                        'Drop size (PHY)'=>$x3,
                                                       
                    
                                        'Objectif'=>$objectif->objectif/$objectif->nbr_jour,
                                        'TRO%'=>  round (  
                                    $total_can *0.33* 24 /5.678 + 
                                    $total_PET2 *2* 6 /5.678 + 
                                    $total_PET15*6*1.5/5.678+
                                    $total_PET1 *6 / 5.678 + 
                                    $total_PET33 * 0.33 *  12 / 5.678 + 
                                    $total_RB100 * 12 / 5.678 + 
                                    $total_RB30 *0.30* 24 / 5.678 
                                     , 0)/
                                     $objectif->objectif *100

                        ];            
}            

                Excel::create('itsolutionstuff_example', function($excel) use ($userData1) {
                    $excel->sheet('mySheet', function($sheet) use ($userData1)
                    {
                        $sheet->fromArray($userData1);
                        for ($j=0; $j <count($userData1) ; $j++) { 
                            if ($userData1[$j]['nom_superviseur']!="")
                            {
                                $k=$j;
                                $sheet->cells('A'.$k.':J'.$k.'', function ($cells) {
                                    $cells->setBackground('#FFFF00');
                                    $cells->setAlignment('center');
                                });   
                            }
                         }
                        
                    });
                    })->download('xlsx'); 
   
    
}

     public function imprimer1(Request $request)
    {
 
            $timestamp = strtotime($request->search1);
            $role=Auth::user()->roles->first();
            if($role->name=="Admin"){
                $superviseur=Superviseur::where('name_superviseur','LIKE','%'.$request->search1.'%')->get();
            }else if($role->name=="Superviseur"){ 
                $superviseur=Superviseur::whereEmail(Auth::user()->email)->get();
            }else if($role->name=="CDF"){
                $zone=Zone::whereEmail(Auth::user()->email)->first();
                $superviseur=Superviseur::whereZone_id($zone->id)->get();
                $userData1[] = [
                                    'CDZ'=> Auth::user()->name,
                                    'nom_superviseur' => "",
                                    'Nom route' => "",
                                    'RB30' =>0, 
                                    'PET0.33'=>0 ,
                                    'RB100'=>  0, 
                                    'PET1'=>0 ,
                                    'PET1.5'=> 0, 
                                    'PET2'=>0 ,
                                    'can'=>0 ,
                                    'VISITES_AVEC_VENTE'=>0,
                                    'VISITES_PROGRAMMEES'=>0,
                                    'VISITES_realise'=>0,
                                    'Total phy'=>0,
                                    'Familial'=>"",
                                    'Single serve'=>"",
                                    'Taux de succés'=>"" ,
                                    'Taux de visite'=>"" ,
                                    'Total 80_Z'=> "",
                                    'Drop size (80Z)'=> "",
                                    'Drop size (PHY)'=>"",
                                    'Taux de succés'=>"", 
                                    'Objectif'=>0,
                                    'TRO%'=>  ""
                                ];
                foreach ($superviseur as $super) {
                    $j=0;
                    $SUM_RB30=0; 
                    $SUM_PET33=0;
                    $SUM_RB100=0;  
                    $SUM_PET1=0;
                    $SUM_PET15=0; 
                    $SUM_PET2=0;
                    $SUM_can=0;
                    $SUM_VISITES_AVEC_VENTE=0; 
                    $SUM_VISITES_PROGRAMMEES=0;
                    $SUM_VISITES_realise=0; 
                    $RB30=0; 
                        $PET33=0;
                        $RB100=0;  
                        $PET1=0;
                        $PET15=0; 
                        $PET2=0;
                        $can=0;
                        $VISITES_AVEC_VENTE=0;  
                        $VISITES_PROGRAMMEES=0;
                        $VISITES_realise=0;    
                        



                    $depot_super=$super->depositaire_superviseurs;
                    foreach ($depot_super as $d_s) {
                        $routes=$d_s->routes;
                        
                        $d_s->push($routes);
                        $i=0;
                        $RB30=0; 
                        $PET33=0;
                        $RB100=0;  
                        $PET1=0;
                        $PET15=0; 
                        $PET2=0;
                        $can=0;
                        $VISITES_AVEC_VENTE=0;  
                        $VISITES_PROGRAMMEES=0;
                        $VISITES_realise=0;    
                         
                        $counter=0;
                        foreach ($routes as $route) {
                        $RB30=0; 
                                    $PET33=0;
                                    $RB100=0;  
                                    $PET1=0;
                                    $PET15=0; 
                                    $PET2=0;
                                    $can=0;
                                        
                            $head_daily=$route->head_daily_ventes;
                            $route->push($head_daily);
                            if($head_daily==null) continue;
                            foreach($head_daily as $hed )
                            {
                                $fromDate = date($request->search3); 
                                $toDate = date($request->search4);
                                
                                $body=$hed->body_daily_ventes;

                                $hed->push($hed->body_daily_ventes);
                                foreach ($body as $bd) {
                                    $counter++;

                                    $month=date("Y-m", strtotime($bd->date_ajout));
                                    
                                    $objectif=entete_obj::where('date','=',$month)->whereType_route($hed->route->type_route)->first();
                                    $timestamp = strtotime($bd->date_ajout);
                                    $RB30=$bd->RB30 + $RB30; 
                                    $PET33=$bd->PET0_33 + $PET33;
                                    $RB100=$bd->RB100 + $RB100;  
                                    $PET1=$bd->PET_1 + $PET1;
                                    $PET15=$bd->PET1_5+ $PET15; 
                                    $PET2=$bd->PET_2+ $PET2;
                                    $can=$bd->can+ $can;
                                    $name_superviseur= $super->name_superviseur;
                                    $SUM_RB30=$SUM_RB30 +   $bd->RB30; 
                                    $SUM_PET33=$SUM_PET33   +   $bd->PET0_33;
                                    $SUM_RB100=$SUM_RB100   +  $bd->RB100;  
                                    $SUM_PET1=$SUM_PET1+    $bd->PET_1;
                                    $SUM_PET15=$SUM_PET15   +   $bd->PET1_5; 
                                    $SUM_PET2=$SUM_PET2 +    $bd->PET_2;
                                    $SUM_can=$SUM_can+  $bd->can;
                                    $VISITES_AVEC_VENTE=$bd->VISITES_AVEC_VENTE + $VISITES_AVEC_VENTE; 

                                    $VISITES_PROGRAMMEES=$bd->VISITES_PROGRAMMEES      +   $VISITES_PROGRAMMEES;
                                    $SUM_VISITES_AVEC_VENTE=$bd->VISITES_AVEC_VENTE+ $SUM_VISITES_AVEC_VENTE;  
                                    
                                    $SUM_VISITES_PROGRAMMEES=$bd->VISITES_PROGRAMMEES+$SUM_VISITES_PROGRAMMEES;
                                    $VISITES_realise=$bd->VISITES_realise+$VISITES_realise;

                                    $SUM_VISITES_realise=$bd->VISITES_realise+$SUM_VISITES_realise;
                                    if ($VISITES_PROGRAMMEES==0) {
                        $xl=0;
                    }else $xl=round( $VISITES_AVEC_VENTE / $VISITES_PROGRAMMEES *100,0);
                    if ($VISITES_AVEC_VENTE==0) {
                        $x2=0;
                    }else $x2=round (  
                                                $can *0.33* 24 /5.678 + 
                                                $PET2 *2* 6 /5.678 + 
                                                $PET15*6*1.5/5.678+
                                                $PET1 *6 / 5.678 + 
                                                $PET33 * 0.33 *  12 / 5.678 + 
                                                $RB100 * 12 / 5.678 + 
                                                $RB30 *0.30* 24 / 5.678 
                                                 , 0)/
                                                 $VISITES_AVEC_VENTE;

                    if ($VISITES_AVEC_VENTE==0) {
                        $x3=0;
                    }else                             
                        $x3=round(($can + 
                                                $PET2 + 
                                                $PET15 +  
                                                $PET1 + 
                                                $PET33 + 
                                                $RB100 + 
                                                $RB30) 
                                    /$VISITES_AVEC_VENTE,0);

                    if ($VISITES_AVEC_VENTE==0) {
                        $x4=0;
                    }else                             
                        $x4=$VISITES_PROGRAMMEES/$VISITES_realise*100;     
                    
                                    
                                    
                                    
                                /*else{
                                    $userData1[] = [
                                        'CDZ'=> "",
                                        'nom_superviseur' => "",
                                        'Nom route' => $route->name_route,
                                        'RB30' =>$RB30, 
                                        'PET0.33'=>$PET33 ,
                                        'RB100'=>  $RB100, 
                                        'PET1'=>$PET1 ,
                                        'PET1.5'=> $RB30, 
                                        'PET2'=>$PET2 ,
                                        'can'=>$can

                                    ];
                                }
                                   
                                    $userData[$i] = [
                                        'CDZ'=> "",
                                        'nom_superviseur' => $name_superviseur,
                                        'Nom route' => $route->name_route,
                                        'RB30' =>$RB30, 
                                        'PET0.33'=>$PET33 ,
                                        'RB100'=>  $RB100, 
                                        'PET1'=>$PET1 ,
                                        'PET1.5'=> $RB30, 
                                        'PET2'=>$PET2 ,
                                        'can'=>$can ,
                                        'VISITES_AVEC_VENTE'=>$VISITES_AVEC_VENTE,
                                        'VISITES_PROGRAMMEES'=>$VISITES_PROGRAMMEES,
                                        'VISITES_realise'=>$VISITES_realise,
                                        'Total phy'=>$can +$PET2 +$PET15 + $PET1 +$PET33 +$RB100 +$RB30 ,
                    'Familial'=>round( ($can*0.33*24/5.678) +($PET2 *2* 6 /5.678) +($PET15*6*1.5/5.678)+$PET1 *6 / 5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB100 * 12 / 5.678 +$RB30 *0.30* 24 / 5.678 , 0)-round ( $can *0.33* 24 /5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB30 *0.30* 24 / 5.678 , 0),
                    'Single serve'=>round ( $can *0.33* 24 /5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB30 *0.30* 24 / 5.678, 0),
                    'Taux de succés'=>round( $VISITES_AVEC_VENTE / $VISITES_PROGRAMMEES *100,0) ,
                    'Total 80_Z'=> round (  
                                                $can *0.33* 24 /5.678 + 
                                                $PET2 *2* 6 /5.678 + 
                                                $PET15*6*1.5/5.678+
                                                $PET1 *6 / 5.678 + 
                                                $PET33 * 0.33 *  12 / 5.678 + 
                                                $RB100 * 12 / 5.678 + 
                                                $RB30 *0.30* 24 / 5.678 
                                                 , 3),
                                              
                    'Drop size (80Z)'=> round (  
                                                $can *0.33* 24 /5.678 + 
                                                $PET2 *2* 6 /5.678 + 
                                                $PET15*6*1.5/5.678+
                                                $PET1 *6 / 5.678 + 
                                                $PET33 * 0.33 *  12 / 5.678 + 
                                                $RB100 * 12 / 5.678 + 
                                                $RB30 *0.30* 24 / 5.678 
                                                 , 0)/
                                                 $VISITES_AVEC_VENTE,
                                             
                    'Drop size (PHY)'=>round(($can + 
                                                $PET2 + 
                                                $PET15 +  
                                                $PET1 + 
                                                $PET33 + 
                                                $RB100 + 
                                                $RB30) 
                                    /$VISITES_AVEC_VENTE,0),
                                                       
                    'Taux de succés'=>round( $VISITES_AVEC_VENTE / $VISITES_PROGRAMMEES *100,0), 
                    
                    'Objectif'=>$objectif->objectif/$objectif->nbr_jour,
                    'TRO%'=>  round (  
                                    $can *0.33* 24 /5.678 + 
                                    $PET2 *2* 6 /5.678 + 
                                    $PET15*6*1.5/5.678+
                                    $PET1 *6 / 5.678 + 
                                    $PET33 * 0.33 *  12 / 5.678 + 
                                    $RB100 * 12 / 5.678 + 
                                    $RB30 *0.30* 24 / 5.678 
                                     , 0)/
                                     $objectif->objectif *100

                                    ];
                                 */    
                                }
                             $userData1[] = [
                                        'CDZ'=> "",
                                        'nom_superviseur' => $name_superviseur,
                                        'Nom route' => $route->name_route,
                                        'RB30' =>$RB30, 
                                        'PET0.33'=>$PET33 ,
                                        'RB100'=>  $RB100, 
                                        'PET1'=>$PET1 ,
                                        'PET1.5'=> $PET15, 
                                        'PET2'=>$PET2 ,
                                        'can'=>$can,
                                        'VISITES_AVEC_VENTE'=>$VISITES_AVEC_VENTE,
                                        'VISITES_PROGRAMMEES'=>$VISITES_PROGRAMMEES,
                                        'VISITES_realise'=>$VISITES_realise,
                                        'Total phy'=>$can +$PET2 +$PET15 + $PET1 +$PET33 +$RB100 +$RB30 ,
                    'Familial'=>round( ($can*0.33*24/5.678) +($PET2 *2* 6 /5.678) +($PET15*6*1.5/5.678)+$PET1 *6 / 5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB100 * 12 / 5.678 +$RB30 *0.30* 24 / 5.678 , 0)-round ( $can *0.33* 24 /5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB30 *0.30* 24 / 5.678 , 0),
                    'Single serve'=>round ( $can *0.33* 24 /5.678 +$PET33 * 0.33 *  12 / 5.678 +$RB30 *0.30* 24 / 5.678, 0),
                    'Taux de succés'=>$xl ,
                    'Taux de visite'=>$x4 ,
                    'Total 80_Z'=> round (  
                                                $can *0.33* 24 /5.678 + 
                                                $PET2 *2* 6 /5.678 + 
                                                $PET15*6*1.5/5.678+
                                                $PET1 *6 / 5.678 + 
                                                $PET33 * 0.33 *  12 / 5.678 + 
                                                $RB100 * 12 / 5.678 + 
                                                $RB30 *0.30* 24 / 5.678 
                                                 , 3),
                                              
                    'Drop size (80Z)'=> $x2,
                                             
                    'Drop size (PHY)'=>$x3,
                                                       
                     
                    
                    'Objectif'=>$objectif->objectif/$objectif->nbr_jour,
                    'TRO%'=>  round (  
                                    $can *0.33* 24 /5.678 + 
                                    $PET2 *2* 6 /5.678 + 
                                    $PET15*6*1.5/5.678+
                                    $PET1 *6 / 5.678 + 
                                    $PET33 * 0.33 *  12 / 5.678 + 
                                    $RB100 * 12 / 5.678 + 
                                    $RB30 *0.30* 24 / 5.678 
                                     , 0)/
                                     $objectif->objectif *100

                                

                                    ];
                                   

                            }
                            $i++;
                                
                        }
                        $d_s->push($routes);
                        
                    }
                    if($counter!=0){
                    if ($SUM_VISITES_PROGRAMMEES==0) {
                        $xl=0;
                    }else $xl=round( $SUM_VISITES_AVEC_VENTE / $SUM_VISITES_PROGRAMMEES *100,0);
                    if ($SUM_VISITES_AVEC_VENTE==0) {
                        $x2=0;
                    }else $x2=round (  
                                                $SUM_can *0.33* 24 /5.678 + 
                                                $SUM_PET2 *2* 6 /5.678 + 
                                                $SUM_PET15*6*1.5/5.678+
                                                $SUM_PET1 *6 / 5.678 + 
                                                $SUM_PET33 * 0.33 *  12 / 5.678 + 
                                                $SUM_RB100 * 12 / 5.678 + 
                                                $SUM_RB30 *0.30* 24 / 5.678 
                                                 , 0)/
                                                 $SUM_VISITES_AVEC_VENTE;

                    if ($SUM_VISITES_AVEC_VENTE==0) {
                        $x3=0;
                    }else                             
                        $x3=round(($SUM_can + 
                                                $SUM_PET2 + 
                                                $SUM_PET15 +  
                                                $SUM_PET1 + 
                                                $SUM_PET33 + 
                                                $SUM_RB100 + 
                                                $SUM_RB30) 
                                    /$SUM_VISITES_AVEC_VENTE,0);

                    if ($SUM_VISITES_AVEC_VENTE==0) {
                        $x4=0;
                    }else                             
                        $x4=$SUM_VISITES_PROGRAMMEES/$SUM_VISITES_realise*100;     
                    
                    $userData1[] = [
                                        'CDZ'=> "",
                                        'nom_superviseur' => "total",
                                        'Nom route' => "",
                                        'RB30' =>$SUM_RB30, 
                                        'PET0.33'=>$SUM_PET33 ,
                                        'RB100'=>  $SUM_RB100, 
                                        'PET1'=>$SUM_PET1 ,
                                        'PET1.5'=> $SUM_PET15, 
                                        'PET2'=>$SUM_PET2 ,
                                        'can'=>$SUM_can , 
                                        'VISITES_AVEC_VENTE'=>$SUM_VISITES_AVEC_VENTE,
                                        'VISITES_PROGRAMMEES'=>$SUM_VISITES_PROGRAMMEES,
                                        'VISITES_realise'=>$SUM_VISITES_realise,
                                        'Total phy'=>$SUM_can +$SUM_PET2 +$SUM_PET15 + $SUM_PET1 +$SUM_PET33 +$SUM_RB100 +$SUM_RB30 ,
                                        'Familial'=>round( ($SUM_can*0.33*24/5.678) +($SUM_PET2 *2* 6 /5.678) +($SUM_PET15*6*1.5/5.678)+$SUM_PET1 *6 / 5.678 +$SUM_PET33 * 0.33 *  12 / 5.678 +$SUM_RB100 * 12 / 5.678 +$SUM_RB30 *0.30* 24 / 5.678 , 0)-round ( $SUM_can *0.33* 24 /5.678 +$SUM_PET33 * 0.33 *  12 / 5.678 +$SUM_RB30 *0.30* 24 / 5.678 , 0),
                                        'Single serve'=>round ( $SUM_can *0.33* 24 /5.678 +$SUM_PET33 * 0.33 *  12 / 5.678 +$SUM_RB30 *0.30* 24 / 5.678, 0),
                                        'Taux de succés'=> $xl ,
                                        'Taux de visite'=>$x4,
                                        'Total 80_Z'=> round (  
                                                $SUM_can *0.33* 24 /5.678 + 
                                                $SUM_PET2 *2* 6 /5.678 + 
                                                $SUM_PET15*6*1.5/5.678+
                                                $SUM_PET1 *6 / 5.678 + 
                                                $SUM_PET33 * 0.33 *  12 / 5.678 + 
                                                $SUM_RB100 * 12 / 5.678 + 
                                                $SUM_RB30 *0.30* 24 / 5.678 
                                                 , 3),
                                              
                                        'Drop size (80Z)'=>$x2,
                                             
                                        'Drop size (PHY)'=>$x3,
                                                       
                    
                                        'Objectif'=>$objectif->objectif/$objectif->nbr_jour,
                                        'TRO%'=>  round (  
                                    $SUM_can *0.33* 24 /5.678 + 
                                    $SUM_PET2 *2* 6 /5.678 + 
                                    $SUM_PET15*6*1.5/5.678+
                                    $SUM_PET1 *6 / 5.678 + 
                                    $SUM_PET33 * 0.33 *  12 / 5.678 + 
                                    $SUM_RB100 * 12 / 5.678 + 
                                    $SUM_RB30 *0.30* 24 / 5.678 
                                     , 0)/
                                     $objectif->objectif *100

                                    ];
                    $super->push($depot_super);
                   } 
                    //return response($super->depositaire_superviseurs);
                    # code...
                }
                
                $i=0;
                $result=array();
                $k=0;

                $total_RB30=0;
                $total_PET33=0;
                $total_RB100=0;
                $total_PET1=0;
                $total_PET15=0;
                $total_PET2=0;
                $total_can=0;
                $total_VISITES_AVEC_VENTE=0;
                $total_VISITES_PROGRAMMEES=0;
                $total_VISITES_realise=0;
                $somme_objectif=0;
                   
                $nombre_somme=0;
                foreach ($userData1 as $user) {
               
                    if ($user['nom_superviseur']== "total") {
                         # code...
                        $result[] = [
                                'CDZ'=> "",
                                'nom_superviseur' => "total",
                                'Nom route' =>  $user['Nom route'],
                                'RB30' =>$user['RB30'], 
                                'PET0.33'=>$user['PET0.33'] ,
                                'RB100'=>$user['RB100'], 
                                'PET1'=>$user['PET1'] ,
                                'PET1.5'=>$user['PET1.5'], 
                                'PET2'=>$user['PET2'] ,
                                'can'=>$user['can'],
                                'VISITES_AVEC_VENTE'=>$user['VISITES_AVEC_VENTE'],
                                'VISITES_PROGRAMMEES'=>$user['VISITES_PROGRAMMEES'],
                                'VISITES_realise'=>$user['VISITES_realise'],
                                'Total phy'=>$user['Total phy'] ,
                                'Familial'=>$user['Familial'],
                                'Single serve'=>$user['Single serve'],
                                'Taux de succés'=> $user['Taux de succés'],
                                'Total 80_Z'=> $user['Total 80_Z'],
                                'Drop size (80Z)'=> $user['Drop size (80Z)'],
                                'Drop size (PHY)'=>$user['Drop size (PHY)'],
                                'Taux de succés'=>$user['Taux de succés'], 
                                'Objectif'=>$user['Objectif'],
                                'TRO%'=> $user['TRO%']  
                        ];
                        $total_RB30=$user['RB30']+$total_RB30;
                        $total_PET33=$user['PET0.33']+$total_PET33;
                        $total_RB100=$user['RB100']+$total_RB100;
                        $total_PET1=$user['PET1']+$total_PET1;
                        $total_PET15=$user['PET1.5']+$total_PET15;
                        $total_PET2=$user['PET2']+$total_PET2;
                        $total_can=$user['can']+$total_can;
                        $total_VISITES_PROGRAMMEES=$user['VISITES_PROGRAMMEES']+$total_VISITES_PROGRAMMEES;
                        $total_VISITES_realise=$user['VISITES_realise']+$total_VISITES_realise;
                        $total_VISITES_AVEC_VENTE=$user['VISITES_AVEC_VENTE']+$total_VISITES_AVEC_VENTE;
                        $somme_objectif=$user['Objectif']+$somme_objectif;
                        $nombre_somme=$nombre_somme + 1;
                        

                     }
                     elseif(count($result)==0)
                    {
                        $result[0] = [
                            'CDZ'=> Auth::user()->name,
                            'nom_superviseur' =>"",
                            'Nom route' =>"",
                            'RB30' =>0, 
                            'PET0.33'=>0,
                            'RB100'=>0, 
                            'PET1'=>0,
                            'PET1.5'=>0, 
                            'PET2'=>0 ,
                            'can'=>0,
                            'VISITES_AVEC_VENTE'=>"",
                            'VISITES_PROGRAMMEES'=>"",
                            'VISITES_realise'=>"",
                            'Total phy'=>0 ,
                            'Familial'=>0,
                            'Single serve'=>0,
                            'Taux de succés'=>0,
                            'Total 80_Z'=> 0,
                            'Drop size (80Z)'=> 0,
                            'Drop size (PHY)'=>0,
                            'Taux de succés'=>0, 
                            'Objectif'=>0,
                            'TRO%'=> 0    
                        
                        ];
                        $result[1] = [
                            'CDZ'=> "",
                            'nom_superviseur' => $user['nom_superviseur'],
                            'Nom route' =>"",
                            'RB30' =>0, 
                            'PET0.33'=>0 ,
                            'RB100'=>0, 
                            'PET1'=>0,
                            'PET1.5'=>0, 
                            'PET2'=>0 ,
                            'can'=>0,
                            'VISITES_AVEC_VENTE'=>"",
                            'VISITES_PROGRAMMEES'=>"",
                            'VISITES_realise'=>"",
                            'Total phy'=>0 ,
                            'Familial'=>0,
                            'Single serve'=>0,
                            'Taux de succés'=>0,
                            'Total 80_Z'=> 0,
                            'Drop size (80Z)'=> 0,
                            'Drop size (PHY)'=>0,
                            'Taux de succés'=>0, 
                            'Objectif'=>0,
                            'TRO%'=> 0  
                        ];
                        $result[2] = [
                            'CDZ'=> "",
                            'nom_superviseur' => "",
                            'Nom route' =>  $user['Nom route'],
                            'RB30' =>$user['RB30'], 
                            'PET0.33'=>$user['PET0.33'] ,
                            'RB100'=>$user['RB100'], 
                            'PET1'=>$user['PET1'] ,
                            'PET1.5'=>$user['PET1.5'], 
                            'PET2'=>$user['PET2'] ,
                            'can'=>$user['can'],
                            'VISITES_AVEC_VENTE'=>$user['VISITES_AVEC_VENTE'],
                            'VISITES_PROGRAMMEES'=>$user['VISITES_PROGRAMMEES'],
                            'VISITES_realise'=>$user['VISITES_realise'],
                            'Total phy'=>$user['Total phy'] ,
                            'Familial'=>$user['Familial'],
                            'Single serve'=>$user['Single serve'],
                            'Taux de succés'=> $user['Taux de succés'],
                            'Total 80_Z'=> $user['Total 80_Z'],
                            'Drop size (80Z)'=> $user['Drop size (80Z)'],
                            'Drop size (PHY)'=>$user['Drop size (PHY)'],
                            'Taux de succés'=>$user['Taux de succés'], 
                            'Objectif'=>$user['Objectif'],
                            'TRO%'=> $user['TRO%']
                        ];
                       $j++;$i++;
                        continue;
                    }else{   
                    $iter=0;
                    

                    for ($i=0; $i <count($result) ; $i++) { 
                        
                        if($result[$i]['Nom route']==$user['Nom route']) {
                            
                            $k++;
                            $fds=$result[$i]['RB30']+$user['RB30'];
                            $result[$i]['CDZ'] = "";
                            $result[$i]['nom_superviseur'] = "";
                            $result[$i]['Nom route'] = $user['Nom route'];
                            $result[$i]['RB30'] = $fds ; 
                            $result[$i]['PET0.33']=$result[$i]['PET0.33']+$user['PET0.33'];
                            $result[$i]['RB100']=$result[$i]['RB100']+$user['RB100']; 
                            $result[$i]['PET1']=$result[$i]['PET1']+$user['PET1'];
                            $result[$i]['PET1.5']=$result[$i]['PET1.5']+$user['PET1.5']; 
                            $result[$i]['PET2']=$result[$i]['PET2']+$user['PET2'];
                            $result[$i]['can']=$result[$i]['can']+$user['can'];
                            $result[$i]['VISITES_AVEC_VENTE']=$result[$i]['VISITES_AVEC_VENTE']+$user['VISITES_AVEC_VENTE'];
                            $result[$i]['VISITES_PROGRAMMEES']=$result[$i]['VISITES_PROGRAMMEES']+$user['VISITES_PROGRAMMEES'];
                            $result[$i]['VISITES_realise']=$result[$i]['VISITES_realise']+$user['VISITES_realise'];
                            $result[$i]['Total phy']=$result[$i]['Total phy']+$user['Total phy'] ;
                            $result[$i]['Familial']=$result[$i]['Familial']+$user['Familial'];
                            $result[$i]['Single serve']=$result[$i]['Single serve']+$user['Single serve'];
                            $result[$i]['Taux de succés']=$result[$i]['Taux de succés']+$user['Taux de succés'];
                            $result[$i]['Total 80_Z']=$result[$i]['Total 80_Z']+$user['Total 80_Z'];
                            $result[$i]['Drop size (80Z)']=$result[$i]['Drop size (80Z)']+$user['Drop size (80Z)'];
                            $result[$i]['Drop size (PHY)']=$result[$i]['Drop size (PHY)']+$user['Drop size (PHY)'];
                            $result[$i]['Taux de succés']=$result[$i]['Taux de succés']+$user['Taux de succés']; 
                            $result[$i]['Objectif']=$result[$i]['Objectif']+$user['Objectif'];
                            $result[$i]['TRO%']=$result[$i]['TRO%']+$user['TRO%'];
                            break;
                        }
                    }
                    foreach ($result as $rs) {
                        
                        
                        $iter++;
                    }
                    $trouve=0;
                    for ($j=0; $j <count($result) ; $j++) { 
                        if ($user['nom_superviseur'] == $result[$j]['nom_superviseur']){
                            $trouve=1;break;
                        }
                     }
                    
                    
                    if(count($result)<=$i){
                       
                        
                        if($trouve==1){
                            $result[] = [
                                'CDZ'=> "",
                                'nom_superviseur' => "",
                                'Nom route' =>  $user['Nom route'],
                                'RB30' =>$user['RB30'], 
                                'PET0.33'=>$user['PET0.33'] ,
                                'RB100'=>$user['RB100'], 
                                'PET1'=>$user['PET1'] ,
                                'PET1.5'=>$user['PET1.5'], 
                                'PET2'=>$user['PET2'] ,
                                'can'=>$user['can'],
                                'VISITES_AVEC_VENTE'=>$user['VISITES_AVEC_VENTE'],
                                'VISITES_PROGRAMMEES'=>$user['VISITES_PROGRAMMEES'],
                                'VISITES_realise'=>$user['VISITES_realise'],
                                'Total phy'=>$user['Total phy'] ,
                                'Familial'=>$user['Familial'],
                                'Single serve'=>$user['Single serve'],
                                'Taux de succés'=> $user['Taux de succés'],
                                'Total 80_Z'=> $user['Total 80_Z'],
                                'Drop size (80Z)'=> $user['Drop size (80Z)'],
                                'Drop size (PHY)'=>$user['Drop size (PHY)'],
                                'Taux de succés'=>$user['Taux de succés'], 
                                'Objectif'=>$user['Objectif'],
                                'TRO%'=> $user['TRO%']
                        
                            ];
                            
                        }else{
                        $result[] = [
                            'CDZ'=> "",
                            'nom_superviseur' => $user['nom_superviseur'],
                            'Nom route' =>"",
                            'RB30' =>0, 
                            'PET0.33'=>0 ,
                            'RB100'=>0, 
                            'PET1'=>0,
                            'PET1.5'=>0, 
                            'PET2'=>0 ,
                            'can'=>0,
                            'VISITES_AVEC_VENTE'=>"",
                            'VISITES_PROGRAMMEES'=>"",
                            'VISITES_realise'=>"",
                            'Total phy'=>0 ,
                            'Familial'=>0,
                            'Single serve'=>0,
                            'Taux de succés'=>0,
                            'Total 80_Z'=> 0,
                            'Drop size (80Z)'=> 0,
                            'Drop size (PHY)'=>0,
                            'Taux de succés'=>0, 
                            'Objectif'=>0,
                            'TRO%'=> 0  
                        
                        ];
                        $result[] = [
                            'CDZ'=> "",
                            'nom_superviseur' => "",
                            'Nom route' =>  $user['Nom route'],
                            'RB30' =>$user['RB30'], 
                            'PET0.33'=>$user['PET0.33'] ,
                            'RB100'=>$user['RB100'], 
                            'PET1'=>$user['PET1'] ,
                            'PET1.5'=>$user['PET1.5'], 
                            'PET2'=>$user['PET2'] ,
                            'can'=>$user['can'],
                            'VISITES_AVEC_VENTE'=>$user['VISITES_AVEC_VENTE'],
                            'VISITES_PROGRAMMEES'=>$user['VISITES_PROGRAMMEES'],
                            'VISITES_realise'=>$user['VISITES_realise'],
                            'Total phy'=>$user['Total phy'] ,
                            'Familial'=>$user['Familial'],
                            'Single serve'=>$user['Single serve'],
                            'Taux de succés'=> $user['Taux de succés'],
                            'Total 80_Z'=> $user['Total 80_Z'],
                            'Drop size (80Z)'=> $user['Drop size (80Z)'],
                            'Drop size (PHY)'=>$user['Drop size (PHY)'],
                            'Taux de succés'=>$user['Taux de succés'], 
                            'Objectif'=>$user['Objectif'],
                            'TRO%'=> $user['TRO%']
                        ];
                    }
                }

                    
                }
                    
                
            
            }
                    
                    if ($total_VISITES_AVEC_VENTE==0) {
                        $x2=0;
                    }else $x2=round (  
                                                $total_can *0.33* 24 /5.678 + 
                                                $total_PET2 *2* 6 /5.678 + 
                                                $total_PET15*6*1.5/5.678+
                                                $total_PET1 *6 / 5.678 + 
                                                $total_PET33 * 0.33 *  12 / 5.678 + 
                                                $total_RB100 * 12 / 5.678 + 
                                                $total_RB30 *0.30* 24 / 5.678 
                                                 , 0)/
                                                 $total_VISITES_AVEC_VENTE;

                    if ($total_VISITES_AVEC_VENTE==0) {
                        $x3=0;
                    }else                             
                        $x3=round(($total_can + 
                                                $total_PET2 + 
                                                $total_PET15 +  
                                                $total_PET1 + 
                                                $total_PET33 + 
                                                $total_RB100 + 
                                                $total_RB30) 
                                    /$total_VISITES_AVEC_VENTE,0);

                    if ($total_VISITES_AVEC_VENTE==0) {
                        {$x4=0;$x5=0;}
                    }else                             
                       { $x4=$total_VISITES_PROGRAMMEES/$total_VISITES_realise*100;
                        $x5=round(($total_can + 
                                                $total_PET2 + 
                                                $total_PET15 +  
                                                $total_PET1 + 
                                                $total_PET33 + 
                                                $total_RB100 + 
                                                $total_RB30) 
                                    /$total_VISITES_AVEC_VENTE,0);} 
                    if ($total_VISITES_PROGRAMMEES==0) {
                        $xl=0;
                    }else $xl=round( $total_VISITES_AVEC_VENTE / $total_VISITES_PROGRAMMEES *100,0);    
                    $result[] = [
                                'CDZ'=> Auth::user()->name,
                                'nom_superviseur' => "total",
                                'Nom route' =>  "",
                                'RB30' =>$total_RB30, 
                                'PET0.33'=>$total_PET33 ,
                                'RB100'=>$total_RB100, 
                                'PET1'=>$total_PET1 ,
                                'PET1.5'=>$total_PET15, 
                                'PET2'=>$total_PET2 ,
                                'can'=>$total_can,
                                'VISITES_AVEC_VENTE'=>$total_VISITES_AVEC_VENTE,
                                'VISITES_PROGRAMMEES'=>$total_VISITES_PROGRAMMEES,
                                'VISITES_realise'=>$total_VISITES_realise,
                                'Total phy'=>$total_can +$total_PET2 +$total_PET15 + 
                                $total_PET1 +$total_PET33 +$total_RB100 +$total_RB30 ,
                                'Familial'=>round( ($total_can*0.33*24/5.678) +($total_PET2 *2* 6 /5.678) +($total_PET15*6*1.5/5.678)+$total_PET1 *6 / 5.678 +$total_PET33 * 0.33 *  12 / 5.678 +$total_RB100 * 12 / 5.678 +$total_RB30 *0.30* 24 / 5.678 , 0)-round ( $total_can *0.33* 24 /5.678 +$total_PET33 * 0.33 *  12 / 5.678 +$SUM_RB30 *0.30* 24 / 5.678 , 0),
                                'Single serve'=>round ( $total_can *0.33* 24 /5.678 +$total_PET33 * 0.33 *  12 / 5.678 +$total_RB30 *0.30* 24 / 5.678, 0),
                                'Taux de succés'=> $xl,
                                'Taux de visite'=> $x4,
                                'Total 80_Z'=> $x2,
                                'Drop size (80Z)'=> $x3,
                                'Drop size (PHY)'=>$x5,
                                'Objectif'=>$somme_objectif/$nombre_somme,
                                'TRO%'=> round (  
                                    $total_can *0.33* 24 /5.678 + 
                                    $total_PET2 *2* 6 /5.678 + 
                                    $total_PET15*6*1.5/5.678+
                                    $total_PET1 *6 / 5.678 + 
                                    $total_PET33 * 0.33 *  12 / 5.678 + 
                                    $total_RB100 * 12 / 5.678 + 
                                    $total_RB30 *0.30* 24 / 5.678 
                                     , 0)/
                                     ($somme_objectif/$nombre_somme) *100

                        ];


                Excel::create('itsolutionstuff_example', function($excel) use ($result) {
                    $excel->sheet('mySheet', function($sheet) use ($result)
                    {
                        $sheet->fromArray($result);
                        for ($j=0; $j <count($result) ; $j++) { 
                            if ($result[$j]['nom_superviseur']!="")
                            {
                                $k=$j;
                                $sheet->cells('A'.$k.':J'.$k.'', function ($cells) {
                                    $cells->setBackground('#FFFF00');
                                    $cells->setAlignment('center');
                                });   
                            }
                         }
                        
                    });
                    })->download('xlsx');   
                foreach($superviseur as $sup){
                    
                  
                }
                    
            } 
            $facture = collect([]);
            $i=0;           
            foreach ($superviseur as $super) {
                $depot_super=$super->depositaire_superviseurs;
                $routep=Route::where('name_route','LIKE','%'.$request->search2.'%')->get();
                foreach ($depot_super as $du) 
                {
                    foreach ($routep as $r) 
                    {
                /*->whereDate('date_ajout','LIKE','%'.$request->search1.'%')->get()*/
                        $head_body=$du->head_daily_ventes->where('route_id','=',$r->id);
                        foreach ($head_body as $h_b) {
                            
                            if($request->search4 == null)
                                $facture1=body_daily_vente::orderBy('created_at', 'desc')->whereBody_id($h_b->id)->WhereRaw("CAST(date_ajout AS TEXT) ILIKE '%$request->search3%'")->get();
                            else {
                                /*$fromDate = new Carbon($request->search1); 
                                $toDate = new Carbon($request->search3); 
                                */
                                $fromDate = date($request->search3); 
                                $toDate = date($request->search4);
                                    
                                $facture1=body_daily_vente::whereBody_id($h_b->id)->WhereBetween("date_ajout",array($fromDate, $toDate))->get();
                            }
                    
                            foreach ($facture1 as $fact) 
                            {
                                $headers=$fact->headers;
                                $routes=$headers->route->where('name_route','LIKE','%'.$request->search2.'%')->get();
                                foreach ($routes as $route) 
                                {
                                    $prevendeur=$route->prevendeur;
                                    $routes->push($prevendeur);
                                }
                                $headers->push($routes);
                                $sup=$headers->depositaire_superviseur;
                                $superviseurs=$sup->superviseurs;
                                $sup->push($superviseurs);
                                $dipositaires=$sup->dipositaires;
                                $sup->push($dipositaires);
                                $headers->push($sup);
                                $fact->push($headers);
                
                            }
                
                    if($facture1->count()==0)
                    {   

                        continue;
                    }

                    $facture->push($facture1);
                    $i=$i+$facture1->count();
                } 
            }
        }
    }
              
            foreach ($facture as $facture1) {
               foreach ($facture1 as $fac) {
                $head=$fac->headers;
                $objectif=entete_obj::whereId($fac->objectif_id)->whereType_route($head->route->type_route)->first();
                if($fac->VISITES_PROGRAMMEES ==0 )  
                    $x1="0"; 
                    else 
                        $x1=round( $fac->VISITES_AVEC_VENTE / $fac->VISITES_PROGRAMMEES*100,0);
                if($fac->VISITES_AVEC_VENTE ==0 )  
                    {$x2="0";$x3=0;} 
                    else 
                        {$x2=round(($fac->can + 
                                                $fac->PET_2 + 
                                                $fac->PET1_5 +  
                                                $fac->PET_1 + 
                                                $fac->PET0_33 + 
                                                $fac->RB100 + 
                                                $fac->RB30) 
                                    /$fac->VISITES_AVEC_VENTE,0);
                        $x3=round (  
                                                $fac->can *0.33* 24 /5.678 + 
                                                $fac->PET_2 *2* 6 /5.678 + 
                                                $fac->PET1_5*6*1.5/5.678+
                                                $fac->PET_1 *6 / 5.678 + 
                                                $fac->PET0_33 * 0.33 *  12 / 5.678 + 
                                                $fac->RB100 * 12 / 5.678 + 
                                                $fac->RB30 *0.30* 24 / 5.678 
                                                 , 0)/
                                                 $fac->VISITES_AVEC_VENTE;
                }    
                $timestamp = strtotime($fac->date_ajout);
                $userData[] = [
                    'Date' => date('d-m-Y', $timestamp),
                    'Name_depot' => $fac->headers['depositaire_superviseur']['dipositaires']['name_dipositaire'],
                    'name_superviseur' => $fac->headers['depositaire_superviseur']['superviseurs']['name_superviseur'],
                    'name_route' => $fac->headers['route']['name_route'],
                    'name_prevendeur' => $fac->headers['route']['prevendeur']['name_prevendeur'],
                    'RB30' =>$fac->RB30, 
                    'PET0.33'=>$fac->PET0_33 ,
                    'RB100'=>  $fac->RB100, 
                    'PET1'=>$fac->PET_1 ,
                    'PET1.5'=> $fac->RB30, 
                    'PET2'=>$fac->PET_2 ,
                    'can'=>$fac->can, 
                    'Total phy'=>$fac->can +$fac->PET_2 +$fac->PET1_5 + $fac->PET_1 +$fac->PET0_33 +$fac->RB100 +$fac->RB30 ,
                    'Familial'=>round( ($fac->can*0.33*24/5.678) +($fac->PET_2 *2* 6 /5.678) +($fac->PET1_5*6*1.5/5.678)+$fac->PET_1 *6 / 5.678 +$fac->PET0_33 * 0.33 *  12 / 5.678 +$fac->RB100 * 12 / 5.678 +$fac->RB30 *0.30* 24 / 5.678 , 0)-round ( $fac->can *0.33* 24 /5.678 +$fac->PET0_33 * 0.33 *  12 / 5.678 +$fac->RB30 *0.30* 24 / 5.678 , 0),
                    'Single serve'=>round ( $fac->can *0.33* 24 /5.678 +$fac->PET0_33 * 0.33 *  12 / 5.678 +$fac->RB30 *0.30* 24 / 5.678, 0),
                    
                    'Total 80_Z'=> round (  
                                                $fac->can *0.33* 24 /5.678 + 
                                                $fac->PET_2 *2* 6 /5.678 + 
                                                $fac->PET1_5*6*1.5/5.678+
                                                $fac->PET_1 *6 / 5.678 + 
                                                $fac->PET0_33 * 0.33 *  12 / 5.678 + 
                                                $fac->RB100 * 12 / 5.678 + 
                                                $fac->RB30 *0.30* 24 / 5.678 
                                                 , 3),
                                              
                    'Drop size (80Z)'=> $x3,
                                             
                    'Drop size (PHY)'=>$x2,
                                                       
                    'Taux de succés'=>$x1, 
                    
                    'Objectif'=>$objectif->objectif/$objectif->nbr_jour,
                    'TRO%'=>  round (  
                                    $fac->can *0.33* 24 /5.678 + 
                                    $fac->PET_2 *2* 6 /5.678 + 
                                    $fac->PET1_5*6*1.5/5.678+
                                    $fac->PET_1 *6 / 5.678 + 
                                    $fac->PET0_33 * 0.33 *  12 / 5.678 + 
                                    $fac->RB100 * 12 / 5.678 + 
                                    $fac->RB30 *0.30* 24 / 5.678 
                                     , 0)/
                                     $objectif->objectif *100

                                ];  
               
                    
 
                }
            }
       
            Excel::create('itsolutionstuff_example', function($excel) use ($userData) {
            $excel->sheet('mySheet', function($sheet) use ($userData)
            {
                $sheet->fromArray($userData);
            });
            })->download('xlsx');
            return Response($output);
   
    
}





}





                                             
             