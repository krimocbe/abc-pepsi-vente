<?php

Auth::routes();

Route::group([ 'prefix' => 'dashboard','middleware' =>['auth']], function() {
    Route::post('logout', '\App\Http\Controllers\Auth\LoginController@logout');
    Route::get('/','DashboardController@index');
    Route::get('/facture','FactureController@index')->name('facture');
    Route::get('/objectif','ObjectifController@index')->name('objectif');
    Route::get('/objectif1','ObjectifController@index_prime')->name('objectif1');
    
                    
    Route::post('/add_objectif','ObjectifController@store')->name('add_objectif');
    Route::post('/add_one_facture','FactureController@add_facture')->name('add_one_facture');
    Route::post('/add_many_facture','FactureController@add_many_facture')->name('add_many_facture');
    
    Route::post('/imprime_facture','ObjectifController@imprimer')->name('imprime_facture');
    
    Route::post('/export_excel','CalculeController@imprimer')->name('export_excel');
    Route::get('/add_facture','FactureController@route')->name('add_facture');
    
    Route::get('/fetch','FactureController@fetch')->name('fetch');
    Route::resource('Facture','FactureController');
    Route::resource('Objectif','ObjectifController');
    
    Route::get('/show_products/{id}','FactureController@get_products')->name('show_products');
    Route::post('/update_facture/{id}','FactureController@upd_facture')->name('update_facture');
    Route::get('/search','CalculeController@search');
    Route::get('/search1','CalculeController@search_date');
    Route::get('/supprimer/{id}','FactureController@supprimer');
    Route::get('/show_objectif/{id}','ObjectifController@show_objectif')->name('show_objectif');
    Route::post('/update_objectif','ObjectifController@upd_objectif')->name('update_objectif');
    
    
});