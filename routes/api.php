<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


app('debugbar')->disable(); 	

Route::post('register', 'AuthController@register');
Route::post('auth/login', 'AuthController@login');
Route::post('recover', 'AuthController@recover');
Route::post('/auth/client','AuthController@fetch_clients');
Route::post('/auth/facture','AuthController@add_facture');
Route::get('/putzer/{id}','CalculeController@puttozer')->name('putzero1');


