<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Region;
use App\Zone;
use App\Depositaire;
use App\Superviseur;
use App\depositaire_superviseur;
use App\Route;
use App\prevendeur;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $region = new Region();
        $region->name_region="CENTRE";
        $region->responsable_region="abc-pepsi";
        $region->save();

        $zone= new Zone();
        $zone->name_zone="Alger";
        $zone->responsable_zone="4669";
        $zone->email="oussadmohamed@abc-pepsi.com";
        $zone->password=bcrypt('oussad123');
        $zone->region_id=$region->id;
        $zone->save();
        
        $admin = new User();
        $admin->name = 'Oussad';
        $admin->email='oussadmohamed@abc-pepsi.com';
        $admin->password=bcrypt('oussad123');
        $admin->save();
        $admin->attachRole(3);           
 
 
        $admin = new User();
        $admin->name = 'Omar';
        $admin->email='omar@gmail.com';
        $admin->password=bcrypt('omar123');
        $admin->save();
        $admin->attachRole(1);
        
        
        $superviseur= new User();
        $superviseur->name = 'AZRI FOUAD';
        $superviseur->email='AZRI@abc-pepsi.com';
        $superviseur->password=bcrypt('azri123');
        $superviseur->save();
        $superviseur->attachRole(2);
        
        $sup= new Superviseur();
        $sup->name_superviseur="AZRI FOUAD";
        $sup->email="AZRI@abc-pepsi.com";
        $sup->password=bcrypt('azri123');
        $sup->wilaya="ALGER";
        $sup->matricule="5287";
        $sup->zone_id=$zone->id;
        $sup->save();

        $superviseur2= new User();
        $superviseur2->name = 'BEN AMAR MUSTAPHA KAMEL';
        $superviseur2->email='ben@abc-pepsi.com';
        $superviseur2->password=bcrypt('ben123');
        $superviseur2->save();
        $superviseur2->attachRole(2);

        $sup1= new Superviseur();
        $sup1->name_superviseur="BEN AMAR MUSTAPHA KAMEL";
        $sup1->email="ben@abc-pepsi.com";
        $sup1->password=bcrypt('ben123');
        $sup1->wilaya="ALGER";
        $sup1->matricule="4848";
        $sup1->zone_id=$zone->id;
        $sup1->save();


        $superviseur3= new User();
        $superviseur3->name = 'CHIBAH Yacine';
        $superviseur3->email='chibah@abc-pepsi.com';
        $superviseur3->password=bcrypt('chibah123');
        $superviseur3->save();
        $superviseur3->attachRole(2);

        $sup2= new Superviseur();
        $sup2->name_superviseur="CHIBAH Yacine";
        $sup2->email="chibah@abc-pepsi.com";
        $sup2->password=bcrypt('chibah123');
        $sup2->wilaya="ALGER";
        $sup2->matricule="5268";
        $sup2->zone_id=$zone->id;
        $sup2->save();

        $superviseur3= new User();
        $superviseur3->name = 'FILALI MERABET KARIM';
        $superviseur3->email='filali@abc-pepsi.com';
        $superviseur3->password=bcrypt('filali123');
        $superviseur3->save();
        $superviseur3->attachRole(2);


        $sup3= new Superviseur();
        $sup3->name_superviseur="FILALI MERABET KARIM";
        $sup3->email="filali@abc-pepsi.com";
        $sup3->password=bcrypt('filali123');
        $sup3->wilaya="ALGER";
        $sup3->matricule="5361";
        $sup3->zone_id=$zone->id;
        $sup3->save();

        
        

        $depot=new Depositaire(); 
        $depot->name_dipositaire="YAHIAOUI KARIM(VD)";
        $depot->zone_id=$zone->id;
        $depot->save();

        $depot1=new Depositaire(); 
        $depot1->name_dipositaire="KALEM KHALED(VD)";
        $depot1->zone_id=$zone->id;
        $depot1->save();

        $depot2=new Depositaire(); 
        $depot2->name_dipositaire="EURL NDD";
        $depot2->zone_id=$zone->id;
        $depot2->save();

        $depot3=new Depositaire(); 
        $depot3->name_dipositaire="BENNEBRI MERZAK(DP)";
        $depot3->zone_id=$zone->id;
        $depot3->save();

        $depot4=new Depositaire(); 
        $depot4->name_dipositaire="AMARA SAMIR(DP)";
        $depot4->zone_id=$zone->id;
        $depot4->save();

        $depot_user= new depositaire_superviseur();
        $depot_user->superviseur_id=$sup->id;
        $depot_user->depositaire_id=$depot->id;
        $depot_user->taux=0.60;
        $depot_user->save();

        $depot_user1= new depositaire_superviseur();
        $depot_user1->superviseur_id=$sup1->id;
        $depot_user1->depositaire_id=$depot1->id;
        $depot_user1->taux=0.62;
        $depot_user1->save();

        $depot_user2= new depositaire_superviseur();
        $depot_user2->superviseur_id=$sup2->id;
        $depot_user2->depositaire_id=$depot2->id;
        $depot_user2->taux=0.65;
        $depot_user2->save();

        $depot_user3= new depositaire_superviseur();
        $depot_user3->superviseur_id=$sup3->id;
        $depot_user3->depositaire_id=$depot3->id;
        $depot_user3->taux=0.66;
        $depot_user3->save();

        $depot_user4= new depositaire_superviseur();
        $depot_user4->superviseur_id=$sup3->id;
        $depot_user4->depositaire_id=$depot4->id;
        $depot_user4->taux=0.68;
        $depot_user4->save();

        $route1= new Route();
        $route1->name_route="EL HAMIZ";
        $route1->type_route="CONVENTIONNELLE";
        $route1->depositaire_superviseur_id=$depot_user->id;
        $route1->save();

        $route2= new Route();
        $route2->name_route="REGHAIA2";
        $route2->type_route="CONVENTIONNELLE";
        $route2->depositaire_superviseur_id=$depot_user->id;
        $route2->save();


        $route20= new Route();
        $route20->name_route="ROUIBA";
        $route20->type_route="PREVENTE";
        $route20->depositaire_superviseur_id=$depot_user->id;
        $route20->save();
    
        $prevendeur= new prevendeur();
        $prevendeur->name_prevendeur="BOUTICHE AISSA";
        $prevendeur->type_prevendeur="kkk";
        $prevendeur->code_prevendeur=5335;
        $prevendeur->route_id=$route20->id;
        $prevendeur->depositaire_superviseur_id=$depot_user->id; 
        $prevendeur->save();


        $route3= new Route();
        $route3->name_route="REGHAIA1";
        $route3->type_route="PREVENTE";
        $route3->depositaire_superviseur_id=$depot_user->id;
        $route3->save();


        $prevendeur= new prevendeur();
        $prevendeur->name_prevendeur="EL TOUIL Hakim";
        $prevendeur->type_prevendeur="kkk";
        $prevendeur->code_prevendeur=4822;
        $prevendeur->route_id=$route3->id;
        $prevendeur->depositaire_superviseur_id=$depot_user->id; 
        $prevendeur->save();


        $route31= new Route();
        $route31->name_route="DERGANA";
        $route31->type_route="CONVENTIONNELLE";
        $route31->depositaire_superviseur_id=$depot_user1->id;
        $route31->save();


        $route4= new Route();
        $route4->name_route="ROUTE KAIDI";
        $route4->type_route="CONVENTIONNELLE";
        $route4->depositaire_superviseur_id=$depot_user1->id;
        $route4->save();


        $route5= new Route();
        $route5->name_route="BAR EL BIEDA";
        $route5->type_route="PREVENTE";
        $route5->depositaire_superviseur_id=$depot_user1->id;
        $route5->save();


        $prevendeur= new prevendeur();
        $prevendeur->name_prevendeur="MEHADJRI SOFIANE";
        $prevendeur->type_prevendeur="kkk";
        $prevendeur->code_prevendeur=4759;
        $prevendeur->route_id=$route5->id;
        $prevendeur->depositaire_superviseur_id=$depot_user1->id; 
        $prevendeur->save();


        $route6= new Route();
        $route6->name_route="BORDJ EL KIFFAN1";
        $route6->type_route="PREVENTE";
        $route6->depositaire_superviseur_id=$depot_user1->id;
        $route6->save();


        $prevendeur= new prevendeur();
        $prevendeur->name_prevendeur="ZERGUI Mohamed Abderrahmane ";
        $prevendeur->type_prevendeur="kkk";
        $prevendeur->code_prevendeur=5152;
        $prevendeur->route_id=$route6->id;
        $prevendeur->depositaire_superviseur_id=$depot_user1->id;
        $prevendeur->save();



        $route7= new Route();
        $route7->name_route="HAMMADI";
        $route7->type_route="CONVENTIONNELLE";
        $route7->depositaire_superviseur_id=$depot_user2->id;
        $route7->save();


        $route8= new Route();
        $route8->name_route="BENCHOUBAN";
        $route8->type_route="CONVENTIONNELLE";
        $route8->depositaire_superviseur_id=$depot_user2->id;
        $route8->save();

        
        $route9= new Route();
        $route9->name_route="BORDJ EL BAHRI";
        $route9->type_route="CONVENTIONNELLE";
        $route9->depositaire_superviseur_id=$depot_user2->id;
        $route9->save();


        $route10= new Route();
        $route10->name_route="EUCALYPTUS";
        $route10->type_route="PREVENTE";
        $route10->depositaire_superviseur_id=$depot_user2->id;
        $route10->save();


        $prevendeur= new prevendeur();
        $prevendeur->name_prevendeur="BOUKHALFA KHALED";
        $prevendeur->type_prevendeur="kkk";
        $prevendeur->code_prevendeur=4867;
        $prevendeur->route_id=$route10->id;
        $prevendeur->depositaire_superviseur_id=$depot_user2->id;
        $prevendeur->save();



        $route11= new Route();
        $route11->name_route="AIN TAYA";
        $route11->type_route="PREVENTE";
        $route11->depositaire_superviseur_id=$depot_user2->id;
        $route11->save();


        $prevendeur= new prevendeur();
        $prevendeur->name_prevendeur="MADANI MOURAD";
        $prevendeur->type_prevendeur="kkk";
        $prevendeur->code_prevendeur=4846;
        $prevendeur->route_id=$route11->id;
        $prevendeur->depositaire_superviseur_id=$depot_user2->id;
        $prevendeur->save();

        $route12= new Route();
        $route12->name_route="SI MUSTAPHA";
        $route12->type_route="CONVENTIONNELLE";
        $route12->depositaire_superviseur_id=$depot_user3->id;
        $route12->save();

        $route13= new Route();
        $route13->name_route="ZEMMOURI";
        $route13->type_route="CONVENTIONNELLE";
        $route13->depositaire_superviseur_id=$depot_user3->id;
        $route13->save();

        $route14= new Route();
        $route14->name_route="BOUMERDES";
        $route14->type_route="CONVENTIONNELLE";
        $route14->depositaire_superviseur_id=$depot_user4->id;
        $route14->save();

        $route15= new Route();
        $route15->name_route="ISSER";
        $route15->type_route="CONVENTIONNELLE";
        $route15->depositaire_superviseur_id=$depot_user3->id;
        $route15->save(); 

        $route14= new Route();
        $route14->name_route="OULED MOUSSA";
        $route14->type_route="CONVENTIONNELLE";
        $route14->depositaire_superviseur_id=$depot_user4->id;
        $route14->save();

        $route15= new Route();
        $route15->name_route="KHEMIS EL KHECHNA";
        $route15->type_route="PREVENTE";
        $route15->depositaire_superviseur_id=$depot_user4->id;
        $route15->save();

        $prevendeur= new prevendeur();
        $prevendeur->name_prevendeur="DJENATI ALI";
        $prevendeur->type_prevendeur="kkk";
        $prevendeur->code_prevendeur=4886;
        $prevendeur->route_id=$route15->id;
        $prevendeur->depositaire_superviseur_id=$depot_user4->id;
        $prevendeur->save();
        

        $route16= new Route();
        $route16->name_route="BOUDOUAOU";
        $route16->type_route="PREVENTE";
        $route16->depositaire_superviseur_id=$depot_user4->id;
        $route16->save();

        $prevendeur= new prevendeur();
        $prevendeur->name_prevendeur="HELAL Mourad";
        $prevendeur->type_prevendeur="kkk";
        $prevendeur->code_prevendeur=5091;
        $prevendeur->route_id=$route16->id;
        $prevendeur->depositaire_superviseur_id=$depot_user4->id;
        $prevendeur->save(); 




    











		
    }
}
