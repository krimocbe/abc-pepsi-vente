<?php

use Illuminate\Database\Seeder;
use App\produit;

class ProduitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rb30 = new produit();
		$rb30->designation='RB30';
		$rb30->Type = ''; 
		$rb30->save();
		$rb100 = new produit();
		$rb100->designation='RB100';
		$rb100->Type = ''; 
		$rb100->save();    
    }
}
