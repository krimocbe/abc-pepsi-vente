<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $admin = new Role();
        $admin->name = 'Admin';
        $admin->save();

        $superviseur = new Role();
        $superviseur->name = 'Superviseur';
        $superviseur->save();

        $chef_de_zone = new Role();
        $chef_de_zone->name = 'CDF';
        $chef_de_zone->save();
    }
}
