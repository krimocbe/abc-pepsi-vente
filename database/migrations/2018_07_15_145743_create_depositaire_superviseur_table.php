<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositaireSuperviseurTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('depositaire_superviseur', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('superviseur_id')->unsigned()->nullable();
            $table->foreign('superviseur_id')->references('id')
            ->on('superviseurs')->onDelete('cascade');
            $table->integer('depositaire_id')->unsigned()->nullable();
            $table->foreign('depositaire_id')->references('id')
            ->on('dispositaires')->onDelete('cascade');
            $table->float('taux')->default(0.6);
            $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('depositaire_superviseur');
    }
}
