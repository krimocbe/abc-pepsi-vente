<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeadDailyVenteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('head_daily_vente', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('route_id')->unsigned();
            $table->integer('depositaire_superviseur_id')->unsigned();
            $table->foreign('depositaire_superviseur_id')->references('id')->on('depositaire_superviseur')->onDelete('cascade');
            $table->integer('prevendeur_id')->unsigned();
            $table->foreign('prevendeur_id')->references('id')->on('prevendeurs')->onDelete('cascade');
            $table->foreign('route_id')->references('id')->on('routes')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('head_daily_vente');
    }
}
