<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrevendeursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prevendeurs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_prevendeur');
            $table->string('type_prevendeur');
            $table->integer('code_prevendeur');
            $table->integer('route_id')->unsigned();
            $table->foreign('route_id')->references('id')->on('routes')->onDelete('cascade');
            $table->integer('depositaire_superviseur_id')->unsigned();
            $table->foreign('depositaire_superviseur_id')->references('id')->on('depositaire_superviseur')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prevendeurs');
    }
}
