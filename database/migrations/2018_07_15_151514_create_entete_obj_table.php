<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnteteObjTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entete_obj', function (Blueprint $table) {
            $table->bigincrements('id');
            $table->string('date');
            $table->integer('nbr_jour');
            $table->integer('objectif');
            $table->json('jour');
            $table->string('type_route');
            $table->unique(['date', 'type_route']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entete_obj');
    }
}
