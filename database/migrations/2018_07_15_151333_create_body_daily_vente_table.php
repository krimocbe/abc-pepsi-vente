<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBodyDailyVenteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('body_daily_vente', function (Blueprint $table) {
            $table->increments('id');
            $table->string('VISITES_PROGRAMMEES');
            $table->string('VISITES_AVEC_VENTE');
            $table->string('VISITES_realise');
            $table->string('DROP_SIZE_80Z')->default("0");
            $table->string('DROP_SIZE_PHY')->default("0");
            $table->integer('quantite');
            $table->date('date_ajout');
            $table->integer('body_id')->unsigned();
            $table->foreign('body_id')->references('id')->on('head_daily_vente')->onDelete('cascade');
            $table->unique(['date_ajout', 'body_id']);
            $table->integer('objectif_id');
            $table->integer('RB30');
            $table->integer('RB100');
            $table->integer('PET0_33');
            $table->integer('PET_1');
            $table->integer('PET1_5');
            $table->integer('PET_2');
            $table->integer('can');
            $table->integer('type')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('body_daily_vente');
    }
}
