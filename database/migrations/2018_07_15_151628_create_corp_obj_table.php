<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorpObjTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corp_obj', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('objectif');
            $table->integer('produit_id')->unsigned();
            $table->foreign('produit_id')->references('id')->on('produit')->onDelete('cascade');
            $table->integer('id_entete')->unsigned();
            $table->foreign('id_entete')->references('id')->on('entete_obj')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corp_obj');
    }
}
