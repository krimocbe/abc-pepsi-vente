@extends('dashboard.layouts.main') 
    
@section('content')
	
    	<h6 class="c-grey-900 mT-10 mB-30">
    		<span class="icon-holder">
    			<h6 class="text-center">Affichage des objectifs des vente par mois </h6>
        	</span>

        </h6>

	<div class=pull-right>
			<a href="{{ route('objectif1') }} " class="btn btn-primary" style="margin-bottom: 20px;"><i class="fa fa-plus"></i> Ajouter un Objectif du mois</a>
	</div>

<table class="table">
        <thead class="thead-light">
            <tr>
                <th>Mois</th>
                <th>Nombre des jours</th>
                <th>Objectif</th>
                <th>Type_route</th>
                <th>action</th>
                
            </tr>
        </thead>

        <tbody>
            @foreach($test as $fac1)
                <tr>
            
                	<td>{{ date("F  Y",strtotime($fac1->date))}}</td>
                    <td>{{ $fac1->nbr_jour }}</td>
                    <td>{{ $fac1->objectif }}</td>
                    <td>{{ $fac1->type_route }}</td>
                    <td>
                    <a href="{{ route('Objectif.edit',$fac1->id) }}" class="btn btn-primary btn-xs"><i
                                class="fa fa-pencil"></i></a>
                    {!! Form::open(['method' => 'DELETE','route' => ['Objectif.destroy', $fac1->id],'style'=>'display:inline']) !!}

                    <button type="submit" class="btn btn-danger btn-xs cur-p"><i class="fa fa-trash"></i></button>

                    {!! Form::close() !!}

    				</td>
    				
                </tr>
                @endforeach
            
        </tbody>
    </table>



@endsection    