@extends('dashboard.layouts.main')

@section('page-header')
    Facture <small>{{ trans('app.add_new_item') }}</small>
@endsection

@section('content')

   <div id="facture"  data-id="{{ Auth::user()->id }} "></div>    

@endsection
