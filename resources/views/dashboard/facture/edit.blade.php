@extends('dashboard.layouts.main')

@section('title')
Facture - Edit 
@endsection

@section('content')

<div class="bgc-white p-20 bd">
  <h6 class="c-grey-900">Edit Facture</h6>
  <div class="mT-30">
    
    <form action="{{ route('update_facture',$id) }}" method="POST">
     <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
     <div class="row"> 
               
                      <label class="col-md-12 control-label">Select:</label>
                      
                       <select class="form-control" name="text" >
                               
                               @foreach($items as $p => $data):
                                    echo '<option value="{{$p}}"  <?php if ($depot_name==$data): echo "selected"; ?>
                                        
                                    <?php endif ?>>{{ $data }}</option>';
                               @endforeach;
                       </select>
              
    </div>

    <div class="row"> 
               
                      <label class="col-md-12 control-label">Select:</label>
                      
                       <select class="form-control" name="route" >
                               
                               @foreach($items_route as $p => $route):
                                    echo '<option value="{{$p}}"  <?php if ($name_route==$route): echo "selected"; ?>
                                        
                                    <?php endif ?>>{{ $route }}</option>';
                               @endforeach;
                       </select>
              
    </div>
                   



        <div class="row">
            <label for="name">RB30:</label>
            <input type="number" class="form-control" value="{{ $body->RB30 }}" name="RB30">
        </div>
        
        <div class="row">
            <label for="price">RB100:</label>
            <input type="number" class="form-control" value="{{ $body->RB100 }}" name="RB100">
        </div>
        <div class="row">
            <label for="price">PET0.33:</label>
            <input type="number" class="form-control" value="{{ $body->PET0_33 }}" name="PET0_33">
        </div>
        
        <div class="row">
            <label for="price">PET1:</label>
            <input type="number" class="form-control" value="{{ $body->PET_1 }}" name="PET1">
        </div>

         <div class="row">
            <label for="price">PET1.5:</label>
            <input type="number" class="form-control" value="{{ $body->PET1_5 }}" name="PET1_5">
        </div>
       
        <div class="row">
            <label for="price">PET2:</label>
            <input type="number" class="form-control" name="PET2" value="{{ $body->PET_2 }}">
        </div>
       

        <div class="row">
            <label for="price">can:</label>
            <input type="number" class="form-control" name="can" value="{{ $body->can }}">
        </div>
       
        <div class="row">
            <label for="price"> VISITES_realise:</label>
            <input type="number" class="form-control" name=" VISITES_realise" value="{{ $body->VISITES_realise }}" />
        </div>

        <div class="row">
            <label for="price">Nombre clients visité:</label>
            <input type="number" class="form-control" name="nbr_client_visite" value="{{ $body->VISITES_PROGRAMMEES }}">
        </div>
       

        <div class="row">
            <label for="price">Nombre clients visité avec ventes:</label>
            <input type="number" class="form-control" name="nbr_client_visite_vente" value="{{ $body->VISITES_AVEC_VENTE }}" >
        </div>
       
       <div class="row">
            <label for="price">date:</label>
            <input type="date" class="form-control" name="date" value="{{ $body->date_ajout }}" >
        </div>
        
        <div class="pull-right">
            <input type="submit" class="btn btn-info"  />
        </div>
    

</form>
</div>
</div>


@endsection
