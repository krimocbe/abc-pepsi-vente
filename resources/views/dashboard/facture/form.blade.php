
    <div class="bgc-white p-40 bd">
        
        {{ Form::label('Nom du depot', 'Nom du depot') }}
        {{ Form::select('text', $items , null , ['class' => 'form-control']) }} 
      
        

        {{ Form::label('Nom du route', 'Nom du route') }}
        {{ Form::select('route', $items_route, null, ['class' => 'form-control']) }} 
      
        {{ Form::label('RB30', 'RB30') }}
        {{ Form::number('RB30' , null, ['class' => 'form-control']) }}

        
        {{ Form::label('RB100', 'RB100') }}
        {{ Form::number('RB100' , null, ['class' => 'form-control']) }}

        
        {{ Form::label('PET0.33', 'PET0.33') }}
        {{ Form::number('PET0_33' , '', ['class' => 'form-control']) }}

        
        {{ Form::label('PET1', 'PET1') }}
        {{ Form::number('PET1' , '', ['class' => 'form-control']) }}

        {{ Form::label('PET1.5', 'PET1.5') }}
        {{ Form::number('PET1_5' , '', ['class' => 'form-control']) }}

        
        {{ Form::label('PET2', 'PET2') }}
        {{ Form::number('PET2' , '', ['class' => 'form-control']) }}
      
        
        {{ Form::label('can', 'can') }}
        {{ Form::number('can' , '', ['class' => 'form-control']) }}
        
        {{ Form::label('VISITES_realise', 'VISITES_realise') }}
        {{ Form::number('VISITES_realise' , '', ['class' => 'form-control']) }}
       
       
      
        {{ Form::label('Nombre clients visité', 'Nombre clients visité') }}
        {{ Form::number('nbr_client_visite' , '', ['class' => 'form-control']) }}
      
        
        {{ Form::label('Nombre clients visité avec ventes', 'Nombre clients visité avec ventes') }}
        {{ Form::number('nbr_client_visite_vente' , '', ['class' => 'form-control']) }}
      
        
          {{ Form::label('Nombre clients visité avec ventes', 'Nombre clients visité avec ventes') }}
        {{ Form::date('date' , '', ['class' => 'form-control']) }}
        

        
    </div>
  </div>
</div>
