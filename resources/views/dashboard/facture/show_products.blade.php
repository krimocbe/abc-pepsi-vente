@extends('dashboard.layouts.main')


@section('content')
 
<h6 class="c-grey-900 mT-10 mB-30">
    		<span class="icon-holder">
    			<h6 class="text-center">REPORTING DES VENTES DE LA DISTRIBUTION DIRECTE</h6>
        	</span>
</h6>





<div class="row gap-20">
	<div class="col-md-3">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">RB30</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-20 c-green-500">{{ $fac->RB30 }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">PET0.33</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash3"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">{{ $fac->PET0_33 }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">RB100</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash2"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-red-50 c-red-500">{{ $fac->RB100 }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">PET1</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash4"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-red-50 c-red-500">{{ $fac->PET_1 }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="row gap-20">
	<div class="col-md-3">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">PET1.5</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash5"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-20 c-green-500">{{ $fac->RB30 }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">PET2</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash5"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">{{ $fac->PET_2 }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">can</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash1"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-red-50 c-red-500">{{ $fac->can }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Total phy</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash4"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-red-50 c-red-500">
    										  {{
    										  	$fac->can + 
    										  	$fac->PET_2 + 
    										  	$fac->PET1_5 +  
    										  	$fac->PET_1 + 
    										  	$fac->PET0_33 + 
    										  	$fac->RB100 + 
    										  	$fac->RB30 
    										  }}
    					</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row gap-20">
	<div class="col-md-3">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Familial</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash5"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-20 c-green-500">

 							 				{{  round(  
    										  	$fac->can *0.33* 24 /5.678 + 
    										  	$fac->PET_2 *2* 6 /5.678 + 
    										  	$fac->PET1_5*6*1.5/5.678+
    										  	$fac->PET_1 *6 / 5.678 + 
    										  	$fac->PET0_33 * 0.33 *  12 / 5.678 + 
    										  	$fac->RB100 * 12 / 5.678 + 
    										  	$fac->RB30 *0.30* 24 / 5.678 
    										     , 0)-round (  
    										  	$fac->can *0.33* 24 /5.678 + 
    										  	$fac->PET0_33 * 0.33 *  12 / 5.678 + 
    										  	$fac->RB30 *0.30* 24 / 5.678 
    										     , 0)
    										}}
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Single serve</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash5"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">
  											{{ round (  
    										  	$fac->can *0.33* 24 /5.678 + 
    										  	$fac->PET0_33 * 0.33 *  12 / 5.678 + 
    										  	$fac->RB30 *0.30* 24 / 5.678 
    										     , 0)
    										 }}
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Taux de succés</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash1"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-red-50 c-red-500">
						
                    {{ round( $fac->VISITES_AVEC_VENTE / $fac->VISITES_PROGRAMMEES *100,0)}}%
                    
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Total 80_Z</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash4"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-red-50 c-red-500">
    										   {{ 
    										   	round (  
    										  	$fac->can *0.33* 24 /5.678 + 
    										  	$fac->PET_2 *2* 6 /5.678 + 
    										  	$fac->PET1_5*6*1.5/5.678+
    										  	$fac->PET_1 *6 / 5.678 + 
    										  	$fac->PET0_33 * 0.33 *  12 / 5.678 + 
    										  	$fac->RB100 * 12 / 5.678 + 
    										  	$fac->RB30 *0.30* 24 / 5.678 
    										     , 3)
    										  }}
    					</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="row gap-20">
	<div class="col-md-3">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Drop size (80Z)</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash5"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-20 c-green-500">

  											{{  round (  
    										  	$fac->can *0.33* 24 /5.678 + 
    										  	$fac->PET_2 *2* 6 /5.678 + 
    										  	$fac->PET1_5*6*1.5/5.678+
    										  	$fac->PET_1 *6 / 5.678 + 
    										  	$fac->PET0_33 * 0.33 *  12 / 5.678 + 
    										  	$fac->RB100 * 12 / 5.678 + 
    										  	$fac->RB30 *0.30* 24 / 5.678 
    										     , 0)/
    										     $fac->VISITES_AVEC_VENTE
    										 }}
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Drop size (PHY)</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash5"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">
  
  			{{  					round(($fac->can + 
    										  	$fac->PET_2 + 
    										  	$fac->PET1_5 +  
    										  	$fac->PET_1 + 
    										  	$fac->PET0_33 + 
    										  	$fac->RB100 + 
    										  	$fac->RB30) 
    								/$fac->VISITES_AVEC_VENTE,0)
    										  		   }}
    					</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Taux de succés</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash1"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-red-50 c-red-500">
						
                    {{ round( $fac->VISITES_AVEC_VENTE / $fac->VISITES_PROGRAMMEES *100,0) }}%
                    
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Objectif</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash4"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-red-50 c-red-500">
    										{{ 
    											$objectif->objectif/$objectif->nbr_jour
    										}}
    					</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="row gap-20">
	<div class="col-md-3">
		
	</div>
	<div class="col-md-2">
		
	</div>

	<div class="col-md-3">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">TRO%</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash5"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-20 c-green-500">

  											{{  round (  
    										  	$fac->can *0.33* 24 /5.678 + 
    										  	$fac->PET_2 *2* 6 /5.678 + 
    										  	$fac->PET1_5*6*1.5/5.678+
    										  	$fac->PET_1 *6 / 5.678 + 
    										  	$fac->PET0_33 * 0.33 *  12 / 5.678 + 
    										  	$fac->RB100 * 12 / 5.678 + 
    										  	$fac->RB30 *0.30* 24 / 5.678 
    										     , 0)/
    										     $objectif->objectif *100
    										 }}%
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-md-4">
		
	</div>
</div>



@endsection