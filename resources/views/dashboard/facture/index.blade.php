@extends('dashboard.layouts.main') 


@section('content')


 

    	<h6 class="c-grey-900 mT-10 mB-30">
    		<span class="icon-holder">
    			<h6 class="text-center">REPORTING DES VENTES DE LA DISTRIBUTION DIRECTE</h6>
        	</span>

        </h6>	




<div class="row gap-20">
	<div class="col-md-2">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Total Factures</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-20 c-green-500">{{ $i }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Total routes</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash3"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">{{ $ro }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Total dépositaires</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash4"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-red-50 c-red-500">{{ $depot }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-md-2">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Total RB30</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-20 c-green-500">{{ $sum_RB30 }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Total RB100</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash3"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">{{ $sum_RB100 }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Total PET0.33</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash4"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-red-50 c-red-500">{{ $sum_PET0_33 }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row gap-20">
	<div class="col-md-2">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Total PET 1</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-20 c-green-500">{{ $sum_PET_1 }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Total PET1.5</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash3"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">{{ $sum_PET1_5 }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Total PET2</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash4"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-red-50 c-red-500">{{ $sum_PET_2 }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Total phy</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash4"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-red-50 c-red-500">
    										  {{
    										  	$sum_can + 
    										  	$sum_PET_2 + 
    										  	$sum_PET1_5 +  
    										  	$sum_PET_1 + 
    										  	$sum_PET0_33 + 
    										  	$sum_RB100 + 
    										  	$sum_RB30 
    										  }}
    					</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Familial</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash5"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-20 c-green-500">

 							 				{{  round(  
    										  	$sum_can *0.33* 24 /5.678 + 
    										  	$sum_PET_2 *2* 6 /5.678 + 
    										  	$sum_PET1_5*6*1.5/5.678+
    										  	$sum_PET_1 *6 / 5.678 + 
    										  	$sum_PET0_33 * 0.33 *  12 / 5.678 + 
    										  	$sum_RB100 * 12 / 5.678 + 
    										  	$sum_RB30 *0.30* 24 / 5.678 
    										     , 0)-round (  
    										  	$sum_can *0.33* 24 /5.678 + 
    										  	$sum_PET0_33 * 0.33 *  12 / 5.678 + 
    										  	$sum_RB30 *0.30* 24 / 5.678 
    										     , 0)
    										}}
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Single serve</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash5"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">
  											{{ round (  
    										  	$sum_can *0.33* 24 /5.678 + 
    										  	$sum_PET0_33 * 0.33 *  12 / 5.678 + 
    										  	$sum_RB30 *0.30* 24 / 5.678 
    										     , 0)
    										 }}
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>


<div class="row gap-20">
	<div class="col-md-2">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Total 80_Z</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-20 c-green-500">
							 {{ 
    										   	round (  
    										  	$sum_can *0.33* 24 /5.678 + 
    										  	$sum_PET_2 *2* 6 /5.678 + 
    										  	$sum_PET1_5*6*1.5/5.678+
    										  	$sum_PET_1 *6 / 5.678 + 
    										  	$sum_PET0_33 * 0.33 *  12 / 5.678 + 
    										  	$sum_RB100 * 12 / 5.678 + 
    										  	$sum_RB30 *0.30* 24 / 5.678 
    										     , 3)
    										  }}
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Drop size (80Z)</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash3"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">
  						@if($sum_VISITES_AVEC_VENTE!=0)					{{  round (  
    										  	$sum_can *0.33* 24 /5.678 + 
    										  	$sum_PET_2 *2* 6 /5.678 + 
    										  	$sum_PET1_5*6*1.5/5.678+
    										  	$sum_PET_1 *6 / 5.678 + 
    										  	$sum_PET0_33 * 0.33 *  12 / 5.678 + 
    										  	$sum_RB100 * 12 / 5.678 + 
    										  	$sum_RB30 *0.30* 24 / 5.678 
    										     , 0)/
    										     $sum_VISITES_AVEC_VENTE

    										 }}
    					@else {{ 0 }}   @endif					</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Drop size (PHY)</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash4"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-red-50 c-red-500">
							@if($sum_VISITES_AVEC_VENTE!=0)
  							{{  	round(($sum_can + 
    										  	$sum_PET_2 + 
    										  	$sum_PET1_5 +  
    										  	$sum_PET_1 + 
    										  	$sum_PET0_33 + 
    										  	$sum_RB100 + 
    										  	$sum_RB30) 
    								/$sum_VISITES_AVEC_VENTE,0)

    						    }} @else  {{ 0 }}  
    								@endif
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Taux de succés</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash4"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-red-50 c-red-500">
    								@if ($sum_VISITES_PROGRAMMEES!=0)		  {{ round( $sum_VISITES_AVEC_VENTE / $sum_VISITES_PROGRAMMEES *100,0) }}% 
    								@else  {{ 0 }}  
    								@endif
    					</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Familial</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash5"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-20 c-green-500">

 							 				{{  round(  
    										  	$sum_can *0.33* 24 /5.678 + 
    										  	$sum_PET_2 *2* 6 /5.678 + 
    										  	$sum_PET1_5*6*1.5/5.678+
    										  	$sum_PET_1 *6 / 5.678 + 
    										  	$sum_PET0_33 * 0.33 *  12 / 5.678 + 
    										  	$sum_RB100 * 12 / 5.678 + 
    										  	$sum_RB30 *0.30* 24 / 5.678 
    										     , 0)-round (  
    										  	$sum_can *0.33* 24 /5.678 + 
    										  	$sum_PET0_33 * 0.33 *  12 / 5.678 + 
    										  	$sum_RB30 *0.30* 24 / 5.678 
    										     , 0)
    										}}
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Single serve</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash5"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">
  											{{ round (  
    										  	$sum_can *0.33* 24 /5.678 + 
    										  	$sum_PET0_33 * 0.33 *  12 / 5.678 + 
    										  	$sum_RB30 *0.30* 24 / 5.678 
    										     , 0)
    										 }}
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<br/>
    @if(Auth::user()->hasRole('Superviseur'))
	<div class="pull-right">
			<a href="{{ route('add_facture') }} " class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter une facture</a>

	</div>
	@endif
    


<br/>
<br/>
<br/>
<br/>






                                                       
                    


<table class="table">
        <thead class="thead-light">
            <tr>

                <th></th>
                <th>Date ajout</th>
                <th>Name Depot</th>
                <th>Name Superviseur</th>
                <th>Nom route</th>
                <th>Type route</th>
                <th>nom prevendeur</th>
                <th>visite programmes</th>
                <th>visite avec vente</th>
                <th>action</th>
                
            </tr>
        </thead>
        <tbody>
            @foreach($facture as $fac1)
                <tr>
                	@foreach($fac1->sortBy('date_ajout') as $fac)	
                    <td><input type="checkbox" id="scales" name="feature"
               value="{{ $fac->id }}"  /></td>
                	<td>{{ date('d-M-y', strtotime($fac->date_ajout)) }}</td>
                	<td>{{ $fac->headers['depositaire_superviseur']['dipositaires']['name_dipositaire'] }}</td>
                    <td>{{ $fac->headers['depositaire_superviseur']['superviseurs']['name_superviseur'] }}</td>
                    <td>{{$fac->headers['route']['name_route'] }}</td>
                    <td>{{$fac->headers['route']['type_route'] }}</td>
                    <td>{{  $fac->headers['route']['prevendeur']['name_prevendeur'] }}</td>
                    <td>{{ $fac->VISITES_PROGRAMMEES }}</td>
                    <td>{{ $fac->VISITES_AVEC_VENTE }}</td>
                    <td>

                    	@if((($fac->type==0)&&(Auth::user()->hasRole('Admin')))||(($fac->type==0)&&(Auth::user()->hasRole('CDF'))))
                    <a href="{{ route('Facture.edit',$fac->id) }}" class="btn btn-primary btn-xs"><i
                                class="fa fa-pencil"></i></a>
                         @endif       
                    <a href="{{ route('show_products',$fac->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a>            
                    {!! Form::open(['method' => 'DELETE','route' => ['Facture.destroy', $fac->id],'style'=>'display:inline']) !!}

                    <button type="submit" class="btn btn-danger btn-xs cur-p"><i class="fa fa-trash"></i></button>

                    {!! Form::close() !!}

    				</td>
    				
                </tr>
                @endforeach
            @endforeach
        </tbody>
    </table>
     		
@endsection
			