<div class="sidebar">
	<div class="sidebar-inner">
		<!-- ### $Sidebar Header ### -->
		<div class="sidebar-logo">
			<div class="peers ai-c fxw-nw">
				<div class="peer peer-greed">
					<a class='sidebar-link td-n' href="/" class="td-n">
						<div class="peers ai-c fxw-nw">
							<div class="peer">
								<div class="logo">
									<img src="assets/static/images/logo.png" alt="">
								</div>
							</div>
							<div class="peer peer-greed">
								<h5 class="lh-1 mB-0 logo-text">Pepsi-abc</h5>
							</div>
						</div>
					</a>
				</div>
				<div class="peer">
					<div class="mobile-toggle sidebar-toggle">
						<a href="" class="td-n">
							<i class="ti-arrow-circle-left"></i>
						</a>
					</div>
				</div>
			</div>
		</div>

		<!-- ### $Sidebar Menu ### -->
		<ul class="sidebar-menu scrollable pos-r">
			<li class="nav-item mT-15 active">
				<a class='sidebar-link' href="/dashboard" default>
					<span class="icon-holder">
						<i class="c-blue-500 ti-home"></i>
					</span>
					<span class="title">Dashboard</span>
				</a>

				<a class='sidebar-link' href="{{route('facture') }}" >
					<span class="icon-holder">
						<i class="c-blue-500 ti-view-list-alt"></i>
					</span>
					<span class="title">Dailyfacture</span>
				</a>
				@if(Auth::user()->hasRole('Admin'))
				<a class='sidebar-link' href="{{route('objectif') }}" >
					<span class="icon-holder">
						<i class="c-blue-500 ti-view-list-alt"></i>
					</span>
					<span class="title"> Objectif du mois </span>
				</a>
				@endif
				
				
			</li>
		</ul>
	</div>
</div>