@extends('dashboard.layouts.main') 
@section('title') Home
@endsection 


@section('content')
<div class="row gap-20">
	<div class="col-md-4">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Total Factures</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-50 c-green-500"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Total routes</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash3"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100 mB-10">
				<h6 class="lh-1">Total prevendeurs</h6>
			</div>
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<span id="sparklinedash4"></span>
					</div>
					<div class="peer">
						<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-red-50 c-red-500"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<table class="table table-bordered">
        <thead class="thead-light">
            <tr>

                <th></th>
                <th>Name chef de zone</th>
                <th>Name Superviseur</th>
                <th>Nom route</th>
                <th>Total phy</th>
                <th>Familial</th>
                <th>Single serve</th>
                <th>Taux de succés</th>
                <th>Taux de visite</th>
                <th>Total 80_Z</th>
                <th>Drop size (80Z)</th>
                <th>Drop size (PHY)</th>
                <th>TRO</th>
                
            </tr>
        </thead>
        <body id="att">
            @foreach($userData1 as $fac2)
               @if($fac2['CDZ']=="" && $fac2['nom_superviseur']=="total" && $fac2['Nom route']=="" && $fac2['Total phy']=="" && $fac2['Familial']=="" ) @continue
               @endif 
                <tr>
                	<td><input type="checkbox" id="scales" name="feature"
               		  /></td>
                	<td>{{ $fac2['CDZ'] }}</td>
                	<td>{{ $fac2['nom_superviseur'] }}</td>
                	<td>{{ $fac2['Nom route'] }}</td>
                	<td>{{ $fac2['Total phy'] }}</td>
                	<td>{{ $fac2['Familial'] }}</td>
                	<td>{{ $fac2['Single serve'] }}</td>
                	<td>{{ $fac2['Taux de succés'] }}</td>
                	<td>{{ $fac2['Taux de visite'] }}</td>
                	<td>{{ $fac2['Total 80_Z'] }}</td>
                	<td>{{ $fac2['Drop size (80Z)'] }}</td>
                	<td>{{ $fac2['Drop size (PHY)'] }}</td>
                	<td>{{ $fac2['TRO%'] }}</td>
				</tr>
            @endforeach
        </body>
    </table>

@endsection