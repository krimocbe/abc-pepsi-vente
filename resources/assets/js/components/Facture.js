import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {fetch} from './action/objectif';
import {add_facture} from './action/objectif';
import {add_all_facture} from './action/objectif';
import SweetAlert from 'sweetalert2-react';

const ColoredLine = ({ color }) => (
    <hr
        style={{
            color: color,
            backgroundColor: color,
            height: 5
        }}
    />
);

const container = {
  'margin-left':'1140px',
}

export default class Facture extends Component {
  constructor() {
        super();
        this.handlesave=this.handlesave.bind(this);
         this.state = {
            selected: [],
            sum:[],
            show: false,
            msg:"",
            type:"success",
            title:"Succés"

          };
        }
  handlesave(id){
    let ids = [...this.state.selected];
    var state= ids[id];
    add_facture(state).then((data)=>{
      this.setState({
         show:true,
         msg:"1 Facture ajouté avec succés",
         type:"success",
         title:"Succés"
      });
    }).catch((status, err) => {
       this.setState({
         show:true,
         msg:"Erreur , Vous avez déja ajouteé la facture de cette route pour cette Journée !",
         type:"error",
         title:"Erreur"
      });    
  });
  }

  handlesaveall(){
    let ids = [...this.state.selected];
    var state= ids;
    let msg=state.length+" "+ " Facture ajouté avec succés"; 
    let sum=[];
    add_all_facture(state).then((data)=>{
      console.log("hello world",data.status);
      if (data.status === 500) {
         alert("vous avez ajoutez déja la route hadi");
      }
      sum=data.data.facture;
      this.setState({
         show:true,
         msg:msg,
         sum:sum,
         type:"success",
         title:"Succés"
      });
    }).catch((status, err) => {
       this.setState({
         show:true,
         msg:"Erreur , Vous avez déja ajouteé la facture de cette route pour cette Journée !",
         type:"error",
         title:"Erreur"
      });
    }); 
  }
  
  handleInput (key, e , id) {
    
    let ids = [...this.state.selected];
    console.log("key",ids[id][key]);     // create the copy of state array
    ids[id][key] = e.target.value;                  //new value
    var state = Object.assign({}, ids[id]);
    this.setState({   
      selected:ids 
    }); 
    state[key] = e.target.value;
    ids[id]=state;
    this.setState({
      selected: ids
    });

    console.log('**************************');
    console.log(this.state.selected);
    console.log('***************************');
  }

  componentDidMount () {
    fetch().then((data) => {
      
        let items=[];
        let sum=[];
        sum=data.data.facture;
        for (let i = 0; i < data.data.depot.length; i++) {
          console.log("**************depot*************");
          for (let j = 0; j < data.data.depot[i].depositaire_superviseurs.length; j++) {
          for (let k = 0; k < data.data.depot[i].depositaire_superviseurs[j].routes.length; k++) 
          {
          if(data.data.depot[i].depositaire_superviseurs[j].routes[k].prevendeur==null)
          {
           var nom_prevendeur="";
           var id_prevendeur="";
          }else {
              var nom_prevendeur=data.data.depot[i].depositaire_superviseurs[j].routes[k].prevendeur.name_prevendeur;
              var id_prevendeur=data.data.depot[i].depositaire_superviseurs[j].routes[k].prevendeur.id;
          }

          items.push({
            id:k ,
            id_depot: data.data.depot[i].id ,
            title: data.data.depot[i].depositaire_superviseurs[j].routes[k].name_route,
            depot:data.data.depot[i].name_dipositaire,
            note: data.data.depot[i].depositaire_superviseurs[j].routes[k].type_route,
            nom_prevendeur: nom_prevendeur,
            id_provendeur: "" ,
            id_route:data.data.depot[i].depositaire_superviseurs[j].routes[k].id , 
            id_depot_super:data.data.depot[i].depositaire_superviseurs[j].id,
            Date:0,
            RB30:0,
            RB100:0,
            PET033:0,
            PET1:0,
            PET15:0,
            PET2:0,
            can:0,
            visite:0,
            visite_programmé:0,
            visite_vente:0
             
          });

        }
      }
      }
      console.log(items.length);
      this.setState({
        selected:items,
        sum:sum
      });
    });
  }

  
  render() {
    let  quizes  = this.state.selected;
    let show = this.state.show;
    var table= this.state.sum;
    table.map((o)=>{
      console.log(o.sum_PET0_33);
    });
    console.log(table);
  return (
      
      <div className="row">
        <SweetAlert

          type={this.state.type}
          show={show}
          title="Succés"
          text={this.state.msg}
          onConfirm={() => this.setState({ show: false })}
        />
           
        <table className="table table-striped">
          <thead>
          <tr>

            <th>Depot</th>
            <th>Route</th>
            <th>Date</th>
            <th>RB30</th>
            <th>RB100</th>
            <th>PET0.33</th>
            <th>PET1</th>
            <th>PET1.5</th>
            <th>PET2</th>
            <th>can</th>
            <th>visite</th>
            <th>visite_programmé</th>
            <th>visite_vente</th>
            <th>actions</th>

          </tr>
          </thead>
          <tbody>
          {
            _.sortBy(quizes,'id').map((o) => {
              let id=o.id;
              return (
              <tr key={o.id}>
                  
                  <td>   {o.depot}
                  </td>
                  <td>   {o.title}
                  </td>
                  <td>    
                    <input type="date" className="form-control" size="2" name="Date" onChange={(e) => this.handleInput('Date', e, id )}  />
                  </td>
                  <td>
                      <div className="col-xs-1">
                        <input className="form-control" id={id} size="2" type="text" onChange={(e) => this.handleInput('RB30', e, id )}/>
                      </div>
                  </td>
                  
                  <td>
                      <div className="col-xs-1">
                        <input onChange={(e) => this.handleInput('RB100', e, id )}  className="form-control" id="ex1" size="2" type="text"/>
                      </div>
                  </td>
                  <td>
                      <div className="col-xs-1">
                        <input onChange={(e) => this.handleInput('PET033', e, id )} className="form-control" id="ex1" size="2" type="text"/>
                      </div>
                  
                  </td>
                  <td>
                      <div className="col-xs-1">
                        <input onChange={(e) => this.handleInput('PET1', e, id )}  id="ex1" size="2" type="text" className="form-control" />
                      </div>
                  </td>
                  <td>
                      <div className="col-xs-1">
                        <input onChange={(e)=>this.handleInput('PET15',e,id)} className="form-control" id="ex1" size="2" type="text"/>
                      </div>
                  </td>
                  
            
                  <td>
                      <div className="col-xs-1">
                        <input onChange={(e)=>this.handleInput('PET2',e,id)} className="form-control" id="ex1" size="2" type="text"/>
                      </div>
                  </td>

                  <td>
                      <div className="col-xs-1">
                        <input onChange={(e)=>this.handleInput('can',e,id)} className="form-control" size="2" id="ex1" type="text"/>
                      </div>
                  </td>
   
                  <td>
                      <div className="col-xs-1">
                        <input onChange={(e)=>this.handleInput('visite',e,id)} className="form-control" id="ex1" size="2" type="text"/>
                      </div>
                  </td>
                  
                  <td>
                      <div className="col-xs-1">
                        <input onChange={(e)=>this.handleInput('visite_vente',e,id)} size="2" className="form-control" id="ex1" type="text"/>
                      </div>
                  </td>

                  <td>
                      <div className="col-xs-1">
                        <input onChange={(e)=>this.handleInput('visite_programmé',e,id)} size="2" className="form-control" id="ex1" type="text"/>
                      </div>
                  </td>
                    
                  <td>
                    <button  className="btn btn-primary btn-xs" onClick={() => this.handlesave(id)}><i className="fa fa-save"></i></button>
                  </td>
                 
                
                
                </tr>
              );
            })
          }
          </tbody>
        </table>
          

          <div className="pull-right">
            
            <button  style={container} className="btn btn-primary btn-xs" onClick={() => this.handlesaveall()}  ><i className="fa fa-save"></i>Enregistrer tout</button>
          
          </div>                  

{
table.map((o) => {
  return (
            <div className="row gap-20">
              <div className="col-md-3">
                  <div className="layers bd bgc-white p-20">
                      <div className="layer w-100 mB-10">
                          <h6 className="lh-1"> RB100 </h6>
                      </div>
                      <div className="layer w-100">
                          <div className="peers ai-sb fxw-nw">
                              <div className="peer peer-greed">
                                  <span id="sparklinedash"></span>
                              </div>
                              <div className="peer">
                                  <span className="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-50 c-green-500">{o.sum_RB100}</span>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div className="col-md-3">
                  <div className="layers bd bgc-white p-20">
                      <div className="layer w-100 mB-10">
                          <h6 className="lh-1"> RB30 </h6>
                      </div>
                      <div className="layer w-100">
                          <div className="peers ai-sb fxw-nw">
                              <div className="peer peer-greed">
                                  <span id="sparklinedash3"></span>
                              </div>
                              <div className="peer">
                                  <span className="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">{o.sum_RB30} </span>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
    


              <div className="col-md-3">
                  <div className="layers bd bgc-white p-20">
                      <div className="layer w-100 mB-10">
                          <h6 className="lh-1">PET0.33</h6>
                      </div>
                      <div className="layer w-50">
                          <div className="peers ai-sb fxw-nw">
                              <div className="peer peer-greed">
                                  <span id="sparklinedash4"></span>
                              </div>
                              <div className="peer"> 
                                <span className="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">{o.sum_PET0_33}</span>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
    
            
            
     
    <div className="col-md-3">
        <div className="layers bd bgc-white p-20">
            <div className="layer w-100 mB-10">
                <h6 className="lh-1"> PET1 </h6>
            </div>
            <div className="layer w-100">
                <div className="peers ai-sb fxw-nw">
                    <div className="peer peer-greed">
                        <span id="sparklinedash3"></span>
                    </div>
                    <div className="peer">
                        <span className="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500"> {o.sum_PET_1}</span>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div className="col-md-3">
        <div className="layers bd bgc-white p-20">
            <div className="layer w-100 mB-10">
                <h6 className="lh-1"> PET1.5 </h6>
            </div>
            <div className="layer w-100">
                <div className="peers ai-sb fxw-nw">
                    <div className="peer peer-greed">
                        <span id="sparklinedash3"></span>
                    </div>
                    <div className="peer">
                        <span className="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500"> {o.sum_PET1_5}</span>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div className="col-md-3">
        <div className="layers bd bgc-white p-20">
            <div className="layer w-100 mB-10">
                <h6 className="lh-1"> PET2 </h6>
            </div>
            <div className="layer w-100">
                <div className="peers ai-sb fxw-nw">
                    <div className="peer peer-greed">
                        <span id="sparklinedash3"></span>
                    </div>
                    <div className="peer">
                        <span className="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500"> {o.sum_PET_2}</span>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div className="col-md-3">
        <div className="layers bd bgc-white p-20">
            <div className="layer w-100 mB-10">
                <h6 className="lh-1">can</h6>
            </div>
            <div className="layer w-100">
                <div className="peers ai-sb fxw-nw">
                    <div className="peer peer-greed">
                        <span id="sparklinedash3"></span>
                    </div>
                    <div className="peer">
                        <span className="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500"> {o.sum_can}</span>
                    </div>
                </div>
            </div>
        </div>

    </div>

    
</div>
)} )
}

</div>

        );
    }
}


if (document.getElementById('facture')) {
    console.log("found mothfuckers");
    ReactDOM.render(<Facture/>, document.getElementById('facture'));
}




        
        
        
        
        
      