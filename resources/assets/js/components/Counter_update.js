import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';
import DayPicker, { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import { updateObjectif } from './action/objectif';
import {loadObjectif} from './action/objectif';

const container = {
  position:'center'
}
const container1 = {
  float: 'right',
  fontFamily: 'system-ui',
  width: '180px', 
  float: 'right',
  height:'50px',
}

export default class Counter_update extends Component {
    constructor(props) {
        super(props);
        this.handleDayClick = this.handleDayClick.bind(this);
        this.handleInput = this.handleInput.bind(this);
        this.handleSave = this.handleSave.bind(this);
        this.state = {
            selectedDays: [],
            objectif: {
            },

            id:document.getElementById('test-root').getAttribute('data-id')
        };
  }
  
    componentDidMount () {
    console.log("hello world");
    console.log(this.state.id);
    let id = this.state.id;
    loadObjectif(id).then(({data}) => {
      console.log(data.depot.jour);
      var initialtab=[];
      data.depot.jour.map(function(date) {
          initialtab.push(new Date(date)); 
      });
      this.setState({
        selectedDays:initialtab,
        objectif: data.depot
      });
    });

    console.log("*************objectif***************");
    console.log(this.state.objectif);
    console.log("*************fin-objectif***************");
    
  }

  handleSave() {

    var state = Object.assign({}, this.state.objectif);
    state['jour']=this.state.selectedDays;
    console.log(state);
    updateObjectif(state).then((data) => {
      console.log(data);
      alert("hhhhh");
    });
  }

  handleInput (key, e) {

    /*Duplicating and updating the state */
    var state = Object.assign({}, this.state.objectif);
    state[key] = e.target.value;
    console.log(e.target.value);
    this.setState({
      objectif: state
    });
    console.log('**************************');
    console.log(this.state.objectif);
    console.log('***************************');
  }

  
    handleDayClick(day, { selected }) {

    var state = Object.assign({}, this.state.objectif);    
    const { selectedDays } = this.state;
    var taille=0;
    if (selected) {
      const selectedIndex = selectedDays.findIndex(selectedDay =>
        DateUtils.isSameDay(selectedDay, day)
      );
      selectedDays.splice(selectedIndex, 1);
    } else {
      selectedDays.push(day);
      state['Taille'] = selectedDays.length;
      taille++;
    }
    this.setState({ selectedDays:selectedDays });

    console.log(this.state.selectedDays);

    this.setState({
      objectif: state
    });
  }
 

    render() {
        let number=this.state.selectedDays.length;
        let objectif=this.state.objectif.objectif;
        let type_route=this.state.objectif.type_route;

      console.log("shitttttttttttttttttttttttt");
      console.log(number);
      console.log("*******************************************************************");
      console.log(this.state.objectif);
      console.log(this.state.objectif.type_route);
    
        return (
            
            
            <div>
            <div style={container}>
                <DayPicker
                    selectedDays={this.state.selectedDays}
                    onDayClick={this.handleDayClick}
                    disabledDays={[{ daysOfWeek: [5] }]}
                />
            </div>
            <div className="col-md-4"></div>
            <div style={container}>
                    
            <form>
            <div className="form-group">
                <label for="exampleInputEmail1">date du jour</label>
                    <input type="month" onChange={(e)=>this.handleInput('date', e)} value={this.state.objectif.date}   className="form-control"   name="Date"/>
                        
            </div>
            <div className="form-group">
                <label for="exampleInputPassword1">nombre de jour travaillé</label>
                    <input type="number" name="Taille" className="form-control"  onChange={(e) => this.handleInput('nbr_jour', e)} value={number} />
            </div>

                
            <div className="form-group">
                <label for="exampleInputPassword1">Objectif</label>
                    <input type="number" className="form-control" value={objectif}  onChange={(e) => this.handleInput('objectif', e)} name="Objectif"/>
            </div>
            
            <div className="form-group">
                 <label for="type de route">Type de route</label>
                    <select  className="form-control"  onChange={(e) => this.handleInput('type_route', e)} >
                          <option value="CONVENTIONNELLE"   selected={(type_route =='CONVENTIONNELLE') ? 'SELECTED' : null} >CONVENTIONNELLE</option>
                          <option value="PREVENTE"  selected={(type_route =='PREVENTE') ? 'SELECTED' : null} >PREVENTE</option>
                    </select>
              

              </div>
                
            </form>    
            
                <button onClick={() =>this.handleSave()} className="btn btn-info" style={container1}>Sauvegarder</button>
            </div>
            </div>

            
        );
    }
}

if (document.getElementById('test-root')) {
    console.log("found");
    ReactDOM.render(<Counter_update/>, document.getElementById('test-root'));
}   