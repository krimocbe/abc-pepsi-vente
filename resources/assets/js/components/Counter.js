import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';
import DayPicker, { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import { addObjectif } from './action/objectif';
import Select from 'react-select';
import SweetAlert from 'sweetalert-react';

const container = {
  position:'center'
}
const container1 = {
  float: 'right',
  fontFamily: 'system-ui',
  width: '180px', 
  float: 'right',
  height:'50px',

}

export default class Counter extends Component {
    constructor(props) {
        super(props);
        this.handleDayClick = this.handleDayClick.bind(this);
        this.handleInput = this.handleInput.bind(this);
        this.handleSave = this.handleSave.bind(this);
        this.state = {
            selectedDays: [],
            objectif: {
                Date: '',
                Taille:0,
                Obj: 0,
                type_route:'CONVENTIONNELLE',
                selectedDays:[]
            },

            taille:0
        };
  }

  handleSave() {

    var state = Object.assign({}, this.state.objectif);
    console.log(state);
    addObjectif(state).then((data) => {
      console.log(data);
      alert("hhhhh");
    });
  }

  handleInput (key, e) {

    /*Duplicating and updating the state */
    var state = Object.assign({}, this.state.objectif);
    state[key] = e.target.value;
    console.log(e.target.value);
    this.setState({
      objectif: state
    });
    console.log('**************************');
    console.log(this.state.objectif);
    console.log('***************************');
  }

  
    handleDayClick(day, { selected }) {

    var state = Object.assign({}, this.state.objectif);    
    const { selectedDays } = this.state;
    var taille=0;
    if (selected) {
      const selectedIndex = selectedDays.findIndex(selectedDay =>
        DateUtils.isSameDay(selectedDay, day)
      );
      selectedDays.splice(selectedIndex, 1);
    } else {
      selectedDays.push(day);
      state['selectedDays'].push(day);
      state['Taille'] = selectedDays.length;
      taille++;
    }
    this.setState({ selectedDays:selectedDays });
    console.log(this.state.selectedDays);
    console.log(day);
    this.setState({
      objectif: state
    });
  }
 

    render() {
        let number=this.state.selectedDays.length;
        return (
            
            
            <div>
            <div style={container}>
                <DayPicker
                    selectedDays={this.state.selectedDays}
                    onDayClick={this.handleDayClick}
                    disabledDays={[{ daysOfWeek: [5] }]}
                />
            </div>
            <div className="col-md-4"></div>
            <div style={container}>
                    
            <form>
            <div className="form-group">
                <label for="exampleInputEmail1">date du jour</label>
                    <input type="month" onChange={(e)=>this.handleInput('Date', e)}  className="form-control"   name="Date"/>
                        
            </div>
            <div className="form-group">
                <label for="exampleInputPassword1">nombre de jour travaillé</label>
                    <input type="number" name="Taille" className="form-control"  onChange={(e) => this.handleInput('Taille', e)} value={number} />
            </div>

                
            <div className="form-group">
                <label for="exampleInputPassword1">Objectif</label>
                    <input type="number" className="form-control"  onChange={(e) => this.handleInput('Obj', e)} name="obj"/>
            </div>
          
              <div className="form-group">
                 <label for="type de route">Type de route</label>
                    <select  className="form-control"  onChange={(e) => this.handleInput('type_route', e)} >
                          <option value="CONVENTIONNELLE">CONVENTIONNELLE</option>
                          <option value="PREVENTE">PREVENTE</option>
                    </select>
              

              </div>
          



                
            </form>    
            
            <div>
                <button onClick={()=>this.handleSave()} className="btn btn-info" style={container1}>Sauvegarder</button>
            </div>
            </div>
            </div>

            
        );
    }
}

if (document.getElementById('counter')) {
    console.log("found");
    ReactDOM.render(<Counter/>, document.getElementById('counter'));
}   