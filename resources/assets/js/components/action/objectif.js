import axios from 'axios';

export const addObjectif = (quiz) =>
  axios({
    method: 'POST',
    url: `/dashboard/add_objectif`,
    data: quiz
  });

  export const loadObjectif= (id) =>
     axios({
     	method:'GET',
     	url:`/dashboard/show_objectif/${id}`,
     	
  });
  
  export const updateObjectif=(state)=>
  	 axios({
  	 	method:'POST',
  	 	url:`/dashboard/update_objectif`,
  	 	data:state,
 	});

  export const fetch=()=>
     axios({
      method:'GET',
      url:`/dashboard/fetch`,
  });

  export const add_facture=(state)=>
    axios({
      method:'POST',
      url:`/dashboard/add_one_facture`,
      data:state,
    });
    
  export const add_all_facture=(state)=>
    axios({
      method:'POST',
      url:`/dashboard/add_many_facture`,
      data:state,
    });  